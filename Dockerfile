# syntax=docker/dockerfile:1

FROM ubuntu:23.04 as base
RUN apt-get update && apt-get install -y software-properties-common \
    && add-apt-repository ppa:swi-prolog/stable \
    && apt-get install -y -q --no-install-recommends python3-dev python3-pip python3-venv curl swi-prolog-nox xz-utils libnuma-dev \
    && curl -sSL https://get.haskellstack.org/ | sh \
    && apt remove software-properties-common curl -y \
    && apt autoremove -y

COPY parser/haskell /usr/local/src/haskell-parser/
RUN cd /usr/local/src/haskell-parser/ \
    && stack build \
    && cp $(stack exec which haskell-parser) /usr/local/src/haskell-parser/haskell-parser \
    && rm -rf /usr/loca/src/haskell-parser/.stack-work

FROM base
WORKDIR /root/

COPY requirements.txt /opt/requirements.txt
RUN python3 -m venv /opt/venv
# Enable venv
ENV PATH="/opt/venv/bin:$PATH"
RUN pip install -Ur /opt/requirements.txt

COPY . .
RUN rm -rf bin && mkdir bin
RUN cp /usr/local/src/haskell-parser/haskell-parser bin/

CMD ["uvicorn", "--host", "0.0.0.0", "--port", "8080", "src.web:app" ]
