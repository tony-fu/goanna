import {writable} from 'svelte/store';
import {defaultProjectFile, splitProjectFile} from "./defultHsProject.js";

interface File {
    moduleName: string,
    moduleContent: string
}

interface Fix {
    loc: [[number, number], [number, number], string],
    type: string,
}

interface Fingerprint {
    heads: {
        [key: string]: boolean
    },
    locations: {
        [key: number]: {
            is_fix: boolean,
            head: string,
            location: [[number, number], [number, number], string],
        }
    }
}

let initialFiles: File[]  = splitProjectFile(localStorage.getItem('projectFile') || defaultProjectFile)

export let files  = writable(initialFiles);
export let currentFile = writable(null);


export let result = writable([])

export let activeFix = writable([] as Fix[])

export let parseTree = writable({})
export let prologFiles = writable(['', ''] as [string, string])
export let diagnoses = writable([])

export let fingerprints = writable([] as Fingerprint[][])
export let currentFix = writable([null, null] as [number, number])

