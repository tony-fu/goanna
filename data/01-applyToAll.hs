applyToAll :: [a] -> (a -> b) -> [b]
applyToAll [] _ = []
applyToAll (x:xs) f = f x : applyToAll f xs

-- theme: list
-- ghc_accurate: 1
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 1
