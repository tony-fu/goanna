data BinTree = Leaf | Label Int BinTree BinTree

depth :: BinTree -> Int
depth Leaf = 0
depth (Leaf a l r) = 1 + max (depth l) (depth r)

tree = Label 1 (Label 2 Leaf Leaf) (Label 3 Leaf Leaf)
nodeCount = depth tree -- Result: 3

-- theme: adt
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 1
