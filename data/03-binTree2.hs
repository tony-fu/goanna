data BinTree = Leaf | Label Int BinTree BinTree

depth :: BinTree -> Int
depth Leaf = 0
depth (Label a l r) = 1 + max l r

inorderTraversal :: BinTree -> [Int]
inorderTraversal Leaf = []
inorderTraversal (Label label left right) = inorderTraversal left ++ [label] ++ inorderTraversal right

myTree = Label 2 (Label 1 Leaf Leaf) (Label 3 Leaf Leaf)

inorderList = inorderTraversal myTree -- Result: [1, 2, 3]

-- theme: adt
-- ghc_accurate: 1
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
