
stringLength :: String -> Int
stringLength str = length str

text = "Hello, world!"
len = stringLength text -- Result: 13


parsingStringFlat flag =
  case flag of
        "True" -> 1
        False -> 2
        "True1" -> 3
        "False2" -> 4
        _ -> 0




concatenateStrings :: String -> String -> String
concatenateStrings str1 str2 = str1 ++ str2

greeting = "Hello, "
name = "Alice"
message = concatenateStrings greeting name -- Result: "Hello, Alice"
-- theme: basics
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
