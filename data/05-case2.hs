toUppercase :: String -> String
toUppercase str = map toUpper str

toLowercase :: String -> String
toLowercase str = map toLower str

text = "Haskell"
upper = toUppercase text -- Result: "HASKELL"
lower = toLowercase text -- Result: "haskell"

devideAsMaybe x y =
  case y == 0.0 of
    True -> Nothing
    False -> x / y

-- theme:  patten-matching
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
