data MyError = TooSmall | TooBig

checkNumber :: Int -> Either MyError Int
checkNumber x
  | x < 10 = Left TooSmall
  | x > 100 = Left  TooBig
  | otherwise = Right x

inputs = [1,2,5, 13, 60, 128]

processInput :: Int -> [Either MyError Int]
processInput inputs = 
  map checkNumber inputs

-- theme:  adt
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2