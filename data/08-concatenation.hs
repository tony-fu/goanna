myreverse          :: [a] -> [a]
myreverse          =  foldl (flip (:)) []

test :: String
test = xs : myreverse "def"
  where xs = "abc"

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 8