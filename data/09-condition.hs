data U = UCon Bool Int (Int, Int)
u :: U -> Int
u (UCon x y j) =
  if x
    then j
    else fst y + snd y

-- theme: adt
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2