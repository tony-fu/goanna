distence x y = (x + y) * (x - y)

distances xs ys = zipWith distence xs ys

sumDistances :: Int
sumDistances = distances [1 ,3] [2, 4]

median :: [a] -> a
median xs = let lengthOfXs = length xs
                half = lengthOfXs `div` 2
            in at half xs

at 0 (x:xs) = x
at n (x:xs) = at (n - 1) xs

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3