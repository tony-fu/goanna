elementAt' :: [a] -> Int -> a
elementAt' (x:_) 1  = x
elementAt' [] _     = undefined
elementAt' (_:xs) k
  | k < 1           = undefined
  | otherwise       = elementAt' xs (k - 1)

double x = x * 2

eight = double "4"

-- theme: basics
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 3