left (Left a) = Just a
left (Right _) = Nothing

eitherToMaybe :: Either Int String -> Int
eitherToMaybe e = left e

-- theme: adt
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 3