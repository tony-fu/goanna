isEven 0 = "True"
isEven n = isOdd (n - 1)

isOdd 0 = False
isOdd n = isEven (n - 1)


-- theme: function
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3