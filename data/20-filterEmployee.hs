data Person = Employee String Int | Customer String Int

filterEmployees xs = filter isEmployee xs
  where isEmployee (Employee _ _) = True
        isEmployee _ = False

people :: [Person]
people = [Employee "Alice" 123, Customer "Bob" 456, Employee "Charlie" 789]

employees :: [Person]
employees = filter filterEmployees people

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 3