
students = [(1, 99), (2, 60), (3, 55)]

matchFirst key (k, v) = k == key

filterById st studentId =
  filter (matchFirst studentId) st

scores = filterById students '1'

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 7