fizzBuzz :: Int -> String
fizzBuzz n | n `mod` 15 == 0 = "FizzBuzz"
           | n `mod` 5  == 0 = "Fizz"
           | n `mod` 3  == 0 = "Buzz"
           | otherwise       = n

-- theme: pattern-matching
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 2