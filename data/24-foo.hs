antisymatric :: Eq a => a -> a -> Bool
antisymatric x y = x >= y && y >= x

-- theme: type-class
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2