f xs y = map (+ y) xs

xs :: [Int]
xs = [1.0, 2.0, 3.0, 4.0, 5.0]


ys :: [Float]
ys = f xs 2.5

-- theme:  basics
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2