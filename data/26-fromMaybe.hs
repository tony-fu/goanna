fromMaybe' m x =
    case m of
        Nothing -> x
        Just a -> a

maybeOrZero m = fromMaybe' 0 m

-- add two maybe values
a = maybeOrZero (Just 3)
b = maybeOrZero Nothing
c = a + b

-- theme:  adt
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 11