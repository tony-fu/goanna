new :: Monoid m => m
new = mempty

doubleSelf :: (Functor f) => f a -> f a
doubleSelf xs = xs `mappend` xs


concatenate :: Monoid a => [a] -> a
concatenate = mconcat

mconcatMap :: Monoid b => (a -> b) -> [a] -> b
mconcatMap f x = mconcat ( map f x)

intercalate :: Monoid a => a -> [a] -> a
intercalate separator [] = mempty
intercalate separator [x] = x
intercalate separator (x:xs) = x `mappend` (separator `mappend` intercalate separator xs)

-- theme:  type-class
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2