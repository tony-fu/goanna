encode [] = []
encode (x:xs) = (length (x : takeWhile (==x) xs), x)
                 : encode (dropWhile (==x) xs)

doubleSelf2 :: (Functor f) => f a -> f a
doubleSelf2 xs = fmap (\a -> mappend a a) xs

-- theme:  type-class
-- ghc_accurate: 1
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2