data Category = Untaxed Int | Taxed  Int Float

getSalary (Taxed salary) = salary
getSalary (Taxed salary tax) = salary - (floor tax)

-- theme:  adt
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3