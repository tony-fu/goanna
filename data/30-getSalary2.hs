data Category = Untaxed Int | Taxed  Int Float

getSalary :: Category -> Int
getSalary (Untaxed salary) = salary
getSalary (Taxed salary tax) = floor salary - tax

-- theme:  adt
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 3