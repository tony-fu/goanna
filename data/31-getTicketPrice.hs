data Ticket = Normal Int | Discounted Int Float

getPrice :: Ticket -> Float
getPrice (Normal price) = price
getPrice (Discounted price discount) = price * discount

-- theme:  adt
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3