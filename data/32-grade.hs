grade x
  | (x >= 90) && (x <= 100) = 'A'
  | (x >= 80) && (x <= 90) = 'B'
  | (x >= 70) && (x <= 80) = "C"
  | (x >= 60) && (x <= 70) = 'D'
  | otherwise = 'F'

-- theme:  pattern-matching
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2