data Person = Person String Int


createPerson :: String -> Int -> Person
createPerson name age = Person name age

alice = createPerson "Alice" 30

greet :: Person -> String
greet (Person age name) = "Hello, " ++ name ++ "! You are " ++ show age ++ " years old."

getName :: Person -> String
getName (Person name _) = name

getAge :: Person -> Int
getAge (Person _ age) = age

bob = createPerson "Bob" 25
bobName = getName bob -- Result: "Bob"
bobAge = getAge bob   -- Result: 25

-- theme:  adt
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2