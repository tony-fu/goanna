incr n = n + 1
decr n = n - 1

incrementOrDecrement True c = incr c
incrementOrDecrement a x =  decr a

-- theme: basics
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 3