insertAt :: Eq a => a -> [a] -> Int -> [a]
insertAt el list n =
    let accu (i, acc) x =
            if i == n
                then (acc ++ [el,x],i+1)
                else (acc ++ [x],i+1)
    in fst (foldl accu ([],1) list)

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 1