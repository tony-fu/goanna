isEven :: Int -> Bool
isEven x = x `mod` 2 == 0

containsEven :: [Int] -> Bool
containsEven xs = any (map isEven xs)

x = containsEven ["36", "8", "123"]

-- theme: list
-- ghc_accurate: 2
-- helium_accurate: 1
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 5