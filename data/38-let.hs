numberOfNumericChars :: [Char] -> Int
numberOfNumericChars xs =
    let numeric = "01234567890"
        isNumeric = filter (`elem` numeric) xs
    in isNumeric

-- theme: basics
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3