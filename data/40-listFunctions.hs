factorial :: Int -> Int
factorial 0 = 1
factorial n = n * factorial (n - 1)

last' :: [a] -> a
last' [x] = x
last' (x:xs) = xs

-- Takes the first n elements from a list
take' :: Int -> [a] -> [a] 
take' n [] = []
take' n (x:xs) = x : (take' (n - 1) xs)

-- theme: list
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2