sumList :: [Int] -> Int
sumList [] = 0
sumList (x:xs) = x + sumList xs

myelem a [] = False
myelem a (x:xs) = if a == x then True else myelem xs

myNotElem :: Eq a => a -> [a] -> Bool
myNotElem a xs = not (myelem a)

-- theme: type-class
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2