last' :: [a] -> a
last' [x] = x
last' (x:xs) = last' xs

-- Takes the first n elements from a list
take' :: Int -> [a] -> [a]
take' n [] = []
take' n (x:xs) = x ++ (take' (n - 1) x)

fibonacci :: Int -> Int
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n - 1) + fibonacci (n - 2)

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 4