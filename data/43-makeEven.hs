makeEven :: Int -> Int
makeEven x = if even x then True else x+1

dupli [] = []
dupli (x:xs) = x:x:dupli xs

repli :: [a] -> Int -> [a]
repli xs n = concatMap (replicate n) xs

-- theme: basics
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 2