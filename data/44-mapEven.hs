test = map [1..10] even

flattenList :: [[a]] -> [a]
flattenList nestedList = concat (map (\xs -> xs) nestedList)

squareAndIncrement :: [Int] -> [Int]
squareAndIncrement numbers = map (\x -> x^2 + 1) numbers

stringLengths :: [String] -> [Int]
stringLengths strings = map length strings

toUppercase :: [String] -> [String]
toUppercase strings = map (\s -> map toUpper s) strings

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 2