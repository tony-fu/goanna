

stringToInts :: [Char] -> [Int]
stringToInts cs = map charToInt cs

charToMaybe :: [Maybe Char]
charToMaybe =  map id ['a', 'b', 'c']

charToInt 'A' = 1
charToInt 'B' = 2
charToInt 'C' = 3
charToInt 'D' = 4
charToInt 'E' = 5
charToInt 'F' = 6
charToInt 'G' = 7
charToInt 'H' = 8

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 4