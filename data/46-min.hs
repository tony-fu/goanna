min' :: a -> a -> a
min' x y = if x < y then x else y

r :: [a] -> [a]
r list = r' list []
  where
    r' [] rd     = rd
    r' (x:xs) rd = r' xs (x:rd)

-- theme: type-class
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2