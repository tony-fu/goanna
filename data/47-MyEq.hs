class MyEq a where
  myeq :: a -> a -> Bool

instance MyEq Int where
  myeq x y = x == y

instance MyEq Bool where
  myeq True True = True
  myeq False False = True
  myeq _ _ = False

data Pair a b = Pair a b

instance (MyEq a, MyEq b) => MyEq (Pair a b) where
  myeq (Pair x1 y1) (Pair x2 y2) = myeq x1 x2 && myeq y1 y2

data MyList a = Nil | Cons a (MyList a)

isEq = myeq (Pair (Cons True Nil) True) (Pair (Cons True Nil) False)

-- theme: type-class
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 5