normalDays = 28

leapYearDays  = 29

daysInFeburary isleapyear  = if isleapyear then isleapyear else normalDays

-- theme: basics
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 2