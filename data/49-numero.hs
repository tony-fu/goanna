prefixes = ["", "un", "duo","tre","quattuor","quin",   "sex", "septen","octo", "novem"]
suffixes = ["dec"] ++ (map (\x -> x:"gint") ["vi", "tri","quadra",  "quinqua","sexa","septua","octo", "nona"])

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 5