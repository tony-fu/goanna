data Person = Person String Int

class HasAge a where
  ageOf :: a -> Int

instance HasAge Person where
  ageOf (Person _ age) = age

olderThan ::  a -> a -> Bool
olderThan x y = ageOf x >= ageOf y

-- theme: type-class
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2