import Control.Monad.State

getToken :: State [Char] Char
getToken = do
    tokens <- get
    put (tail tokens)
    return (head tokens)

parseDigitMaybe :: State [Char] Int
parseDigitMaybe = do
  token <- getToken
  return (if token `elem` "0123456789" then Just (read [token]) else Nothing)

-- theme: monad
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 4