import Control.Monad.State

getToken :: State [Char] Char
getToken = do
    tokens <- get
    put (tail tokens)
    return (head tokens)

parseDigitInt :: State [Char] Int
parseDigitInt = do
  token <- getToken
  return (if token `elem` "0123456789" then [token] else 0)

-- theme: monad
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3
