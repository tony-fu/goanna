data Password = P String

-- Validate how good a password is
validate :: Password -> String
validate password = 
    if length password > 10 
        then "Great password"
        else "Password too short"


createPassword :: String -> Password
createPassword str = P str

myPassword = createPassword "my_secret_password"

validatePassword :: Password -> Bool
validatePassword (P str) = length str >= 8

-- theme: adt
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
