data Period
  = DayPeriod Day
  | WeekPeriod Day
  | MonthPeriod Year Month
  | QuarterPeriod Year Quarter
  | YearPeriod Year
  | PeriodBetween Day Day
  | PeriodFrom Day
  | PeriodTo Day
  | PeriodAll

-- synonyms for various date-related scalars
type Year = Int

type Month = Int -- 1-12

type Quarter = Int -- 1-4

type YearWeek = Int -- 1-52

type MonthWeek = Int -- 1-5

type YearDay = Int -- 1-366

type MonthDay = Int -- 1-31

type WeekDay = Int -- 1-7

data Day = Day Year Month Int

type DateSpan  = ((Maybe Day), (Maybe Day))

addDays :: Int -> Day -> Day
addDays n day = day

quarterAsMonth :: Quarter -> Month
quarterAsMonth q = (q - 1) * 3 + 1

-- fromGregorian :: Year -> Month -> Int -> Maybe Day
fromGregorian y m d = Just (Day y m d)

periodAsDateSpan :: Period -> DateSpan
periodAsDateSpan (DayPeriod d) = ((Just d), (Just (addDays 1 d)))
periodAsDateSpan (WeekPeriod b) =  ((Just b), (Just (addDays 7 b)))
periodAsDateSpan (MonthPeriod y m) =
    let yy = if m == 12 then y + 1 else y
        mm = if m == 12 then 1 else m + 1
    in (Just (fromGregorian y m 1), Just (fromGregorian yy mm 1))


-- theme: adt
-- ghc_accurate: 1
-- helium_accurate: 1
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 9
