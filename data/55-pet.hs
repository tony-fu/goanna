
data Pet = Cat String | Dog String | Snake

petIsDog :: Pet -> Bool
petIsDog Dog  = True
petIsDog _  = False

isCat :: Pet -> Bool
isCat (Cat _) = True
isCat _ = False

makeDog :: String -> Pet
dog = makeDog "Fido"
cat = Cat "Mittens"

mittenIsCat = isCat cat

-- theme: adt
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
