v = replicate 3 5 

hello = replicate 2 "Hello" -- Result: ["Hello", "Hello"]

lists = replicate 4 [1, 2, 3] -- Result: [[1, 2, 3], [1, 2, 3], [1, 2, 3], [1, 2, 3]]

emptyList = replicate 0 [] 

repeatChar :: Char -> Int -> Char
repeatChar c n = replicate n c

values = let n = 3
             element = "Value"
             replicatedList = replicate n element
          in replicatedList   
-- Result: ["Value", "Value", "Value"]

-- theme: list
-- ghc_accurate: 1
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
