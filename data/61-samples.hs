
samples = ["2.0", 3.0, 4.0, 5.0, 6.0]

range xs = fromIntegral (length xs)

mean xs = sum xs / range xs

variance xs = map (\x -> (x - mean xs) ^ 2 / range xs) xs


-- theme: basics
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
