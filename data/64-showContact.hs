email = ["john@fp.com", "bill@test.org"]
phone = [22490012, 2258267]

-- Pick a format of contact info
-- based on given choice
showContact choice = if choice == "email"
    then "Email: " ++ head email
    else  "Phone: " ++ head phone

-- theme: basics


-- ghc_accurate: 1
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 4
