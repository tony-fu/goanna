sumEven :: [Int] -> Int
sumEven xs = sum (filter even xs)

stringInts = ["1", "2", "3", "4"]

stringToInt = read

sumEvenStringInts :: Int
sumEvenStringInts = sumEven (map id stringInts)

intersperse' :: a -> [a] -> [a]
intersperse' _ [] = []
intersperse' _ [x] = [x]
intersperse' sep (x:xs) = x : sep : intersperse' sep xs

nub' :: Eq a => [a] -> [a]
nub' [] = []
nub' (x:xs) = x : nub' (filter (/= x) xs)

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 7
