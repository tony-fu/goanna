sumEven :: [Int] -> Int
sumEven xs = sum (filter even xs)

integers = filter read ["1", "2", "3", "4"]

sumEvenStringInts :: Int
sumEvenStringInts = sumEven integers


intersperse' :: a -> [a] -> [a]
intersperse' _ [] = []
intersperse' _ [x] = [x]
intersperse' sep (x:xs) = x : sep : intersperse' sep xs

nub' :: Eq a => [a] -> [a]
nub' [] = []
nub' (x:xs) = x : nub' (filter (/= x) xs)

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 6
