import Control.Monad.State

sumState :: Int -> State Int Int
sumState n = do
  modify (+n)
  get

multiplyState :: Int -> State Int Int
multiplyState n = do
  x <- get
  put (x * n)


-- theme: monad
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 5
