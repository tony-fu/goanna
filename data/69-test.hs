
p :: (Eq a) => [a] -> [[a]]
p [] = []
p (x:xs) = (x : takeWhile (==x) xs) : p (dropWhile (==x) xs)

test = xs : [4, 5, 6]
       where xs = [1, 2, 3]

-- theme: list

-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 4
