data Shape = Circle Float | Rectangle Float Float

area (Circle r) = pi * r * r
area (Rectangle w h) = w * h

shapes = [Rectangle 2.0, Circle 1.0, Circle 2.4, Circle 5.2, Circle 1.1]


-- Calculate the perimeter of a shape
perimeter :: Shape -> Float
perimeter (Circle r) = 2.0 * pi * r
perimeter (Rectangle w h) = 2.0 * (w + h)

-- Check if a shape is a square
isSquare :: Shape -> Bool
isSquare (Circle _) = False
isSquare (Rectangle w h) = w == h

-- Create a circle with a given radius
makeCircle :: Float -> Shape
makeCircle r = Circle r

-- Create a rectangle with given width and height
makeRectangle :: Float -> Float -> Shape
makeRectangle w h = Rectangle w h

-- theme: adt
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
