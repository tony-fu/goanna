standardTrans z =
  case z of
    "shorttitle" -> ["short"]
    "sorttitle" -> ["sorted"]
    "indextitle" -> ["index"]
    "indexsorttitle" -> ["index", "sorted"]
    _ -> z
-- bookTrans :: String -> [String]
bookTrans z =
  case z of
    "title" -> ["booktitle"]
    "subtitle" -> ["booksubtitle"]
    "titleaddon" -> ["booktitleaddon"]
    "shorttitle" -> []
    "sorttitle" -> []
    "indextitle" -> []
    "indexsorttitle" -> []
    _ -> [z]
transformKey x y "author"
  | elem x ["mvbook", "book"] =
    ["bookauthor", "author"]
transformKey _ _ x = [x]


-- theme: list
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 0
-- goanna_total: 2
