
data Tree a = Empty | Branch a (Tree a) (Tree a)
leaf x = Branch x Empty Empty


treeContains :: Eq a => a -> Tree a -> Bool
treeContains _ Empty = False
treeContains x (Branch y left right) = x == y || treeContains x left || treeContains x right

countBranches Empty = 0
countBranches (Branch _ l r) = 1 + l + r


treeMap :: (a -> b) -> Tree a -> Tree b
treeMap _ Empty = Empty
treeMap f (Branch x left right) = Branch (f x) (treeMap f left) (treeMap f right)

treeToList :: Tree a -> [a]
treeToList Empty = []
treeToList (Branch x left right) = treeToList left ++ [x] ++ treeToList right

treeSum :: Tree Int -> Int
treeSum Empty = 0
treeSum (Branch x left right) = x + treeSum left + treeSum right

treeDepth :: Tree a -> Int
treeDepth Empty = 0
treeDepth (Branch _ left right) = 1 + max (treeDepth left) (treeDepth right)

-- theme: adt
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 2
