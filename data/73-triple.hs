myLast :: [a] -> a
myLast [] = undefined
myLast [x] = x
myLast (_:xs) = myLast xs

len           :: [a] -> Int
len []        =  0
len (_:xs)    =  1 + len xs

triple x = x + x + x

v = triple '4'

-- theme: basic
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3
