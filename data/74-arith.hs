fun :: Int -> Int
fun x
  | x `div` 10 == 0 = 0
  | otherwise = 1 + fun (product (map read (show x)))

-- theme: list

-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 3
