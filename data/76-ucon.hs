
data Meal = Meal Bool Int (Int, Int)

mealPrice (Meal dineIn price price') =
  if dineIn
    then price'
    else fst price + snd price

actual = mealPrice (Meal False 0 (15, 10))
expect = 25

test :: Bool
test = actual == expect

-- theme: adt

-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 4
