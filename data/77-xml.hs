
data XML = XML Position Part
data Position = Top | Bottom | Left | Right
data Attribute
type Name = String

data Part =
     Element Name [Attribute] [XML]
   | Comment String
   | Text String

getPart :: XML -> Part
getPart (XML pos part) = part


printXML (Element name [attributs] xmls) =
  "<" ++ name ++ ">"
  ++ mconcat (map printXML xmls)
  ++ "</" ++ name ++ ">"
printXML (Text text) = text

-- theme: adt

-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 3
