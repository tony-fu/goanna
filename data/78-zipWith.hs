

addLists :: [Int] -> [Int] -> [Int]
addLists xs ys = zipWith (+) xs ys

concatenateStrings :: [String] -> [String] -> [String]
concatenateStrings xs ys = zipWith (++) xs ys


multiplyLists :: [Float] -> [Float] -> [Float]
multiplyLists xs ys = zipWith (*) xs ys

customOperation :: [Int] -> [Int] -> [Int]
customOperation xs ys = zipWith (\x y -> 2 * x + 3 * y) xs ys

f :: a -> b -> (a, b)
f x y = (x, y)

g :: [a] -> [b] -> [(a, b)]
g xs ys = zipWith f xs xs

subtractLists :: [Int] -> [Int] -> [Int]
subtractLists xs ys = zipWith (-) xs ys

elementWiseEquality :: [Int] -> [Int] -> [Bool]
elementWiseEquality xs ys = zipWith (==) xs ys


elementWiseOr :: [Bool] -> [Bool] -> [Bool]
elementWiseOr xs ys = zipWith (||) xs ys

-- theme: list
-- ghc_accurate: 0
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3
