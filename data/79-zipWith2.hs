
makeTuple x y = (y, x)

f :: [a] -> [b] -> [(a, b)]
f xs ys = zipWith makeTuple xs ys

addLists :: [Int] -> [Int] -> [Int]
addLists xs ys = zipWith (+) xs ys

concatenateStrings :: [String] -> [String] -> [String]
concatenateStrings xs ys = zipWith (++) xs ys


multiplyLists :: [Float] -> [Float] -> [Float]
multiplyLists xs ys = zipWith (*) xs ys

customOperation :: [Int] -> [Int] -> [Int]
customOperation xs ys = zipWith (\x y -> 2 * x + 3 * y) xs ys

-- theme: tuple
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3
