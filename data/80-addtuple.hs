pair1 = (1, "apple")
pair2 = (2, "banana")
concatenated = pair1 ++ pair2

myTuple1 = (1, "hello")           -- Tuple of two elements
myTuple2 = (42, "world", True)    -- Tuple of three elements

myPair = (3, "apple")
firstElement = fst myPair         -- Access the first element (3)
secondElement = snd myPair        -- Access the second element ("apple")


list1 = [1, 2, 3]
list2 = ["apple", "banana", "cherry"]
zipped = zip list1 list2

-- theme: tuple
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 2
