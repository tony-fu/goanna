monadAdd :: Int -> Int -> Maybe Int
monadAdd x y= do
  a <- Just (x + 3)
  b <- Just (y + 4)
  if a + b > 10
    then return Nothing
    else return (Just (a * b))

-- theme: monad
-- ghc_accurate: 2
-- helium_accurate: 2
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 4
