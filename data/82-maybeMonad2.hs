monadAdd :: Int -> Int -> Maybe Int
monadAdd x y= do
  a <- Just (x + 3)
  b <- Just (y + 4)
  if a + b > 10
    then a
    else b

-- theme: monad
-- ghc_accurate: 0
-- helium_accurate: 1
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 3
