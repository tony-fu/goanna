import Control.Monad.State

x :: State Int Int
x = do
  put '3'
  v <- get
  return v

-- theme: monad
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 2
-- goanna_3: 2
-- goanna_total: 3
