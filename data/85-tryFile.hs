findFirstExe :: [String] -> IO (Maybe String, [String])
findFirstExe = findFirstExe' []

findFirstExe' fs' []     = return ("", reverse fs')
findFirstExe' fs' (f:fs) = do
  isExe <- doesExecutableExist f
  if isExe
    then return (f, reverse fs')
    else findFirstExe' (f:fs') fs

doesExecutableExist :: String -> IO Bool
doesExecutableExist _ = do
  return True

-- theme: monad
-- ghc_accurate: 0
-- helium_accurate: 0
-- goanna_1: 0
-- goanna_3: 2
-- goanna_total: 6
