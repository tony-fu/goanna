{% for cls in classes %}
{{ cls.name }}(T) :-
    T = has(Class, _),
    member1({{ cls.name }}, Class),
    {%- for super_class in cls.super_classes %}
    {{super_class }}(T),
    {%- endfor %}
    true.

{% for rules in cls.instances %}
{{ cls.name }}(T) :-
    nonvar(T),
    {%- for rule in rules %}
    {{ rule }},
    {%- endfor -%}
    {%- for super_class in cls.super_classes %}
    {{super_class }}(T),
    {%- endfor %}
    true.
{% endfor %}
{% endfor %}

