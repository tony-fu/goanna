:- set_prolog_flag(occurs_check, false).
:- set_prolog_flag(autoload, false).
:- style_check(-singleton).
:- use_module(library(assoc)).
:- use_module(library(lists)).

merge_assoc([], _, _).
merge_assoc([Key|Keys], A, B) :-
  get_assoc(Key, A, X),
  get_assoc(Key, B, X),
  merge_assoc(Keys, A, B).

merge_assoc(A, B):- var(A),!, A = B.
merge_assoc(A, B):-
  assoc_to_keys(A, Keys),
  merge_assoc(Keys, A, B).

member1(L,[L|_]) :- !.
member1(L,[_|RS]) :- member1(L,RS).

test_class([with(Class, Instance)|XS]) :-
    nonvar(Class),
    !,
    call(Class, Instance),
    test_class(XS).
test_class(_).

cons(T, _, _, _, _, _) :-
    T = pair(function(A), B),
    B = pair(function(C), D),
    C = pair(list, A),
    D = pair(list, A).
v14(_, Calls, _, _, _, _) :- member(v14, Calls), !.
v14(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v14 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _150,once(member(with(eq, _a), Classes)),_146 = _a,_147 = _a,_148 = bool,_149 = pair(function(_147), _148),_150 = pair(function(_146), _149),
    true.
v7(_, Calls, _, _, _, _) :- member(v7, Calls), !.
v7(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v7 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = pair(maybe, _a),
    true.
v54(_, Calls, _, _, _, _) :- member(v54, Calls), !.
v54(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v54 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _512,once(member(with(num, _a), Classes)),_508 = _a,_509 = _a,_510 = _a,_511 = pair(function(_509), _510),_512 = pair(function(_508), _511),
    true.
v48(_, Calls, _, _, _, _) :- member(v48, Calls), !.
v48(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v48 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig36, Gamma, T),_449 = _a,_450 = pair(list, _449),_451 = _a,_452 = pair(list, _451),_453 = pair(function(_450), _452),
    true.
v16(_, Calls, _, _, _, _) :- member(v16, Calls), !.
v16(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v16 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = ordering,
    true.
v28(_, Calls, _, _, _, _) :- member(v28, Calls), !.
v28(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v28 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig19, Gamma, T),_264 = _a,_265 = char,_266 = pair(list, _265),_267 = pair(function(_264), _266),
    true.
v44(_, Calls, _, _, _, _) :- member(v44, Calls), !.
v44(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v44 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _424,once(member(with(monoid, _a), Classes)),_421 = _a,_422 = pair(list, _421),_423 = _a,_424 = pair(function(_422), _423),
    true.
v49(_, Calls, _, _, _, _) :- member(v49, Calls), !.
v49(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v49 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig37, Gamma, T),_459 = bool,_460 = bool,_461 = bool,_462 = pair(function(_460), _461),_463 = pair(function(_459), _462),
    true.
v23(_, Calls, _, _, _, _) :- member(v23, Calls), !.
v23(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v23 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _194,once(member(with(ord, _a), Classes)),_190 = _a,_191 = _a,_192 = bool,_193 = pair(function(_191), _192),_194 = pair(function(_190), _193),
    true.
v8(_, Calls, _, _, _, _) :- member(v8, Calls), !.
v8(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v8 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,f-_f,b-_b],
    T = _56,once(member(with(functor, _f), Classes)),_46 = _a,_47 = _b,_48 = pair(function(_46), _47),_49 = _f,_50 = _a,_51 = pair(_49, _50),_52 = _f,_53 = _b,_54 = pair(_52, _53),_55 = pair(function(_51), _54),_56 = pair(function(_48), _55),
    true.
v34(_, Calls, _, _, _, _) :- member(v34, Calls), !.
v34(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v34 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig21, Gamma, T),_299 = _a,_300 = _b,_301 = _b,_302 = pair(function(_300), _301),_303 = pair(function(_299), _302),_304 = _b,_305 = _a,_306 = pair(list, _305),_307 = _b,_308 = pair(function(_306), _307),_309 = pair(function(_304), _308),_310 = pair(function(_303), _309),
    true.
v46(_, Calls, _, _, _, _) :- member(v46, Calls), !.
v46(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v46 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _432,once(member(with(monoid, _a), Classes)),_432 = _a,
    true.
v47(_, Calls, _, _, _, _) :- member(v47, Calls), !.
v47(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v47 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig35, Gamma, T),_439 = _a,_440 = _b,_441 = _a,_442 = pair(function(_440), _441),_443 = pair(function(_439), _442),
    true.
v66(_, Calls, _, _, _, _) :- member(v66, Calls), !.
v66(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v66 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig54, Gamma, T),_641 = char,_642 = char,_643 = pair(function(_641), _642),
    true.
v26(_, Calls, _, _, _, _) :- member(v26, Calls), !.
v26(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v26 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig17, Gamma, T),_241 = _a,_242 = bool,_243 = pair(function(_241), _242),_244 = _a,_245 = pair(list, _244),_246 = _a,_247 = pair(list, _246),_248 = pair(function(_245), _247),_249 = pair(function(_243), _248),
    true.
v27(_, Calls, _, _, _, _) :- member(v27, Calls), !.
v27(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v27 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig18, Gamma, T),_255 = char,_256 = pair(list, _255),_257 = _a,_258 = pair(function(_256), _257),
    true.
v29(_, Calls, _, _, _, _) :- member(v29, Calls), !.
v29(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v29 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = pair(function(_276), pair(pair(either, _a), _b)),_276 = _a,
    true.
v53(_, Calls, _, _, _, _) :- member(v53, Calls), !.
v53(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v53 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig41, Gamma, T),_498 = int,_499 = bool,_500 = pair(function(_498), _499),
    true.
v13(_, Calls, _, _, _, _) :- member(v13, Calls), !.
v13(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v13 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [m-_m,b-_b,a-_a],
    T = _114,once(member(with(monad, _m), Classes)),_102 = _m,_103 = _a,_104 = pair(_102, _103),_105 = _a,_106 = _m,_107 = _b,_108 = pair(_106, _107),_109 = pair(function(_105), _108),_110 = _m,_111 = _b,_112 = pair(_110, _111),_113 = pair(function(_109), _112),_114 = pair(function(_104), _113),
    true.
v30(_, Calls, _, _, _, _) :- member(v30, Calls), !.
v30(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v30 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = pair(function(_278), pair(pair(either, _a), _b)),_278 = _b,
    true.
v58(_, Calls, _, _, _, _) :- member(v58, Calls), !.
v58(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v58 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig46, Gamma, T),_547 = int,_548 = int,_549 = int,_550 = pair(function(_548), _549),_551 = pair(function(_547), _550),
    true.
v57(_, Calls, _, _, _, _) :- member(v57, Calls), !.
v57(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v57 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig45, Gamma, T),once(member(with(num, _a), Classes)),_541 = _540,_537 = _a,_538 = pair(list, _537),_539 = _a,_540 = pair(function(_538), _539),
    true.
v33(_, Calls, _, _, _, _) :- member(v33, Calls), !.
v33(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v33 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig20, Gamma, T),_285 = _a,_286 = _b,_287 = pair(function(_285), _286),_288 = _a,_289 = pair(list, _288),_290 = _b,_291 = pair(list, _290),_292 = pair(function(_289), _291),_293 = pair(function(_287), _292),
    true.
v51(_, Calls, _, _, _, _) :- member(v51, Calls), !.
v51(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v51 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig39, Gamma, T),_479 = _a,_480 = _a,_481 = pair(list, _480),_482 = bool,_483 = pair(function(_481), _482),_484 = pair(function(_479), _483),
    true.
v59(_, Calls, _, _, _, _) :- member(v59, Calls), !.
v59(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v59 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _562,once(member(with(enum, _a), Classes)),_559 = _a,_560 = _a,_561 = pair(list, _560),_562 = pair(function(_559), _561),
    true.
v72(_, Calls, _, _, _, _) :- member(v72, Calls), !.
v72(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v72 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig60, Gamma, T),T = _707,_707 = char,T = _712,_712 = _711,get_assoc(lit61, Gamma, _711),_711 = _710,_710 = int,
    true.
v36(_, Calls, _, _, _, _) :- member(v36, Calls), !.
v36(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v36 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig23, Gamma, T),_333 = _a,_334 = pair(list, _333),_335 = _a,_336 = pair(function(_334), _335),
    true.
v17(_, Calls, _, _, _, _) :- member(v17, Calls), !.
v17(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v17 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = ordering,
    true.
v56(_, Calls, _, _, _, _) :- member(v56, Calls), !.
v56(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v56 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _524,once(member(with(num, _a), Classes)),_520 = _a,_521 = _a,_522 = _a,_523 = pair(function(_521), _522),_524 = pair(function(_520), _523),
    true.
v67(_, Calls, _, _, _, _) :- member(v67, Calls), !.
v67(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v67 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig55, Gamma, T),_649 = char,_650 = char,_651 = pair(function(_649), _650),
    true.
v10(_, Calls, _, _, _, _) :- member(v10, Calls), !.
v10(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v10 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,f-_f,b-_b],
    T = _79,once(member(with(applicative, _f), Classes)),_67 = _f,_68 = _a,_69 = _b,_70 = pair(function(_68), _69),_71 = pair(_67, _70),_72 = _f,_73 = _a,_74 = pair(_72, _73),_75 = _f,_76 = _b,_77 = pair(_75, _76),_78 = pair(function(_74), _77),_79 = pair(function(_71), _78),
    true.
v69(_, Calls, _, _, _, _) :- member(v69, Calls), !.
v69(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v69 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig57, Gamma, T),once(member(with(num, _a), Classes)),_674 = _673,_669 = _a,_670 = int,_671 = _a,_672 = pair(function(_670), _671),_673 = pair(function(_669), _672),
    true.
v20(_, Calls, _, _, _, _) :- member(v20, Calls), !.
v20(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v20 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _176,once(member(with(ord, _a), Classes)),_172 = _a,_173 = _a,_174 = bool,_175 = pair(function(_173), _174),_176 = pair(function(_172), _175),
    true.
v35(_, Calls, _, _, _, _) :- member(v35, Calls), !.
v35(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v35 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig22, Gamma, T),_316 = _b,_317 = _a,_318 = _b,_319 = pair(function(_317), _318),_320 = pair(function(_316), _319),_321 = _b,_322 = _a,_323 = pair(list, _322),_324 = _b,_325 = pair(function(_323), _324),_326 = pair(function(_321), _325),_327 = pair(function(_320), _326),
    true.
v22(_, Calls, _, _, _, _) :- member(v22, Calls), !.
v22(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v22 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _188,once(member(with(ord, _a), Classes)),_184 = _a,_185 = _a,_186 = bool,_187 = pair(function(_185), _186),_188 = pair(function(_184), _187),
    true.
v24(_, Calls, _, _, _, _) :- member(v24, Calls), !.
v24(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v24 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _200,once(member(with(ord, _a), Classes)),_196 = _a,_197 = _a,_198 = _a,_199 = pair(function(_197), _198),_200 = pair(function(_196), _199),
    true.
v64(_, Calls, _, _, _, _) :- member(v64, Calls), !.
v64(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v64 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig52, Gamma, T),_617 = float,_618 = float,_619 = float,_620 = pair(function(_618), _619),_621 = pair(function(_617), _620),
    true.
v68(_, Calls, _, _, _, _) :- member(v68, Calls), !.
v68(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v68 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig56, Gamma, T),_657 = float,_658 = float,_659 = pair(function(_657), _658),
    true.
v45(_, Calls, _, _, _, _) :- member(v45, Calls), !.
v45(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v45 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _430,once(member(with(monoid, _a), Classes)),_426 = _a,_427 = _a,_428 = _a,_429 = pair(function(_427), _428),_430 = pair(function(_426), _429),
    true.
v65(_, Calls, _, _, _, _) :- member(v65, Calls), !.
v65(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v65 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig53, Gamma, T),_627 = _a,_628 = bool,_629 = pair(function(_627), _628),_630 = _a,_631 = pair(list, _630),_632 = _a,_633 = pair(list, _632),_634 = pair(function(_631), _633),_635 = pair(function(_629), _634),
    true.
v43(_, Calls, _, _, _, _) :- member(v43, Calls), !.
v43(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v43 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig31, Gamma, T),_411 = bool,_412 = bool,_413 = pair(function(_411), _412),
    true.
v37(_, Calls, _, _, _, _) :- member(v37, Calls), !.
v37(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v37 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig24, Gamma, T),_342 = _a,_343 = pair(list, _342),_344 = _a,_345 = pair(list, _344),_346 = pair(function(_343), _345),
    true.
v18(_, Calls, _, _, _, _) :- member(v18, Calls), !.
v18(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v18 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = ordering,
    true.
v21(_, Calls, _, _, _, _) :- member(v21, Calls), !.
v21(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v21 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _182,once(member(with(ord, _a), Classes)),_178 = _a,_179 = _a,_180 = bool,_181 = pair(function(_179), _180),_182 = pair(function(_178), _181),
    true.
v11(_, Calls, _, _, _, _) :- member(v11, Calls), !.
v11(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v11 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,m-_m],
    T = _88,once(member(with(monad, _m), Classes)),_84 = _a,_85 = _m,_86 = _a,_87 = pair(_85, _86),_88 = pair(function(_84), _87),
    true.
v61(_, Calls, _, _, _, _) :- member(v61, Calls), !.
v61(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v61 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig49, Gamma, T),_580 = bool,_581 = pair(list, _580),_582 = bool,_583 = pair(function(_581), _582),
    true.
v52(_, Calls, _, _, _, _) :- member(v52, Calls), !.
v52(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v52 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig40, Gamma, T),_490 = int,_491 = bool,_492 = pair(function(_490), _491),
    true.
v2(_, Calls, _, _, _, _) :- member(v2, Calls), !.
v2(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v2 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig1, Gamma, T),_21 = _a,_22 = _a,_23 = pair(function(_21), _22),
    true.
v71(_, Calls, _, _, _, _) :- member(v71, Calls), !.
v71(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v71 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig59, Gamma, T),once(member(with(num, _a), Classes)),_700 = _699,_697 = _a,_698 = int,_699 = pair(function(_697), _698),
    true.
v1(_, Calls, _, _, _, _) :- member(v1, Calls), !.
v1(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v1 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig0, Gamma, T),_12 = _a,_13 = pair(list, _12),_14 = int,_15 = pair(function(_13), _14),
    true.
v5(_, Calls, _, _, _, _) :- member(v5, Calls), !.
v5(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v5 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = pair(function(_35), pair(io, _a)),_35 = _a,
    true.
v42(_, Calls, _, _, _, _) :- member(v42, Calls), !.
v42(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v42 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig29, Gamma, T),_404 = float,T = _409,_409 = _408,get_assoc(lit30, Gamma, _408),_407 = float,
    true.
v4(_, Calls, _, _, _, _) :- member(v4, Calls), !.
v4(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v4 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = bool,
    true.
v63(_, Calls, _, _, _, _) :- member(v63, Calls), !.
v63(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v63 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig51, Gamma, T),once(member(with(num, _a), Classes)),_611 = _610,_608 = int,_609 = _a,_610 = pair(function(_608), _609),
    true.
v3(_, Calls, _, _, _, _) :- member(v3, Calls), !.
v3(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v3 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = bool,
    true.
v19(_, Calls, _, _, _, _) :- member(v19, Calls), !.
v19(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v19 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _170,once(member(with(ord, _a), Classes)),_166 = _a,_167 = _a,_168 = ordering,_169 = pair(function(_167), _168),_170 = pair(function(_166), _169),
    true.
v31(_, Calls, _, _, _, _) :- member(v31, Calls), !.
v31(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v31 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = _283,_283 = _282,get_assoc(v32, Gamma, _282),
    true.
v6(_, Calls, _, _, _, _) :- member(v6, Calls), !.
v6(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v6 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    T = pair(function(_40), pair(maybe, _a)),_40 = _a,
    true.
v15(_, Calls, _, _, _, _) :- member(v15, Calls), !.
v15(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v15 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _156,once(member(with(eq, _a), Classes)),_152 = _a,_153 = _a,_154 = bool,_155 = pair(function(_153), _154),_156 = pair(function(_152), _155),
    true.
v12(_, Calls, _, _, _, _) :- member(v12, Calls), !.
v12(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v12 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [m-_m,b-_b,a-_a],
    T = _100,once(member(with(monad, _m), Classes)),_90 = _m,_91 = _a,_92 = pair(_90, _91),_93 = _m,_94 = _b,_95 = pair(_93, _94),_96 = _m,_97 = _b,_98 = pair(_96, _97),_99 = pair(function(_95), _98),_100 = pair(function(_92), _99),
    true.
v50(_, Calls, _, _, _, _) :- member(v50, Calls), !.
v50(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v50 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig38, Gamma, T),_469 = bool,_470 = bool,_471 = bool,_472 = pair(function(_470), _471),_473 = pair(function(_469), _472),
    true.
v70(_, Calls, _, _, _, _) :- member(v70, Calls), !.
v70(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v70 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig58, Gamma, T),once(member(with(num, _a), Classes)),_687 = _686,_684 = _a,_685 = int,_686 = pair(function(_684), _685),
    true.
v41(_, Calls, _, _, _, _) :- member(v41, Calls), !.
v41(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v41 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    get_assoc(sig28, Gamma, T),_391 = _a,_392 = pair(list, _391),_393 = _a,_394 = pair(list, _393),_395 = _a,_396 = pair(list, _395),_397 = pair(function(_394), _396),_398 = pair(function(_392), _397),
    true.
v25(_, Calls, _, _, _, _) :- member(v25, Calls), !.
v25(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v25 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _206,once(member(with(ord, _a), Classes)),_202 = _a,_203 = _a,_204 = _a,_205 = pair(function(_203), _204),_206 = pair(function(_202), _205),
    true.
v62(_, Calls, _, _, _, _) :- member(v62, Calls), !.
v62(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v62 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig50, Gamma, T),_589 = _a,_590 = pair(list, _589),_591 = _b,_592 = pair(list, _591),_595 = pair(tuple(_593), _594),_593 = _a,_594 = _b,_596 = pair(list, _595),_597 = pair(function(_592), _596),_598 = pair(function(_590), _597),
    true.
v9(_, Calls, _, _, _, _) :- member(v9, Calls), !.
v9(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v9 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,f-_f],
    T = _65,once(member(with(applicative, _f), Classes)),_61 = _a,_62 = _f,_63 = _a,_64 = pair(_62, _63),_65 = pair(function(_61), _64),
    true.
v39(_, Calls, _, _, _, _) :- member(v39, Calls), !.
v39(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v39 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig26, Gamma, T),_373 = pair(tuple(_371), _372),_371 = _a,_372 = _b,_374 = _a,_375 = pair(function(_373), _374),
    true.
v55(_, Calls, _, _, _, _) :- member(v55, Calls), !.
v55(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v55 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    T = _518,once(member(with(num, _a), Classes)),_514 = _a,_515 = _a,_516 = _a,_517 = pair(function(_515), _516),_518 = pair(function(_514), _517),
    true.
v38(_, Calls, _, _, _, _) :- member(v38, Calls), !.
v38(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v38 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,c-_c,b-_b],
    get_assoc(sig25, Gamma, T),_352 = _a,_353 = _b,_354 = _c,_355 = pair(function(_353), _354),_356 = pair(function(_352), _355),_357 = _a,_358 = pair(list, _357),_359 = _b,_360 = pair(list, _359),_361 = _c,_362 = pair(list, _361),_363 = pair(function(_360), _362),_364 = pair(function(_358), _363),_365 = pair(function(_356), _364),
    true.
v40(_, Calls, _, _, _, _) :- member(v40, Calls), !.
v40(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v40 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a,b-_b],
    get_assoc(sig27, Gamma, T),_383 = pair(tuple(_381), _382),_381 = _a,_382 = _b,_384 = _b,_385 = pair(function(_383), _384),
    true.
v60(_, Calls, _, _, _, _) :- member(v60, Calls), !.
v60(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v60 | Calls],
    copy_term(Gamma, Gamma_),
    list_to_assoc([], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [],
    get_assoc(sig48, Gamma, T),_571 = bool,_572 = pair(list, _571),_573 = bool,_574 = pair(function(_572), _573),
    true.
functor(T) :-
    T = has(Class, _),
    member1(functor, Class),
    true.

functor(T) :-
    nonvar(T),
    T = _117,
    _117 = maybe,
    true.

functor(T) :-
    nonvar(T),
    T = _120,
    _120 = pair(list, _),
    true.

functor(T) :-
    nonvar(T),
    T = _123,
    _123 = io,
    true.
applicative(T) :-
    T = has(Class, _),
    member1(applicative, Class),
    true.

applicative(T) :-
    nonvar(T),
    T = _126,
    _126 = maybe,
    true.

applicative(T) :-
    nonvar(T),
    T = _129,
    _129 = pair(list, _),
    true.

applicative(T) :-
    nonvar(T),
    T = _132,
    _132 = io,
    true.
monad(T) :-
    T = has(Class, _),
    member1(monad, Class),
    true.

monad(T) :-
    nonvar(T),
    T = _135,
    _135 = maybe,
    true.

monad(T) :-
    nonvar(T),
    T = _138,
    _138 = pair(list, _),
    true.

monad(T) :-
    nonvar(T),
    T = _141,
    _141 = io,
    true.
eq(T) :-
    T = has(Class, _),
    member1(eq, Class),
    true.

eq(T) :-
    nonvar(T),
    T = _209,
    _209 = int,
    true.

eq(T) :-
    nonvar(T),
    T = _212,
    _212 = bool,
    true.

eq(T) :-
    nonvar(T),
    T = _215,
    _215 = ordering,
    true.

eq(T) :-
    nonvar(T),
    T = _218,
    _218 = float,
    true.

eq(T) :-
    nonvar(T),
    T = _221,
    _221 = char,
    true.

eq(T) :-
    nonvar(T),
    T = _229,
    _228 = _a,
    _229 = pair(list, _228),
    eq(_a),
    true.
ord(T) :-
    T = has(Class, _),
    member1(ord, Class),
    true.

ord(T) :-
    nonvar(T),
    T = _232,
    _232 = int,
    true.

ord(T) :-
    nonvar(T),
    T = _235,
    _235 = bool,
    true.

ord(T) :-
    nonvar(T),
    T = _238,
    _238 = char,
    true.
monoid(T) :-
    T = has(Class, _),
    member1(monoid, Class),
    true.

monoid(T) :-
    nonvar(T),
    T = _436,
    _435 = _a,
    _436 = pair(list, _435),
    true.
num(T) :-
    T = has(Class, _),
    member1(num, Class),
    true.

num(T) :-
    nonvar(T),
    T = _527,
    _527 = int,
    true.

num(T) :-
    nonvar(T),
    T = _530,
    _530 = float,
    true.
enum(T) :-
    T = has(Class, _),
    member1(enum, Class),
    true.

enum(T) :-
    nonvar(T),
    T = _565,
    _565 = int,
    true.

enum(T) :-
    nonvar(T),
    T = _568,
    _568 = char,
    true.


type_check :-
    once((
        list_to_assoc([sig0-_,sig1-_,sig2-_,sig3-_,sig4-_,sig5-_,sig6-_,sig7-_,sig8-_,sig9-_,sig10-_,sig11-_,sig12-_,sig13-_,sig14-_,sig15-_,sig16-_,sig17-_,sig18-_,sig19-_,sig20-_,sig21-_,sig22-_,sig23-_,sig24-_,sig25-_,sig26-_,sig27-_,sig28-_,sig29-_,lit30-_,sig31-_,sig32-_,sig33-_,sig34-_,sig35-_,sig36-_,sig37-_,sig38-_,sig39-_,sig40-_,sig41-_,sig42-_,sig43-_,sig44-_,sig45-_,sig46-_,sig47-_,sig48-_,sig49-_,sig50-_,sig51-_,sig52-_,sig53-_,sig54-_,sig55-_,sig56-_,sig57-_,sig58-_,sig59-_,sig60-_,lit61-_,v14-_,v7-_,v54-_,v48-_,v16-_,v28-_,v44-_,v49-_,v23-_,v8-_,v34-_,v46-_,v47-_,v66-_,v26-_,v27-_,v29-_,v53-_,v13-_,v30-_,v58-_,v57-_,v33-_,v51-_,v59-_,v72-_,v36-_,v17-_,v56-_,v67-_,v10-_,v69-_,v20-_,v35-_,v22-_,v24-_,v64-_,v68-_,v45-_,v65-_,v43-_,v37-_,v18-_,v21-_,v11-_,v61-_,v52-_,v2-_,v71-_,v1-_,v5-_,v42-_,v4-_,v63-_,v3-_,v19-_,v31-_,v6-_,v15-_,v12-_,v50-_,v70-_,v41-_,v25-_,v62-_,v9-_,v39-_,v55-_,v38-_,v40-_,v60-_,v32-_], Gamma),
        copy_term(Gamma, Gamma_v14),
        v14(_, [], Gamma_v14,  _,[a-has([eq], a)], Classes_v14),
        copy_term(Gamma, Gamma_v7),
        v7(_, [], Gamma_v7,  _,[], Classes_v7),
        copy_term(Gamma, Gamma_v54),
        v54(_, [], Gamma_v54,  _,[a-has([num], a)], Classes_v54),
        copy_term(Gamma, Gamma_v48),
        v48(_, [], Gamma_v48,  _,[a-has([], a)], Classes_v48),
        copy_term(Gamma, Gamma_v16),
        v16(_, [], Gamma_v16,  _,[], Classes_v16),
        copy_term(Gamma, Gamma_v28),
        v28(_, [], Gamma_v28,  _,[a-has([], a),b-has([], b)], Classes_v28),
        copy_term(Gamma, Gamma_v44),
        v44(_, [], Gamma_v44,  _,[a-has([monoid], a)], Classes_v44),
        copy_term(Gamma, Gamma_v49),
        v49(_, [], Gamma_v49,  _,[], Classes_v49),
        copy_term(Gamma, Gamma_v23),
        v23(_, [], Gamma_v23,  _,[a-has([ord], a)], Classes_v23),
        copy_term(Gamma, Gamma_v8),
        v8(_, [], Gamma_v8,  _,[a-has([], a),f-has([functor], f),b-has([], b)], Classes_v8),
        copy_term(Gamma, Gamma_v34),
        v34(_, [], Gamma_v34,  _,[a-has([], a),b-has([], b)], Classes_v34),
        copy_term(Gamma, Gamma_v46),
        v46(_, [], Gamma_v46,  _,[a-has([monoid], a)], Classes_v46),
        copy_term(Gamma, Gamma_v47),
        v47(_, [], Gamma_v47,  _,[a-has([], a),b-has([], b)], Classes_v47),
        copy_term(Gamma, Gamma_v66),
        v66(_, [], Gamma_v66,  _,[], Classes_v66),
        copy_term(Gamma, Gamma_v26),
        v26(_, [], Gamma_v26,  _,[a-has([], a)], Classes_v26),
        copy_term(Gamma, Gamma_v27),
        v27(_, [], Gamma_v27,  _,[a-has([], a)], Classes_v27),
        copy_term(Gamma, Gamma_v29),
        v29(_, [], Gamma_v29,  _,[], Classes_v29),
        copy_term(Gamma, Gamma_v53),
        v53(_, [], Gamma_v53,  _,[], Classes_v53),
        copy_term(Gamma, Gamma_v13),
        v13(_, [], Gamma_v13,  _,[m-has([monad], m),b-has([], b),a-has([], a)], Classes_v13),
        copy_term(Gamma, Gamma_v30),
        v30(_, [], Gamma_v30,  _,[], Classes_v30),
        copy_term(Gamma, Gamma_v58),
        v58(_, [], Gamma_v58,  _,[], Classes_v58),
        copy_term(Gamma, Gamma_v57),
        v57(_, [], Gamma_v57,  _,[a-has([num], a)], Classes_v57),
        copy_term(Gamma, Gamma_v33),
        v33(_, [], Gamma_v33,  _,[a-has([], a),b-has([], b)], Classes_v33),
        copy_term(Gamma, Gamma_v51),
        v51(_, [], Gamma_v51,  _,[a-has([], a)], Classes_v51),
        copy_term(Gamma, Gamma_v59),
        v59(_, [], Gamma_v59,  _,[a-has([enum], a)], Classes_v59),
        copy_term(Gamma, Gamma_v72),
        v72(_, [], Gamma_v72,  _,[], Classes_v72),
        copy_term(Gamma, Gamma_v36),
        v36(_, [], Gamma_v36,  _,[a-has([], a)], Classes_v36),
        copy_term(Gamma, Gamma_v17),
        v17(_, [], Gamma_v17,  _,[], Classes_v17),
        copy_term(Gamma, Gamma_v56),
        v56(_, [], Gamma_v56,  _,[a-has([num], a)], Classes_v56),
        copy_term(Gamma, Gamma_v67),
        v67(_, [], Gamma_v67,  _,[], Classes_v67),
        copy_term(Gamma, Gamma_v10),
        v10(_, [], Gamma_v10,  _,[a-has([], a),f-has([applicative], f),b-has([], b)], Classes_v10),
        copy_term(Gamma, Gamma_v69),
        v69(_, [], Gamma_v69,  _,[a-has([num], a)], Classes_v69),
        copy_term(Gamma, Gamma_v20),
        v20(_, [], Gamma_v20,  _,[a-has([ord], a)], Classes_v20),
        copy_term(Gamma, Gamma_v35),
        v35(_, [], Gamma_v35,  _,[a-has([], a),b-has([], b)], Classes_v35),
        copy_term(Gamma, Gamma_v22),
        v22(_, [], Gamma_v22,  _,[a-has([ord], a)], Classes_v22),
        copy_term(Gamma, Gamma_v24),
        v24(_, [], Gamma_v24,  _,[a-has([ord], a)], Classes_v24),
        copy_term(Gamma, Gamma_v64),
        v64(_, [], Gamma_v64,  _,[], Classes_v64),
        copy_term(Gamma, Gamma_v68),
        v68(_, [], Gamma_v68,  _,[], Classes_v68),
        copy_term(Gamma, Gamma_v45),
        v45(_, [], Gamma_v45,  _,[a-has([monoid], a)], Classes_v45),
        copy_term(Gamma, Gamma_v65),
        v65(_, [], Gamma_v65,  _,[a-has([], a)], Classes_v65),
        copy_term(Gamma, Gamma_v43),
        v43(_, [], Gamma_v43,  _,[], Classes_v43),
        copy_term(Gamma, Gamma_v37),
        v37(_, [], Gamma_v37,  _,[a-has([], a)], Classes_v37),
        copy_term(Gamma, Gamma_v18),
        v18(_, [], Gamma_v18,  _,[], Classes_v18),
        copy_term(Gamma, Gamma_v21),
        v21(_, [], Gamma_v21,  _,[a-has([ord], a)], Classes_v21),
        copy_term(Gamma, Gamma_v11),
        v11(_, [], Gamma_v11,  _,[a-has([], a),m-has([monad], m)], Classes_v11),
        copy_term(Gamma, Gamma_v61),
        v61(_, [], Gamma_v61,  _,[], Classes_v61),
        copy_term(Gamma, Gamma_v52),
        v52(_, [], Gamma_v52,  _,[], Classes_v52),
        copy_term(Gamma, Gamma_v2),
        v2(_, [], Gamma_v2,  _,[a-has([], a)], Classes_v2),
        copy_term(Gamma, Gamma_v71),
        v71(_, [], Gamma_v71,  _,[a-has([num], a)], Classes_v71),
        copy_term(Gamma, Gamma_v1),
        v1(_, [], Gamma_v1,  _,[a-has([], a)], Classes_v1),
        copy_term(Gamma, Gamma_v5),
        v5(_, [], Gamma_v5,  _,[], Classes_v5),
        copy_term(Gamma, Gamma_v42),
        v42(_, [], Gamma_v42,  _,[], Classes_v42),
        copy_term(Gamma, Gamma_v4),
        v4(_, [], Gamma_v4,  _,[], Classes_v4),
        copy_term(Gamma, Gamma_v63),
        v63(_, [], Gamma_v63,  _,[a-has([num], a)], Classes_v63),
        copy_term(Gamma, Gamma_v3),
        v3(_, [], Gamma_v3,  _,[], Classes_v3),
        copy_term(Gamma, Gamma_v19),
        v19(_, [], Gamma_v19,  _,[a-has([ord], a)], Classes_v19),
        copy_term(Gamma, Gamma_v31),
        v31(_, [], Gamma_v31,  _,[], Classes_v31),
        copy_term(Gamma, Gamma_v6),
        v6(_, [], Gamma_v6,  _,[], Classes_v6),
        copy_term(Gamma, Gamma_v15),
        v15(_, [], Gamma_v15,  _,[a-has([eq], a)], Classes_v15),
        copy_term(Gamma, Gamma_v12),
        v12(_, [], Gamma_v12,  _,[m-has([monad], m),b-has([], b),a-has([], a)], Classes_v12),
        copy_term(Gamma, Gamma_v50),
        v50(_, [], Gamma_v50,  _,[], Classes_v50),
        copy_term(Gamma, Gamma_v70),
        v70(_, [], Gamma_v70,  _,[a-has([num], a)], Classes_v70),
        copy_term(Gamma, Gamma_v41),
        v41(_, [], Gamma_v41,  _,[a-has([], a)], Classes_v41),
        copy_term(Gamma, Gamma_v25),
        v25(_, [], Gamma_v25,  _,[a-has([ord], a)], Classes_v25),
        copy_term(Gamma, Gamma_v62),
        v62(_, [], Gamma_v62,  _,[a-has([], a),b-has([], b)], Classes_v62),
        copy_term(Gamma, Gamma_v9),
        v9(_, [], Gamma_v9,  _,[a-has([], a),f-has([applicative], f)], Classes_v9),
        copy_term(Gamma, Gamma_v39),
        v39(_, [], Gamma_v39,  _,[a-has([], a),b-has([], b)], Classes_v39),
        copy_term(Gamma, Gamma_v55),
        v55(_, [], Gamma_v55,  _,[a-has([num], a)], Classes_v55),
        copy_term(Gamma, Gamma_v38),
        v38(_, [], Gamma_v38,  _,[a-has([], a),c-has([], c),b-has([], b)], Classes_v38),
        copy_term(Gamma, Gamma_v40),
        v40(_, [], Gamma_v40,  _,[a-has([], a),b-has([], b)], Classes_v40),
        copy_term(Gamma, Gamma_v60),
        v60(_, [], Gamma_v60,  _,[], Classes_v60),
        true
    )),
    test_class(Classes_v14),
    test_class(Classes_v7),
    test_class(Classes_v54),
    test_class(Classes_v48),
    test_class(Classes_v16),
    test_class(Classes_v28),
    test_class(Classes_v44),
    test_class(Classes_v49),
    test_class(Classes_v23),
    test_class(Classes_v8),
    test_class(Classes_v34),
    test_class(Classes_v46),
    test_class(Classes_v47),
    test_class(Classes_v66),
    test_class(Classes_v26),
    test_class(Classes_v27),
    test_class(Classes_v29),
    test_class(Classes_v53),
    test_class(Classes_v13),
    test_class(Classes_v30),
    test_class(Classes_v58),
    test_class(Classes_v57),
    test_class(Classes_v33),
    test_class(Classes_v51),
    test_class(Classes_v59),
    test_class(Classes_v72),
    test_class(Classes_v36),
    test_class(Classes_v17),
    test_class(Classes_v56),
    test_class(Classes_v67),
    test_class(Classes_v10),
    test_class(Classes_v69),
    test_class(Classes_v20),
    test_class(Classes_v35),
    test_class(Classes_v22),
    test_class(Classes_v24),
    test_class(Classes_v64),
    test_class(Classes_v68),
    test_class(Classes_v45),
    test_class(Classes_v65),
    test_class(Classes_v43),
    test_class(Classes_v37),
    test_class(Classes_v18),
    test_class(Classes_v21),
    test_class(Classes_v11),
    test_class(Classes_v61),
    test_class(Classes_v52),
    test_class(Classes_v2),
    test_class(Classes_v71),
    test_class(Classes_v1),
    test_class(Classes_v5),
    test_class(Classes_v42),
    test_class(Classes_v4),
    test_class(Classes_v63),
    test_class(Classes_v3),
    test_class(Classes_v19),
    test_class(Classes_v31),
    test_class(Classes_v6),
    test_class(Classes_v15),
    test_class(Classes_v12),
    test_class(Classes_v50),
    test_class(Classes_v70),
    test_class(Classes_v41),
    test_class(Classes_v25),
    test_class(Classes_v62),
    test_class(Classes_v9),
    test_class(Classes_v39),
    test_class(Classes_v55),
    test_class(Classes_v38),
    test_class(Classes_v40),
    test_class(Classes_v60),
    true.

main(G) :-
    once((
        list_to_assoc([sig0-_,sig1-_,sig2-_,sig3-_,sig4-_,sig5-_,sig6-_,sig7-_,sig8-_,sig9-_,sig10-_,sig11-_,sig12-_,sig13-_,sig14-_,sig15-_,sig16-_,sig17-_,sig18-_,sig19-_,sig20-_,sig21-_,sig22-_,sig23-_,sig24-_,sig25-_,sig26-_,sig27-_,sig28-_,sig29-_,lit30-_,sig31-_,sig32-_,sig33-_,sig34-_,sig35-_,sig36-_,sig37-_,sig38-_,sig39-_,sig40-_,sig41-_,sig42-_,sig43-_,sig44-_,sig45-_,sig46-_,sig47-_,sig48-_,sig49-_,sig50-_,sig51-_,sig52-_,sig53-_,sig54-_,sig55-_,sig56-_,sig57-_,sig58-_,sig59-_,sig60-_,lit61-_,v14-_v14,v7-_v7,v54-_v54,v48-_v48,v16-_v16,v28-_v28,v44-_v44,v49-_v49,v23-_v23,v8-_v8,v34-_v34,v46-_v46,v47-_v47,v66-_v66,v26-_v26,v27-_v27,v29-_v29,v53-_v53,v13-_v13,v30-_v30,v58-_v58,v57-_v57,v33-_v33,v51-_v51,v59-_v59,v72-_v72,v36-_v36,v17-_v17,v56-_v56,v67-_v67,v10-_v10,v69-_v69,v20-_v20,v35-_v35,v22-_v22,v24-_v24,v64-_v64,v68-_v68,v45-_v45,v65-_v65,v43-_v43,v37-_v37,v18-_v18,v21-_v21,v11-_v11,v61-_v61,v52-_v52,v2-_v2,v71-_v71,v1-_v1,v5-_v5,v42-_v42,v4-_v4,v63-_v63,v3-_v3,v19-_v19,v31-_v31,v6-_v6,v15-_v15,v12-_v12,v50-_v50,v70-_v70,v41-_v41,v25-_v25,v62-_v62,v9-_v9,v39-_v39,v55-_v55,v38-_v38,v40-_v40,v60-_v60,v32-_], Gamma),
        v14(_v14, [], Gamma,  _, _, Classes_v14),
        v7(_v7, [], Gamma,  _, _, Classes_v7),
        v54(_v54, [], Gamma,  _, _, Classes_v54),
        v48(_v48, [], Gamma,  _, _, Classes_v48),
        v16(_v16, [], Gamma,  _, _, Classes_v16),
        v28(_v28, [], Gamma,  _, _, Classes_v28),
        v44(_v44, [], Gamma,  _, _, Classes_v44),
        v49(_v49, [], Gamma,  _, _, Classes_v49),
        v23(_v23, [], Gamma,  _, _, Classes_v23),
        v8(_v8, [], Gamma,  _, _, Classes_v8),
        v34(_v34, [], Gamma,  _, _, Classes_v34),
        v46(_v46, [], Gamma,  _, _, Classes_v46),
        v47(_v47, [], Gamma,  _, _, Classes_v47),
        v66(_v66, [], Gamma,  _, _, Classes_v66),
        v26(_v26, [], Gamma,  _, _, Classes_v26),
        v27(_v27, [], Gamma,  _, _, Classes_v27),
        v29(_v29, [], Gamma,  _, _, Classes_v29),
        v53(_v53, [], Gamma,  _, _, Classes_v53),
        v13(_v13, [], Gamma,  _, _, Classes_v13),
        v30(_v30, [], Gamma,  _, _, Classes_v30),
        v58(_v58, [], Gamma,  _, _, Classes_v58),
        v57(_v57, [], Gamma,  _, _, Classes_v57),
        v33(_v33, [], Gamma,  _, _, Classes_v33),
        v51(_v51, [], Gamma,  _, _, Classes_v51),
        v59(_v59, [], Gamma,  _, _, Classes_v59),
        v72(_v72, [], Gamma,  _, _, Classes_v72),
        v36(_v36, [], Gamma,  _, _, Classes_v36),
        v17(_v17, [], Gamma,  _, _, Classes_v17),
        v56(_v56, [], Gamma,  _, _, Classes_v56),
        v67(_v67, [], Gamma,  _, _, Classes_v67),
        v10(_v10, [], Gamma,  _, _, Classes_v10),
        v69(_v69, [], Gamma,  _, _, Classes_v69),
        v20(_v20, [], Gamma,  _, _, Classes_v20),
        v35(_v35, [], Gamma,  _, _, Classes_v35),
        v22(_v22, [], Gamma,  _, _, Classes_v22),
        v24(_v24, [], Gamma,  _, _, Classes_v24),
        v64(_v64, [], Gamma,  _, _, Classes_v64),
        v68(_v68, [], Gamma,  _, _, Classes_v68),
        v45(_v45, [], Gamma,  _, _, Classes_v45),
        v65(_v65, [], Gamma,  _, _, Classes_v65),
        v43(_v43, [], Gamma,  _, _, Classes_v43),
        v37(_v37, [], Gamma,  _, _, Classes_v37),
        v18(_v18, [], Gamma,  _, _, Classes_v18),
        v21(_v21, [], Gamma,  _, _, Classes_v21),
        v11(_v11, [], Gamma,  _, _, Classes_v11),
        v61(_v61, [], Gamma,  _, _, Classes_v61),
        v52(_v52, [], Gamma,  _, _, Classes_v52),
        v2(_v2, [], Gamma,  _, _, Classes_v2),
        v71(_v71, [], Gamma,  _, _, Classes_v71),
        v1(_v1, [], Gamma,  _, _, Classes_v1),
        v5(_v5, [], Gamma,  _, _, Classes_v5),
        v42(_v42, [], Gamma,  _, _, Classes_v42),
        v4(_v4, [], Gamma,  _, _, Classes_v4),
        v63(_v63, [], Gamma,  _, _, Classes_v63),
        v3(_v3, [], Gamma,  _, _, Classes_v3),
        v19(_v19, [], Gamma,  _, _, Classes_v19),
        v31(_v31, [], Gamma,  _, _, Classes_v31),
        v6(_v6, [], Gamma,  _, _, Classes_v6),
        v15(_v15, [], Gamma,  _, _, Classes_v15),
        v12(_v12, [], Gamma,  _, _, Classes_v12),
        v50(_v50, [], Gamma,  _, _, Classes_v50),
        v70(_v70, [], Gamma,  _, _, Classes_v70),
        v41(_v41, [], Gamma,  _, _, Classes_v41),
        v25(_v25, [], Gamma,  _, _, Classes_v25),
        v62(_v62, [], Gamma,  _, _, Classes_v62),
        v9(_v9, [], Gamma,  _, _, Classes_v9),
        v39(_v39, [], Gamma,  _, _, Classes_v39),
        v55(_v55, [], Gamma,  _, _, Classes_v55),
        v38(_v38, [], Gamma,  _, _, Classes_v38),
        v40(_v40, [], Gamma,  _, _, Classes_v40),
        v60(_v60, [], Gamma,  _, _, Classes_v60),
        true
    )),
    test_class(Classes_v14),
    test_class(Classes_v7),
    test_class(Classes_v54),
    test_class(Classes_v48),
    test_class(Classes_v16),
    test_class(Classes_v28),
    test_class(Classes_v44),
    test_class(Classes_v49),
    test_class(Classes_v23),
    test_class(Classes_v8),
    test_class(Classes_v34),
    test_class(Classes_v46),
    test_class(Classes_v47),
    test_class(Classes_v66),
    test_class(Classes_v26),
    test_class(Classes_v27),
    test_class(Classes_v29),
    test_class(Classes_v53),
    test_class(Classes_v13),
    test_class(Classes_v30),
    test_class(Classes_v58),
    test_class(Classes_v57),
    test_class(Classes_v33),
    test_class(Classes_v51),
    test_class(Classes_v59),
    test_class(Classes_v72),
    test_class(Classes_v36),
    test_class(Classes_v17),
    test_class(Classes_v56),
    test_class(Classes_v67),
    test_class(Classes_v10),
    test_class(Classes_v69),
    test_class(Classes_v20),
    test_class(Classes_v35),
    test_class(Classes_v22),
    test_class(Classes_v24),
    test_class(Classes_v64),
    test_class(Classes_v68),
    test_class(Classes_v45),
    test_class(Classes_v65),
    test_class(Classes_v43),
    test_class(Classes_v37),
    test_class(Classes_v18),
    test_class(Classes_v21),
    test_class(Classes_v11),
    test_class(Classes_v61),
    test_class(Classes_v52),
    test_class(Classes_v2),
    test_class(Classes_v71),
    test_class(Classes_v1),
    test_class(Classes_v5),
    test_class(Classes_v42),
    test_class(Classes_v4),
    test_class(Classes_v63),
    test_class(Classes_v3),
    test_class(Classes_v19),
    test_class(Classes_v31),
    test_class(Classes_v6),
    test_class(Classes_v15),
    test_class(Classes_v12),
    test_class(Classes_v50),
    test_class(Classes_v70),
    test_class(Classes_v41),
    test_class(Classes_v25),
    test_class(Classes_v62),
    test_class(Classes_v9),
    test_class(Classes_v39),
    test_class(Classes_v55),
    test_class(Classes_v38),
    test_class(Classes_v40),
    test_class(Classes_v60),
    assoc_to_list(Gamma, G).