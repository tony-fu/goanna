:- set_prolog_flag(occurs_check, error).
:- style_check(-singleton).


merge_assoc([], _, _).
merge_assoc([Key|Keys], A, B) :-
  get_assoc(Key, A, X),
  get_assoc(Key, B, Y),
  X = Y,
  merge_assoc(Keys, A, B).

merge_assoc(A, B):-
  assoc_to_keys(A, Keys),
  merge_assoc(Keys, A, B).


member1(L,[L|_]) :- !.
member1(L,[_|RS]) :- member1(L,RS).

test_class([with(Class, Instance)|XS]) :-
    nonvar(Class), nonvar(Instance),
    !,
    call(Class, Instance),
    test_class(XS).
test_class(_).

cons(T, _, _, _, _, _) :-
    T = pair(function(A), B),
    B = pair(function(C), D),
    C = pair(list, A),
    D = pair(list, A).

{% for clause in clauses -%}
{{ clause.head }}(_, Calls, _, Zeta, Theta, Classes) :- Zeta = zeta{}, Theta = theta{}, member({{ clause.head }}, Calls), !.
{{ clause.head }}(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [{{ clause.head }} | Calls],
    copy_term(Gamma, Gamma_),
    Zeta = zeta{
        {%- for arg in fun_args[clause.head] -%}
        {{ arg }}:_{{ ',' if not loop.last }}
        {%- endfor -%}
    },
    Theta = theta{
        {%- for arg in theta[clause.head] -%}
        {{ arg }}:_{{arg}}{{ ',' if not loop.last }}
        {%- endfor -%}
    },
    {% for term in clause.body -%}
        {% if loop.last -%}
            {{ term }}.
        {%- else -%}
            {{ term }},
        {%- endif %}
    {% endfor %}
{% endfor %}