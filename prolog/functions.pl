even(_, CALLS) :- member(even, CALLS), !.
even(T, CALLS) :- odd(T, [even|CALLS]), T = char.

odd(_, CALLS)  :- member(odd, CALLS), !.
odd(T, CALLS)  :- even(T, [odd|CALLS]), T = bool.

len(_, CALLS) :- member(len, CALLS), !.
len(T, CALLS) :-
    CALLS_ = [len|CALLS],
    T = function(A, B), A = list(_), B = int,
    T = function(C, D), C = list(_), XS = C,
    plus(function(X, Y, Z), CALLS_), Z = D, X = int, T = function(XS, Y).
