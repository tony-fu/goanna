:- initialization(main).

type_of_x(T, Gamma):
    T = Gamma.x1.

main:-
    Gamma = gamma{x1: X1, x2: X2, x3: X3},
    type_of_x(T, Gamma).