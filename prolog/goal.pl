
consult('{{ prolog_file }}'),
once((
    Gamma = gamma{
        {%- for k,v in gamma.items() %}
        {{ k }}:_{{ ',' if not loop.last }}
        {%- endfor %}
    },
    {%- for name, var in defined_names %}
    copy_term(Gamma, Gamma_{{name}}),
    {{ name }}(_, [], Gamma_{{name}},  _, theta{
        {%- for arg in theta[name] -%}
        {{ arg }}: has([
            {%- for c in theta_classes[(name, arg)] -%}
            {{ c }}{{ ',' if not loop.last }}
            {%- endfor -%}
        ], {{arg}}){{ ',' if not loop.last }}
        {%- endfor -%}
    }, Classes_{{name}}),
    {%- endfor %}
    {%- for name, var in defined_names %}
    {{ name }}(Gamma.{{ name }}, [], Gamma,  _, _, _),
    {%- endfor %}
    append([
    {%- for name, var in defined_names -%}
        Classes_{{ name }}{{ ',' if not loop.last }}
    {%- endfor -%}
    ], Classes)
)),
test_class(Classes),
true.
