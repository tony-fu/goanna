type_check :-
    once((
        list_to_assoc([
            {%- for k,v in gamma.items() -%}
            {{ k }}-_{{ ',' if not loop.last }}
            {%- endfor -%}
        ], Gamma),
        {%- for name, var in defined_names %}
        copy_term(Gamma, Gamma_{{name}}),
        {{ name }}(_, [], Gamma_{{name}},  _,[
            {%- for arg in theta[name] -%}
            {{ arg }}-has([
                {%- for class in theta_classes[(name, arg)] -%}
                {{ class }}{{ ',' if not loop.last }}
                {%- endfor -%}
            ], {{ arg }}){{ ',' if not loop.last }}
            {%- endfor -%}
        ], Classes_{{name}}),
        {%- endfor %}
        true
    )),
    {%- for name, _ in defined_names %}
    test_class(Classes_{{ name }}),
    {%- endfor %}
    true.

main(G) :-
    once((
        list_to_assoc([
            {%- for k,v in gamma.items() -%}
            {{ k }}-_{{k}}{{ ',' if not loop.last }}
            {%- endfor -%}
        ], Gamma),
        {%- for name, var in defined_names %}
        {{ name }}(_{{name}}, [], Gamma,  _, _, Classes_{{ name }}),
        {%- endfor %}
        true
    )),
    {%- for name, _  in defined_names %}
    test_class(Classes_{{ name }}),
    {%- endfor %}
    assoc_to_list(Gamma, G).
