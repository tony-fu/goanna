from swiplserver import PrologMQI, PrologThread


if __name__ == "__main__":
    with PrologMQI(mqi_traces='_') as mqi:
        with mqi.create_thread() as prolog_thread:
            result = prolog_thread.query("consult('test.pl'), main(G).")
            print(result)

