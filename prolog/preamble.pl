:- set_prolog_flag(occurs_check, true).
:- set_prolog_flag(autoload, false).
:- style_check(-singleton).
:- use_module(library(assoc)).
:- use_module(library(lists)).

merge_assoc([], _, _).
merge_assoc([Key|Keys], A, B) :-
  get_assoc(Key, A, X),
  get_assoc(Key, B, X),
  merge_assoc(Keys, A, B).

merge_assoc(A, B):- var(A),!, A = B.
merge_assoc(A, B):-
  assoc_to_keys(A, Keys),
  merge_assoc(Keys, A, B).

gamma_imply(_, Gamma, _) :- var(Gamma), !.
gamma_imply(Key, Gamma, Value) :- get_assoc(Key, Gamma, Value).

member1(L,[L|_]) :- !.
member1(L,[_|RS]) :- member1(L,RS).

test_class([with(Class, Instance)|XS]) :-
    nonvar(Class),
    !,
    call(Class, Instance),
    test_class(XS).
test_class(_).

cons(T, _, _, _, _, _) :-
    T = pair(function(A), B),
    B = pair(function(C), D),
    C = pair(list, A),
    D = pair(list, A).