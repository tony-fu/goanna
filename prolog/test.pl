:- set_prolog_flag(occurs_check, false).
:- style_check(-singleton).
:- use_module(library(assoc)).
:- use_module(library(lists)).

merge_assoc([], _, _).
merge_assoc([Key|Keys], A, B) :-
  get_assoc(Key, A, X),
  get_assoc(Key, B, X),
  merge_assoc(Keys, A, B).

merge_assoc(A, B):- var(A),!, A = B.
merge_assoc(A, B):-
  assoc_to_keys(A, Keys),
  merge_assoc(Keys, A, B).

gamma_imply(_, Gamma, _) :- var(Gamma), !.
gamma_imply(Key, Gamma, Value) :- get_assoc(Key, Gamma, Value).

member1(L,[L|_]) :- !.
member1(L,[_|RS]) :- member1(L,RS).


test_class(_).

cons(T, _, _, _, _, _) :-
    T = pair(function(A), B),
    B = pair(function(C), D),
    C = pair(list, A),
    D = pair(list, A).
v1(_, Calls, _, _, _, _) :- member(v1, Calls), !.
v1(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [v1 | Calls],
    list_to_assoc([v3-_,v5-_], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [a-_a],
    gamma_imply(sig0, Gamma, T),
    _2 = _a,
    _3 = pair(list, _2),
    _4 = _a,
    _5 = pair(list, _4),
    _6 = pair(function(_3), _5),
    T = _19,
    _19 = _18,
    gamma_imply(v5, Gamma, _8),
    _8 = _f1,
    gamma_imply(v3, Gamma, _9),
    _9 = pair(list, _f1),
    get_assoc(v5, Zeta, _8),
    get_assoc(v3, Zeta, _9),
    _18 = pair(function(_10), _15),
    gamma_imply(v2, Gamma, _13),
    cons(_13, Calls_, Gamma, _, _, Classes),
    get_assoc(v3, Zeta, _11),
    pair(function(_11), _14) = _13,
    get_assoc(v5, Zeta, _12),
    pair(function(_12), _15) = _14,

    true.

type_check :-
    once((
        v1(_, [], _,  _,[a-has([], a)], Classes_v1),
        true
    )),
    true.

main(G) :-
    list_to_assoc([
        cons-_,sig0-_,parent1-_,v1-_v1,v3-_,v5-_,v2-_], Gamma),
    v1(_v1, [], Gamma,  _, _, Classes_v1),
    assoc_to_list(Gamma, G).