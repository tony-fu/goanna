member1(L,[L|_]) :- !.
member1(L,[_|RS]) :- member1(L,RS).


v1(T, _, _, _, _, Classes) :-
    _22 = T,
    member(with(eq, _a), Classes),
    _22 = _21,
    _19 = _a,
    _20 = unit,
    _21 = pair(function(_19), _20).

v2(T, _, _, _, _, Classes) :-
    T = _33,
    _33 = _32,
    v1(_29, _, _, _, _, Classes),
    _31 = _30,
    _30 = int,
    pair(function(_31), _32) = _29.

v4(T, _, _, _, _, Classes) :-
    T = _40,
    _40 = _39,
    v1(_36, _, _, _, _, Classes),
    _38 = _37,
    _37 = float,
    pair(function(_38), _39) = _36.

eq(T) :-
    T = has(Class, _),
    member1(eq, Class),
    true.

eq(T) :-
    nonvar(T),
    T = _9,
    _9 = int,
    true.
eq(T) :-
    nonvar(T),
    T = _9,
    _9 = float,
    true.


test_class([with(Class, Instance)|XS]) :-
    nonvar(Class), nonvar(Instance),
    !,
    call(Class, Instance),
    test_class(XS).
test_class(_).