member1(L,[L|_]) :- !.
member1(L,[_|RS]) :- member1(L,RS).


eq(T) :- T = has(Class, _), member1(eq, Class).
eq(T) :- T = int, !.
eq(T) :- T = char, !.
eq(T) :- T = float, !.
eq(T) :- T = list(X), eq(X), !.


ord(T) :- T = has(Class, _), member1(ord, Class), eq(T).
ord(T) :- T = int, eq(T), !.
ord(T) :- T = char, eq(T), !.
ord(T) :- T = float, eq(T), !.
ord(T) :- T = list(X), ord(X), eq(T), !.

% shoudl succeed
gt(T) :-
    T = function(T1, T1, T1),
    ord(T1).

% should succeed
eqeq(T) :-
    T = function(T1, T1, T1),
    eq(T1).

% should succeed
mygt(T) :-
    T = function(T1, T1, T1),
    gt(T).

% should fail
mygt2(T) :-
    T = function(T1, T2, T1),
    T1 = has([ord, eq], a),
    T2 = has([ord, eq], b),
    gt(T).


% should fail
has_eq(T) :-
    T = function(T1, T1, T1),
    T1 = has([eq], _),
    gt(T).

% should succeed
has_ord(T) :-
    T = function(T1, T1, T1),
    T1 = has([ord, eq], a),
    gt(T).

% should succeed
need_eq(T) :-
    T = function(T1, T1, T1),
    T1 = has([ord, eq], a),
    eqeq(T).
