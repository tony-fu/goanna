import itertools
from collections import defaultdict
from typing import cast

import networkx as nx

from src.syntax import *
from src.traverse import Traverse
from src.type_utils import Binding, Zeta, Theta


def names_from_pat(pat: Pat):
    match pat:
        case PVar(name=name):
            return [name]
        case PApp(pats=pats) | PList(pats=pats) | PTuple(pats=pats):
            return list(itertools.chain(*[names_from_pat(p) for p in pats]))
        case PLit() | PWildCard():
            return []


def gather_theta(asts: list[Pretty], translated_names: dict[str, Binding], translated_classes: dict[str, str]) -> Theta:
    def update_func(data: Theta, ast: Pretty) -> tuple[Theta, list[int]]:
        match ast:
            case ClassDecl(d_head=d_head, decls=decls):
                d_head: DeclHead
                class_name = translated_classes.get(d_head.name)
                class_var = cast(str, d_head.ty_vars[0].name).split('.')[-1]
                for decl in decls:
                    names = decl.names
                    for name in names:
                        name = translated_names[name].translated
                        if name not in data.type_vars:
                            data.type_vars[name] = set()
                        data.type_vars[name].add(class_var)
                        if (name, class_var) not in data.type_classes:
                            data.type_classes[(name, class_var)] = set()
                        data.type_classes[(name, class_var)].add(class_name)
                return data, []

            case TypeSig(names=names):
                data.current = [translated_names[name].translated for name in names]
                for name in data.current:
                    data.type_vars[name] = set()
                return data, []

            case TyForall(context=context, ty=ty):
                if context is not None:
                    assertions = context.assertions
                    for assertion in assertions:
                        assertion: TyApp
                        class_name = assertion.ty1.name
                        class_name = translated_classes[class_name]
                        type_var_name = assertion.ty2.name
                        type_var_name = type_var_name.split('.')[-1]
                        for fun in data.current:
                            if (fun, type_var_name) not in data.type_classes:
                                data.type_classes[(fun, type_var_name)] = set()
                            data.type_classes[(fun, type_var_name)].add(class_name)
                return data, []
            case TyVar(name=name):
                name = name.split('.')[-1]
                for fun in data.current:
                    data.type_vars[fun].add(name)
                return data, []

            case _:
                return data, []

    traverser = Traverse(init_data=Theta(type_vars=defaultdict(set), type_classes=defaultdict(set), current=[]),
                         update_func=update_func)
    traverser.traverse_all(asts)
    return traverser.value


def gather_scopes(asts: list[Pretty], translations: dict[str, Binding], global_names: set[str]) -> Zeta:
    def update_func(data: Zeta, ast: Pretty) -> tuple[Zeta, list[int]]:
        match ast:
            case PatBind(pat=PVar(name=name)):
                if name in global_names:
                    data.current = None

                name = translations[name].translated
                if data.current is None:
                    data.current = name
                    if name not in data.args:
                        data.args[name] = set()
                elif data.current is not None and name != data.current:
                    parent = data.current
                    data.relations.append((parent, name))
                    data.args[name] = data.args[parent].copy()
                    data.current = name
                return data, []

            case Alt(pat=pat):
                names = names_from_pat(pat)
                names = {translations[name].translated for name in names}
                for name in names:
                    data.args[data.current].add(name)
                return data, []

            case ExpLambda(pats=pats):
                for pat in pats:
                    names = names_from_pat(pat)
                    names = {translations[name].translated for name in names}
                    for name in names:
                        data.args[data.current].add(name)
                return data, []
            case _:
                return data, []

    traverser = Traverse(
        init_data=Zeta(
            current=None,
            args=defaultdict(set),
            relations=[],
            relation_map=defaultdict(set)
        ),
        update_func=update_func)
    traverser.traverse_all(asts)
    relation_diagram = nx.DiGraph()
    relation_diagram.add_edges_from(traverser.value.relations)
    for node in relation_diagram.nodes:
        traverser.value.relation_map[node] = set(nx.descendants(relation_diagram, node))
    return traverser.value


def gather_garma_keys(asts: list[Pretty]) -> dict[int, str]:
    def update_func(data: dict[int, str], ast: Pretty) -> tuple[dict[int, str], list[int]]:
        if ast.ann[2] == 'Prelude':
            return data, []
        if ast.id in data:
            return data, []
        match ast:
            case ExpVar(name='void') | ExpCon(name='()'):
                data[ast.id] = f'special{len(data)}'
                return data, []
            case ExpLit() | PLit():
                data[ast.id] = f'lit{len(data)}'
                return data, []
            case ExpList() | PList() | ExpTuple() | PTuple() :
                data[ast.id] = f'parent{len(data)}'
                return data, []
            case PApp():
                data[ast.id] = f'parent{len(data)}'
                return data, []
            case TypeSig(ty=ty):
                data[ty.id] = f'sig{len(data)}'
                return data, []
            case _:
                return data, []

    traverser = Traverse(init_data={}, update_func=update_func)
    traverser.traverse_all(asts)
    return traverser.value


def translate_classes(asts: list[Pretty]) -> dict[str, str]:
    def update_func(data: dict[str, str], ast: Pretty) -> tuple[dict[str, str], list[int]]:
        match ast:
            case ClassDecl(d_head=d_head):
                name = d_head.name
                short_name = name.split('.')[-1].lower()
                if short_name not in data.values():
                    data.update({name: f'{short_name}'})
                    return data, []
                else:
                    counter = 0
                    while True:
                        if f'{short_name}{counter}' in data.values():
                            counter += 1
                        else:
                            data.update({name: f'{short_name}{counter}'})
                            return data, []
            case _:
                return data, []

    traverser = Traverse(init_data={}, update_func=update_func)
    traverser.traverse_all(asts)
    return traverser.value


def translate_types(asts: list[Pretty]) -> dict[str, str]:
    def update_func(data: dict[str, str], ast: Pretty) -> tuple[dict[str, str], list[int]]:
        match ast:
            case DataDecl(d_head=d_head):
                name = d_head.name
                short_name = name.split('.')[-1].lower()
                if short_name not in data.values():
                    data.update({name: f'{short_name}'})
                    return data, []
                else:
                    counter = 0
                    while True:
                        if f'{short_name}{counter}' in data.values():
                            counter += 1
                        else:
                            data.update({name: f'{short_name}{counter}'})
                            return data, []

            case _:
                return data, []

    traverser = Traverse(init_data={}, update_func=update_func)
    traverser.traverse_all(asts)
    return traverser.value


def translate_names(asts: list[Pretty]) -> dict[str, Binding]:
    return NameTranslator().translate(asts)


class NameTranslator:
    def __init__(self):
        self.counter = 0
        # self.defined_names = defined_names

    def get_id(self):
        self.counter += 1
        return self.counter

    def translate(self, asts: list[Pretty]) -> dict[str, Binding]:
        traverser = Traverse(init_data={'cons': Binding(
            translated='cons',
            is_defined=True,
            decl_node=None)}, update_func=self.update_func)
        traverser.traverse_all(asts)
        return traverser.value

    def update_func(self, data: dict[str, Binding], ast: Pretty) -> tuple[dict[str, Binding], list[int]]:
        if ast.id in data:
            return data, []

        match ast:
            case TypeSig(names=names, ty=ty):
                for name in names:
                    if name not in data:
                        new_id = self.get_id()
                        data.update({name: Binding(
                            translated=f'v{new_id}',
                            is_defined=True,
                            decl_node=ty.id,
                            is_function=isinstance(ty, TyFun)
                        )})
                    else:
                        data[name].decl_node = ast.id
                        data[name].is_defined = True
                        data[name].is_function = isinstance(ty, TyFun)
                return data, []

            case PatBind(pat=PVar(name=name, id=id), rhs_list=rhs_list):
                if name not in data:
                    new_id = self.get_id()
                    data.update({name: Binding(
                        translated=f'v{new_id}',
                        is_defined=True,
                        decl_node=id,
                        is_function=all([isinstance(rhs, ExpLambda) for rhs in rhs_list])
                    )})
                else:
                    data[name].decl_node = id
                    data[name].is_defined = True
                    data[name].is_function = all([isinstance(rhs, ExpLambda) for rhs in rhs_list])
                return data, []

            case DataCon(name=name, id=id):
                if name not in data:
                    new_id = self.get_id()
                    data.update({name: Binding(
                        translated=f'v{new_id}',
                        is_defined=True,
                        decl_node=id,
                        is_function=True
                    )})
                else:
                    data[name].decl_node = id
                    data[name].is_defined = True
                return data, []

            case PVar(name=name, id=id):
                if name not in data:
                    new_id = self.get_id()
                    data.update({name: Binding(
                        translated=f'v{new_id}',
                        decl_node=id
                    )})
                else:
                    data[name].decl_node = id
                return data, []

            case ExpVar(name=name) | ExpCon(name=name):
                if name == 'undefined':
                    return data, []

                if name not in data:
                    new_id = self.get_id()
                    data.update({name: Binding(
                        translated=f'v{new_id}'
                    )})

                data[name].instances.update({ast.id: f'v{self.get_id()}'})

                return data, []

            case _:
                return data, []
