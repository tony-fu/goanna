from typing import cast
from src.logic import *
from src.syntax import *
from src.type_utils import *


def head_from(name: str, file: str) -> RuleHead:
    return RuleHead(type='type', name=name, id=None, module=file)


def head_for_instance(name: str, id: int, file: str) -> RuleHead:
    return RuleHead(type='instance', name=name, id=id, module=file)


def pair(*terms: LTerm) -> LTerm:
    match len(terms):
        case 0:
            raise ValueError("adt needs at least one argument")
        case 1:
            return terms[0]
        case _:
            return LStruct(functor='pair', args=[pair(*terms[:-1]), terms[-1]])


def list_of(elem: LTerm) -> LTerm:
    return pair(LAtom(value='list'), elem)


def fun_of(*terms: LTerm) -> LTerm:
    match len(terms):
        case 0:
            raise ValueError("fun_of needs at least one argument")
        case 1:
            return terms[0]
        case _:
            return pair(LStruct(functor='function', args=[terms[0]]), fun_of(*terms[1:]))


def tuple_of(*terms: LTerm) -> LTerm:
    match len(terms):
        case 0:
            raise ValueError("tuple_of needs at least one argument")
        case 1:
            return terms[0]
        case _:
            return pair(LStruct(functor='tuple', args=[terms[0]]), tuple_of(*terms[1:]))


def type_of(name: str, var: LVar, gamma: LVar, zeta: LVar) -> LStruct:
    return LStruct(functor=name, args=[var, Call_, gamma, zeta, wildcard, LVar(value='Classes')])


def node_var(node: Pretty) -> LVar:
    return LVar(value='_' + str(node.id))


def type_var(ty_var: TyVar) -> LVar:
    # Represent a type variable
    simplified = ty_var.name.split('.')[-1]
    return LVar(value='_' + simplified)


def type_con(name: str) -> LAtom:
    return LAtom(value='h_' + name)


def gamma_has_key_value(name: str, value: LTerm) -> LStruct:
    return LStruct(functor='gamma_imply', args=[LAtom(value=name), GammaVar, value])


def zeta_has_key_value(name: str, value: LVar) -> LStruct:
    return LStruct(functor='get_assoc', args=[LAtom(value=name), ZetaVar, value])


def get_all_constraints(asts: list[Pretty], state: ConstraintGenState) -> tuple[list[Rule], list[tuple[str, str]]]:
    for ast in asts:
        generate_constraint(ast, head=None, state=state)
    return state.rules, state.class_diagram


def generate_constraint(ast: Pretty, head: RuleHead | None, state: ConstraintGenState):
    match ast:
        case Module(decls=decls):
            for decl in decls:
                generate_constraint(decl, None, state)
        case ClassDecl(context=context, d_head=d_head, decls=decls):
            d_head: DeclHead
            if len(d_head.ty_vars) != 1: raise NotImplementedError("Multi-parameter type class")

            context: Context | None
            class_name = state.class_translation.get(d_head.name)
            class_var = type_var(d_head.ty_vars[0])
            state.add_class_edge('root', class_name)
            if context is not None:
                for tyApp in context.assertions:
                    tyApp: TyApp
                    sup = tyApp.ty1
                    sup_name = state.class_translation.get(cast(TyCon, sup).name)
                    state.add_class_edge(class_name, sup_name)

            for decl in decls:
                decl: TypeSig
                names = decl.names
                ty = decl.ty
                for name in names:
                    head = head_from(state.decl_translation[name].translated, ast.ann[2])
                    state.add_axiom(unify(T, node_var(ty)), head)
                    rule_body = LStruct(functor='member', args=[
                        LStruct(functor='with', args=[LAtom(value=class_name), class_var]),
                        LVar(value='Classes')
                    ])
                    state.add_axiom(once(rule_body), head)
                    generate_constraint(ty, head, state)

        case InstDecl(context=context, ty_con=ty_con, tys=tys):
            ty_con: TyCon
            context: Context | None
            class_name = state.class_translation.get(ty_con.name)
            head = head_for_instance(class_name, ast.id, ast.ann[2])
            instance_type: Ty = tys[0]
            state.add_axiom(unify('T', node_var(instance_type)), head)
            generate_constraint(instance_type, head, state)
            if context is not None:
                for tyApp in context.assertions:
                    tyApp: TyApp
                    class_name = state.class_translation.get(tyApp.ty1.name)
                    instance_var = type_var(tyApp.ty2)
                    state.add_axiom(LStruct(functor=class_name, args=[instance_var]), head)

        case DataDecl(d_head=d_head, constructors=constructors, deriving=deriving):
            type_name = state.type_translation[d_head.name]
            type_vars: list[TyVar] = d_head.ty_vars
            data_type = pair(LAtom(value=type_name), *[type_var(v) for v in type_vars])
            for constructor in constructors:
                con_name = constructor.name
                head = head_from(state.decl_translation[con_name].translated, ast.ann[2])
                state.add_axiom(unify(T, fun_of(*[node_var(ty) for ty in constructor.tys], data_type)), head)
                for ty in constructor.tys:
                    generate_constraint(ty, head, state)

        case PatBind(pat=PVar(), rhs_list=[Rhs(exp=ExpVar(name='undefined'))]):
            pass
        case PatBind(pat=PVar(name=name), rhs_list=rhs, wheres=wheres):
            head = head_from(state.decl_translation[name].translated, ast.ann[2])
            for rhs in rhs:
                state.add_axiom(unify(T, node_var(rhs)), head)
                generate_constraint(rhs, head, state)
            for where_ in wheres:
                generate_constraint(where_, head, state)

        case FunBind(_, matches=matches):
            pat = matches[0].pat
            assert isinstance(pat, PVar)
            fun_name = pat.name
            head = head_from(state.decl_translation[fun_name].translated, ast.ann[2])
            for match in matches:
                for rhs in match.rhs_list:
                    state.add_axiom(unify(T, node_var(rhs)), head)
                    generate_constraint(rhs, head, state)
                for where_ in match.wheres:
                    generate_constraint(where_, head, state)

        case Rhs(guards=guards, exp=exp):
            for guard in guards:
                state.add_axiom(unify(node_var(guard), 'bool'), head)
                generate_constraint(guard, head, state)
            state.add_axiom(unify(node_var(ast), node_var(exp)), head)
            generate_constraint(exp, head, state)

        case PVar(name=name):
            key_in_zeta = state.decl_translation[name].translated
            state.add_axiom(zeta_has_key_value(key_in_zeta, node_var(ast)), head)

        case PWildCard():
            pass

        case PLit(lit=lit):
            if gamma_key := state.gamma_keys.get(ast.id):
                state.add_axiom(gamma_has_key_value(gamma_key, node_var(ast)), head)
            # state.add_axiom(gamma_has_key_value(state.gamma_keys[ast.id], node_var(ast)), head)
            state.add_rule(unify(node_var(ast), node_var(lit)), head, ast.id)
            generate_constraint(lit, head, state)

        case PList(pats=pats):
            if gamma_key := state.gamma_keys.get(ast.id):
                state.add_axiom(gamma_has_key_value(gamma_key, node_var(ast)), head)
            # state.add_axiom(gamma_has_key_value(state.gamma_keys[ast.id], node_var(ast)), head)
            fresh = state.fresh()
            state.add_rule(unify(node_var(ast), list_of(fresh)), head, ast.id)
            for pat in pats:
                generate_constraint(pat, head, state)
                state.add_axiom(unify(fresh, node_var(pat)), head)

        case PApp(name=name, pats=pats):
            if name == 'cons':
                car = pats[0]
                cdr = pats[1]

                v_elem = state.fresh()
                v_list = state.fresh()
                v_cons = state.fresh()
                state.add_axiom(unify(node_var(ast),  v_list), head)

                if isinstance(car, PVar):
                    state.add_axiom(gamma_has_key_value(state.decl_translation[car.name].translated, node_var(car)), head)
                state.add_rule(unify(node_var(car), v_elem), head, car.id)


                if isinstance(cdr, PVar):
                    state.add_axiom(gamma_has_key_value(state.decl_translation[cdr.name].translated, node_var(cdr)), head)
                state.add_rule(
                    unify(node_var(cdr), v_list)
                    , head, cdr.id)
                if gamma_key := state.gamma_keys.get(ast.id):
                    state.add_axiom(gamma_has_key_value(gamma_key, v_cons), head)

                # state.add_axiom(gamma_has_key_value(state.gamma_keys[ast.id], v_cons), head)
                state.add_axiom(unify(v_cons, fun_of(v_elem, v_list, v_list)), head)
                state.add_rule(type_of(
                    state.decl_translation[name].translated,
                    v_cons,
                    wildcard,
                    wildcard
                ), head, ast.id)

                generate_constraint(car, head, state)
                generate_constraint(cdr, head, state)

            else:
                fun = fun_of(*[node_var(pat) for pat in pats], node_var(ast))
                v = state.fresh()
                if gamma_key := state.gamma_keys.get(ast.id):
                    state.add_axiom(gamma_has_key_value(gamma_key, v), head)
                # state.add_axiom(gamma_has_key_value(state.gamma_keys[ast.id], v), head)
                state.add_axiom(unify(fun, v), head)
                for pat in pats:
                    generate_constraint(pat, head, state)
                state.add_rule(type_of(
                    state.decl_translation[name].translated,
                    v,
                    wildcard,
                    wildcard
                ), head, ast.id)

        case PTuple(pats=pats):
            state.add_axiom(unify(node_var(ast), tuple_of(*[node_var(pat) for pat in pats])), head)
            for pat in pats:
                generate_constraint(pat, head, state)

        case TypeSig(ty=ty, names=names):
            for name in names:
                head = head_from(state.decl_translation[name].translated, ast.ann[2])
                if gamma_key := state.gamma_keys.get(ty.id):
                    state.add_axiom(gamma_has_key_value(gamma_key, T), head)
                state.add_rule(unify(T, node_var(ty)), head, ty.id)
                generate_constraint(ty, head, state)
        case TyVar():
            state.add_axiom(unify(node_var(ast), type_var(cast(TyVar, ast))), head)

        case TyCon(name=name):
            if name == '()':
                state.add_axiom(unify(node_var(ast), 'unit'), head)
            elif name == '[]':
                state.add_axiom(unify(node_var(ast), list_of(LVar(value='_'))), head)
            elif name == '(->)':
                state.add_axiom(unify(node_var(ast), LAtom(value='function')), head)
            else:
                ty_name = state.type_translation[name]
                state.add_axiom(unify(node_var(ast), ty_name), head)

        case TyForall(context=context, ty=ty):
            if context is not None:
                for tyApp in context.assertions:
                    tyApp: TyApp
                    class_name = state.class_translation.get(tyApp.ty1.name)
                    instance_var = type_var(tyApp.ty2)
                    rule_body = LStruct(functor='member', args=[
                        LStruct(functor='with', args=[LAtom(value=class_name), instance_var]),
                        LVar(value='Classes')
                    ])
                    state.add_axiom(once(rule_body), head)
            state.add_axiom(unify(node_var(ast), node_var(ty)), head)
            generate_constraint(ty, head, state)

        case TyApp(ty1=ty1, ty2=ty2):
            generate_constraint(ty1, head, state)
            generate_constraint(ty2, head, state)
            state.add_axiom(unify(node_var(ast), pair(node_var(ty1), node_var(ty2))), head)

        case TyFun(ty1=ty1, ty2=ty2):
            generate_constraint(ty1, head, state)
            generate_constraint(ty2, head, state)
            state.add_axiom(unify(node_var(ast), fun_of(node_var(ty1), node_var(ty2))), head)

        case TyList(ty=ty):
            generate_constraint(ty, head, state)
            state.add_axiom(unify(node_var(ast), list_of(node_var(ty))), head)

        case TyTuple(tys=tys):
            state.add_axiom(unify(node_var(ast), tuple_of(*[node_var(ty) for ty in tys])), head)
            for ty in tys:
                generate_constraint(ty, head, state)

        case ExpLit(lit=lit):
            if gamma_key := state.gamma_keys.get(ast.id):
                state.add_axiom(gamma_has_key_value(gamma_key, node_var(ast)), head)
            state.add_rule(unify(node_var(ast), node_var(lit)), head, ast.id)
            generate_constraint(lit, head, state)

        case ExpApp(exp1=exp1, exp2=exp2):
            generate_constraint(exp1, head, state)
            generate_constraint(exp2, head, state)
            fun = fun_of(*[node_var(exp2), node_var(ast)])
            state.add_axiom(unify(fun, node_var(exp1)), head)

        case ExpLet(binds=decls, exp=exp):
            for decl in decls:
                generate_constraint(decl, head, state)
            generate_constraint(exp, head, state)
            state.add_axiom(unify(node_var(ast), node_var(exp)), head)

        case ExpIf(cond=cond, if_true=if_ture, if_false=if_false):
            state.add_axiom(unify(node_var(cond), 'bool'), head)
            state.add_axiom(unify(node_var(ast), node_var(if_ture)), head)
            state.add_axiom(unify(node_var(ast), node_var(if_false)), head)
            generate_constraint(cond, head, state)
            generate_constraint(if_ture, head, state)
            generate_constraint(if_false, head, state)

        case ExpCase(exp=exp, alts=alts):
            for alt in alts:
                alt: Alt
                pat = alt.pat
                alt_exp = alt.exp
                state.add_axiom(unify(node_var(exp), node_var(pat)), head)
                state.add_axiom(unify(node_var(ast), node_var(alt_exp)), head)
                generate_constraint(pat, head, state)
                generate_constraint(alt_exp, head, state)
            generate_constraint(exp, head, state)

        case ExpLambda(exp=exp, pats=pats):
            for pat in pats:
                generate_constraint(pat, head, state)
            fun = fun_of(*[node_var(pat) for pat in pats], node_var(exp))
            state.add_axiom(unify(node_var(ast), fun), head)
            generate_constraint(exp, head, state)

        case ExpList(exps=exps):
            if gamma_key := state.gamma_keys.get(ast.id):
                state.add_axiom(gamma_has_key_value(gamma_key, node_var(ast)), head)
            # state.add_axiom(gamma_has_key_value(state.gamma_keys[ast.id], node_var(ast)), head)
            fresh = state.fresh()
            state.add_rule(unify(node_var(ast), list_of(fresh)), head, ast.id)
            for exp in exps:
                generate_constraint(exp, head, state)
                state.add_axiom(unify(fresh, node_var(exp)), head)

        case ExpTuple(exps=exps):
            state.add_axiom(unify(node_var(ast), tuple_of(*[node_var(exp) for exp in exps])), head)
            for exp in exps:
                generate_constraint(exp, head, state)

        case ExpVar(name=name) | ExpCon(name=name):
            if name == '()':
                state.add_rule(unify(node_var(ast), 'unit'), head, ast.id)
            elif name == 'undefined':  # Bottom
                pass
            elif name == 'void':  # Type Hole
                # key_in_gamma = state.gamma_keys[ast.id]
                if gamma_key := state.gamma_keys.get(ast.id):
                    state.add_axiom(gamma_has_key_value(gamma_key, node_var(ast)), head)
                # state.add_axiom(gamma_has_key_value(key_in_gamma, node_var(ast)), head)
                state.add_rule(fail, head, ast.id)

            elif state.decl_translation[name].translated == head.name:  # Recursive call
                state.add_rule(unify(node_var(ast), 'T'), head, ast.id)

            elif state.decl_translation[name].is_defined:  # Function
                key_in_gamma = state.decl_translation[name].instances[ast.id]
                state.add_axiom(gamma_has_key_value(key_in_gamma, node_var(ast)), head)
                translated_var = state.decl_translation[name].translated

                gamma = LVar(value='_') if len(state.decl_translation[name].instances) >= 2 else GammaVar

                if translated_var in state.scopes.relation_map.get(head.name, []):
                    state.add_rule(
                        type_of(
                            translated_var,
                            node_var(ast),
                            gamma,
                            ZetaVar
                        ), head, ast.id)
                else:
                    state.add_rule(
                        type_of(
                            translated_var,
                            node_var(ast),
                            gamma,
                            wildcard
                        ), head, ast.id)

            else:  # Formal Argument
                key_in_zeta = state.decl_translation[name].translated
                state.add_axiom(zeta_has_key_value(key_in_zeta, node_var(ast)), head)

        case LitInt():
            state.add_axiom(unify(node_var(ast), 'int'), head)

        case LitString():
            state.add_axiom(unify(node_var(ast), list_of(LAtom(value='char'))), head)

        case LitChar():
            state.add_axiom(unify(node_var(ast), 'char'), head)

        case LitFrac():
            state.add_axiom(unify(node_var(ast), 'float'), head)
