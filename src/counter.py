class UniqueId:
    def __init__(self, start: int | None):
        self.counter = start + 1 if start else 1

    def get_id(self):
        self.counter += 1
        return self.counter

    @property
    def value(self):
        return self.counter