import re
import tempfile
from platform import platform
from pathlib import Path
from subprocess import run
import ujson
import src.transform_ast
import src.rename
from src.type_utils import ParserResult

transform = src.transform_ast.transform
get_global_names = src.rename.get_global_names




def get_parser(project_dir: Path) -> str:
    return str(project_dir / "bin" / "haskell-parser.exe") if platform() == 'Windows' else str(
        project_dir / "bin" / "haskell-parser")



def split_files(file: str) -> dict[str, str]:
    file_list= re.split(r'-- *module *: *([\w|\.]+) *\n', file)[1:]
    result = {}
    for i in range(0, len(file_list), 2):
        file_name = file_list[i]
        file_content = file_list[i+1]
        result.update({file_name: file_content})
    return result

def parse_single_file_project(file: str) -> ParserResult:
    file_list= re.split(r'-- *module *: *([\w|\.]+) *\n', file)[1:]
    files = []
    for i in range(0, len(file_list), 2):
        file_name = file_list[i].replace('.', '/') + '.hs'
        file_content = file_list[i+1]
        files.append((file_name, file_content))

    with tempfile.TemporaryDirectory() as tempdir:
        dir_path = Path(tempdir)
        for f in files:
            file_path = dir_path / f[0]
            file_path.parent.mkdir(parents=True, exist_ok=True)
            file_path.write_text(f[1], encoding='utf-8')
        return parse_project(dir_path)



def parse_project(base_dir: Path) -> ParserResult:
    project_dir = Path(__file__).parent.parent
    result = run(f'{get_parser(project_dir)} {base_dir}', shell=True, check=True, capture_output=True)
    parsed_data = ujson.loads(result.stdout)
    try:
        parse_result = ParserResult(**parsed_data)
        return parse_result
    except:
        raise Exception(parsed_data['contents'])




if __name__ == '__main__':
    parse_project(Path('../test'))
