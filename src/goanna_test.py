from tempfile import NamedTemporaryFile

from src.bind import translate_names, translate_types, translate_classes, gather_garma_keys, gather_scopes, gather_theta
from src.constraints import get_all_constraints
from src.goanna import split_files, parse_single_file_project
from src.node_list import get_node_list
from src.synonym import translate_synonyms
from src.transform_ast import transform_project
from src.type_check import run_marco, error_analysis, get_fix_fingerprints
from src.type_utils import ConstraintGenState, TypeCheckState
import cProfile

body = """
-- module: Prelude
data Char
data Int
data Float


type String = [Char]

length :: [a] -> Int
length = undefined

id :: a -> a
id = undefined

data Bool = True | False
data IO a = IO a

data Maybe a = Just a | Nothing

class Functor f where
    fmap :: (a -> b) -> f a -> f b

class Applicative f where
    pure :: a -> f a
    (<*>) :: f (a -> b) -> f a -> f b

class Monad m where
    return :: a -> m a
    (>>) :: m a -> m b -> m b
    (>>=) :: m a -> (a -> m b) -> m b

instance Functor Maybe
instance Functor []
instance Functor IO

instance Applicative Maybe
instance Applicative []
instance Applicative IO

instance Monad Maybe
instance Monad []
instance Monad IO

class Eq a where
    (==) :: a -> a -> Bool
    (/=) :: a -> a -> Bool

data Ordering = LT | EQ | GT

class Ord a where
    compare :: a -> a -> Ordering
    (<) :: a -> a -> Bool
    (<=) :: a -> a -> Bool
    (>) :: a -> a -> Bool
    (>=) :: a -> a -> Bool
    max :: a -> a -> a
    min :: a -> a -> a

instance Eq Int
instance Eq Bool
instance Eq Ordering
instance Eq Float
instance Eq Char
instance Eq a => Eq [a]

instance Ord Int
instance Ord Bool
instance Ord Char

filter :: (a -> Bool) -> [a] -> [a]
filter = undefined

read :: [Char] -> a
read = undefined

show :: a -> [Char]
show = undefined

data Either a b = Left a | Right b
otherwise = True

map :: (a -> b) -> [a] -> [b]
map = undefined

foldr :: (a -> b -> b) -> b -> [a] -> b
foldr = undefined

foldl :: (b -> a -> b) -> b -> [a] -> b
foldl = undefined


head :: [a] -> a
head = undefined

tail :: [a] -> [a]
tail = undefined

zipWith :: (a->b->c) -> [a] -> [b] -> [c]
zipWith = undefined

fst :: (a,b) -> a
fst = undefined

snd :: (a,b) -> b
snd = undefined

(++) :: [a] -> [a] -> [a]
(++) = undefined

pi :: Float
pi = 3.14


not :: Bool -> Bool
not = undefined

class Monoid a where
  mconcat :: [a] -> a
  mappend :: a -> a -> a
  mempty :: a

instance Monoid [a]

const  :: a -> b -> a
const = undefined


reverse :: [a] -> [a]
reverse = undefined

(||) :: Bool -> Bool -> Bool
(||) = undefined

(&&) :: Bool -> Bool -> Bool
(&&) = undefined

elem :: a -> [a] -> Bool
elem = undefined

even :: Int -> Bool
even = undefined

odd  :: Int -> Bool
odd = undefined

class Num a where
    (+) :: a -> a -> a
    (-) :: a -> a -> a
    (*) :: a -> a -> a

instance Num Int
instance Num Float

sum :: Num a => [a] -> a
sum = undefined

mod :: Int -> Int -> Int
mod = undefined

class Enum a where
    enumFrom :: a -> [a]

instance Enum Int
instance Enum Char

any :: [Bool] -> Bool
any = undefined

and :: [Bool] -> Bool
and = undefined

zip :: [a] -> [b] -> [(a, b)]
zip = undefined


fromIntegral :: Num a => Int -> a
fromIntegral = undefined

(/) :: Float -> Float -> Float
(/) = undefined

dropWhile :: (a -> Bool) -> [a] -> [a]
dropWhile = undefined

toUpper :: Char -> Char
toUpper = undefined

toLower :: Char -> Char
toLower = undefined

sqrt :: Float -> Float
sqrt = undefined

(^) :: Num a => a -> Int -> a
(^) = undefined

floor :: Num a => a -> Int
floor = undefined

ceiling :: Num a => a -> Int
ceiling = undefined


-- module: Main
students = [(1, 99), (2, 60), (3, 55)]

filterByKey k (key, value)  = key == k

x = filter (filterByKey '1') students

    """

def run():
    files = split_files(body)
    parse_result = parse_single_file_project(body)
    asts, global_names, counter_value = transform_project(parse_result.contents)
    asts = translate_synonyms(asts, counter_value)
    translated_names = translate_names(asts)
    translated_types = translate_types(asts)
    translated_class = translate_classes(asts)
    gamma_keys = gather_garma_keys(asts)
    scopes = gather_scopes(asts, translated_names, global_names)
    theta = gather_theta(asts, translated_names, translated_class)

    constraint_state = ConstraintGenState(
        decl_translation=translated_names,
        class_translation=translated_class,
        type_translation=translated_types,
        rules=[],
        gamma_keys=gamma_keys,
        class_diagram=[],
        scopes=scopes,
        theta=theta
    )

    rules, class_diagram = get_all_constraints(asts, constraint_state)
    node_list = get_node_list(asts)
    with NamedTemporaryFile(mode='w', suffix='.pl', delete=False) as f:
        file_name = f.name
        type_checking_state = TypeCheckState(
            decl_translation=translated_names,
            type_translation=translated_types,
            gamma_keys=gamma_keys,
            file_name=file_name,
            rules=rules,
            class_diagram=class_diagram,
            node_list=node_list,
            scopes=scopes,
            theta=theta
        )

        error_list = run_marco(type_checking_state)
        type_groups = error_analysis(type_checking_state, error_list)
        fingerprints = get_fix_fingerprints(type_checking_state, error_list, node_list, files, type_groups)

        print(len(fingerprints))

if __name__ == "__main__":
    run()
    # import pstats
    # from pstats import SortKey
    #
    #
    # cProfile.run('run()', "goanna-stats")
    # p = pstats.Stats('goanna-stats')
    # p.strip_dirs().sort_stats(SortKey.TIME).print_stats()
    # run()