import itertools
import os
from collections import defaultdict
from contextlib import ContextDecorator
from pathlib import Path
import networkx as nx
from swiplserver import PrologMQI, PrologThread
from src.logic import LTerm
from src.mqi import MQI
from src.type_utils import TypeCheckState, Rule
from jinja2 import Environment

EOL = os.linesep
type_check_predicates = Environment().from_string("""
type_check :-
    once((
        {%- for name in defined_names %}
        {{ name }}(_, [], _,  _,[
            {%- for arg in theta[name] -%}
            {{ arg }}-has([
                {%- for class in theta_classes[(name, arg)] -%}
                {{ class }}{{ ',' if not loop.last }}
                {%- endfor -%}
            ], {{ arg }}){{ ',' if not loop.last }}
            {%- endfor -%}
        ], Classes_{{name}}),
        {%- endfor %}
        true
    )),
    {%- for name in defined_names %}
    test_class(Classes_{{ name }}),
    {%- endfor %}
    true""")

main_predicate = Environment().from_string("""
main(G) :-
    once((
        list_to_assoc([
            cons-_{{ ',' if gamma_keys }}
            {%- for k in gamma_keys -%}
            {{ k }}-_{{ k if k in defined_names}}{{ ',' if not loop.last }}
            {%- endfor -%}
        ], Gamma),
        {%- for name in defined_names %}
        {{ name }}(_{{name}}, [], Gamma,  _, _, Classes_{{ name }}),
        {%- endfor %}
        true
    )),
    {%- for name in defined_names %}
    test_class(Classes_{{ name }}),
    {%- endfor %}
    assoc_to_list(Gamma, G),
    write_canonical(G)""")

type_rule_predicate_1 = Environment().from_string("{{ name }}(_, Calls, _, _, _, _) :- member({{ name }}, Calls), !")

type_rule_predicate_2 = Environment().from_string("""{{ name }}(T, Calls, Gamma, Zeta, Theta, Classes) :-
    Calls_ = [{{ name }} | Calls],
    list_to_assoc([
        {%- for v in zeta_vars -%}
        {{ v }}-_{{ ',' if not loop.last }}
        {%- endfor -%}
    ], Zeta_),
    merge_assoc(Zeta, Zeta_),
    Theta = [
        {%- for v in theta_vars -%}
        {{ v }}-_{{v}}{{ ',' if not loop.last }}
        {%- endfor -%}
    ],
    {% for rule in rules -%}
    {{ rule }},
    {% endfor %}
    true""")

class_rule_predicate = Environment().from_string("""{{ name }}(T) :- 
    T = has(Class, _),
    member1({{ name }}, Class),
    {%- for sup in super_classes %}
    {{sup }}(T),
    {%- endfor %}
    true""")

instance_rule_predicate = Environment().from_string("""
{{ name }}(T) :-
    nonvar(T),
    {%- for rule in rules %}
    {{ rule }},
    {%- endfor -%}
    {%- for sup in super_classes %}
    {{ sup }}(T),
    {%- endfor %}
    true""")

prolog_script = Environment().from_string("""
{{ preamble }}
{%- for rule in typing_rules %}
{{ rule }}.
{%- endfor %}

{%- for rule in class_rules %}
{{ rule }}.
{%- endfor %}

{{ type_check_predicate }}.
{{ main_predicate }}.
""")

class Inventory(ContextDecorator):
    def __init__(self, state: TypeCheckState):
        base_modules = ['Prelude']
        class_diagram = nx.DiGraph()
        class_diagram.add_edges_from(state.class_diagram)
        referenced_names = itertools.chain(*[b.instances.values() for b in state.decl_translation.values() if b.is_defined])
        self.rules = state.rules
        self.preamble = Path(__file__).parent.parent / "prolog/preamble.pl"
        self.axiomatic_rules: set[int] = {r.id for r in state.rules if r.axiom or (r.head.module in base_modules)}
        self.effective_rules: set[int] = {r.id for r in state.rules if r.id not in self.axiomatic_rules}
        self.zeta_vars: dict[str, set[str]] = state.scopes.args
        self.theta_vars: dict[str, set[str]] = state.theta.type_vars
        self.theta_classes = state.theta.type_classes
        self.defined_names: set[str] = {b.translated for b in state.decl_translation.values() if b.is_defined and b.translated != 'cons'}
        self.defined_classes: list[str] = [c for c in class_diagram.nodes if c != 'root']
        self.instance_rules: dict[str, dict[int, list[LTerm]]] = {c: defaultdict(list) for c in self.defined_classes}
        self.typing_rules: dict[str, list[Rule]] = defaultdict(list)
        self.super_classes: dict[str, set[str]] = {cls: set(nx.descendants(class_diagram, cls)) for cls in
                                                   self.defined_classes}
        self.gamma_keys = [*state.gamma_keys.values(), *[
            b.translated for b in state.decl_translation.values() if b.translated != 'cons'], *referenced_names]
        self.effective_names = {r.head.name for r in self.rules if r.id in self.effective_rules}
        for rule in self.rules:
            if rule.head.type == 'type':
                self.typing_rules[rule.head.name].append(rule)
            if rule.head.type == 'instance':
                self.instance_rules[rule.head.name][rule.head.id].append(rule.body)

        for key, value in self.theta_classes.items():
            key: tuple[str, str]
            value: set[str]
            for cls in value:
                super_classes = self.super_classes[cls]
                self.theta_classes[key] = value.union(super_classes)



    def assertz(self, clause):
        self.prolog.query(f'assertz(({clause}))')

    def __enter__(self):
        # self.mqi = PrologMQI(unix_domain_socket='')
        self.prolog: PrologThread = MQI().mqi.create_thread()
        return self

    def __exit__(self, *exc):
        self.prolog.stop()
        # self.mqi.stop()

    def run_preamble_type_check(self):
        self.prolog.query(f"consult('{self.preamble.as_posix()}')")
        for clause in self.render_class_rules():
            self.assertz(clause)
        for clause in self.render_typing_rules({r.id for r in self.rules if r.head.type == 'type'}, self.defined_names):
            self.assertz(clause)
        self.assertz(self.render_type_checking())

    def run_preamble_type_inference(self):
        self.prolog.query(f"consult('{self.preamble.as_posix()}')")
        for clause in self.render_class_rules():
            self.assertz(clause)
        for clause in self.render_typing_rules({r.id for r in self.rules if r.head.type == 'type'}, self.defined_names):
            self.assertz(clause)
        self.assertz(self.render_main())
    def render_typing_rules(self, rules: set[int], names: set[str]):
        for name in names:
            yield type_rule_predicate_1.render(name=name)
            yield type_rule_predicate_2.render(
                name=name,
                rules=[r.body for r in self.typing_rules[name] if (r.id in rules) or r.axiom],
                zeta_vars=self.zeta_vars.get(name, []),
                theta_vars=self.theta_vars.get(name, [])
            )

    def render_class_rules(self):
        for name in self.defined_classes:
            super_classes = self.super_classes[name]
            yield class_rule_predicate.render(name=name, super_classes=super_classes)
            for rules in self.instance_rules[name].values():
                yield instance_rule_predicate.render(
                    name=name, rules=rules,
                    super_classes=super_classes)

    def render_prolog(self) -> str:
        return prolog_script.render(
            preamble=self.preamble.read_text(),
            typing_rules=self.render_typing_rules(self.effective_rules, self.defined_names),
            class_rules=self.render_class_rules(),
            type_check_predicate=self.render_type_checking(),
            main_predicate=self.render_main()
        )

    def render_type_checking(self) -> str:
        return type_check_predicates.render(
            gamma_keys=self.gamma_keys,
            defined_names=self.defined_names,
            theta=self.theta_vars,
            theta_classes=self.theta_classes
        )

    def render_main(self) -> str:
        return main_predicate.render(
            gamma_keys=self.gamma_keys,
            defined_names=self.defined_names)

    def retract(self, name):
        self.prolog.query(f'retractall({name}(_, _, _, _, _, _))')

    def sat(self, selected_rules: set[int]) -> bool:
        for name in self.effective_names:
            self.retract(name)
        for clause in self.render_typing_rules(selected_rules, self.effective_names):
            self.assertz(clause)
        result = self.prolog.query("type_check")
        return result

    def check_type(self, selected_rules: set[int]) -> list:
        for name in self.effective_names:
            self.retract(name)
        for clause in self.render_typing_rules(selected_rules, self.effective_names):
            self.assertz(clause)

        result = self.prolog.query("main(G)")
        return result