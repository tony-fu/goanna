from __future__ import annotations

from swiplserver import *
from contextlib import contextmanager
from pydantic import BaseModel

Prolog = PrologThread

class LTerm(BaseModel):
    pass


class LVar(LTerm):
    value: str

    def __str__(self):
        return self.value


class LAtom(LTerm):
    value: str
    def __str__(self):
        return self.value


class LStruct(LTerm):
    functor: str
    args: list[LTerm]

    def __str__(self):
        if self.functor == '=':
            return f'{self.args[0]} = {self.args[1]}'
        else:
            return f'{self.functor}({", ".join([arg.__str__() for arg in self.args])})'


class LArray(LTerm):
    items: list[LTerm]

    def __str__(self):
        return self.value


class LDict(LTerm):
    items: dict[str, LTerm]

    def __str__(self):
        return self.value



class LAnd(LTerm):
    term_a: LTerm
    term_b: LTerm
    def __str__(self):
        return f'({self.term_a}, {self.term_b})'


# Specil Vars
T = LVar(value='T')
Call = LVar(value='Calls')
Call_ = LVar(value='Calls_')
GammaVar = LVar(value='Gamma')
GammaVar_ = LVar(value='Gamma_')
ZetaVar = LVar(value='Zeta')
ZetaVar_ = LVar(value='Zeta_')

# Special terms
succeed = LAtom(value='true')
fail = LAtom(value='false')
nil = LAtom(value='nil')
wildcard = LVar(value='_')
cut = LAtom(value='!')


# Special functors
def cons(x: LTerm, xs: LTerm) -> LTerm:
    return LStruct(functor='[|]', args=[x, xs])


# Special predicates
def unify(a: LTerm | str, b: LTerm | str):
    if isinstance(a, str):
        if a.islower():
            a = LAtom(value=a)
        else:
            a = LVar(value=a)
    if isinstance(b, str):
        if b.islower():
            b = LAtom(value=b)
        else:
            b = LVar(value=b)
    return LStruct(functor='=', args=[a, b])



def once(term: LTerm) -> LTerm:
    return LStruct(functor='once', args=[term])

class Clause(BaseModel):
    head: str
    body: list[LTerm]

    def __str__(self):
        head = self.head.__str__()
        body = ',\n  '.join([b.__str__() for b in self.body])
        if len(body) == 0:
            return head
        else:
            return f'{head} :-\n  {body}'

    def __repr__(self):
        return self.__str__()


@contextmanager
def start_prolog():
    # Code to acquire resource, e.g.:
    mqi = PrologMQI()
    prolog_thread: Prolog = mqi.create_thread()
    try:
        yield prolog_thread
    finally:
        # Code to release resource, e.g.:
        prolog_thread.stop()
        mqi.stop()



def run_swi():
    with start_prolog() as prolog:
        for i in range(1000):
            prolog.query('write(hello),nl, write(world).')

