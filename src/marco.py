import time
from typing import Callable
import networkx
from pydantic import BaseModel
from itertools import combinations
from pysat.solvers import Solver


class RuleSet(BaseModel):
    rules: set[int]
    setId: int


class Error(BaseModel):
    error_id: int
    mus_list: list[RuleSet]
    mcs_list: list[RuleSet]

class Marco:
    def __init__(self, rules: set[int], sat_fun: Callable[[set[int]], bool]):
        self.rules = frozenset(rules)
        self.graph = networkx.Graph()
        self.mus_list: set[frozenset[int]] = set()
        self.mss_list: set[frozenset[int]] = set()
        self.mcs_list: set[frozenset[int]] = set()
        self.tc_errors: list[Error] = []
        self.g = Solver()
        self.g.set_phases(literals=[r for r in self.rules])
        self.loop_counter = 0
        self.sat_counter = 0
        self.max_loops = 999
        self.sat_fun = sat_fun

    def grow(self, seed: frozenset[int]) -> frozenset[int]:
        for c in (self.rules - seed):
            if self.sat(seed | {c}):
                seed = seed | {c}

        return seed

    def shrink(self, seed: frozenset[int]) -> frozenset[int]:
        for c in seed:
            if not self.sat(seed - {c}):
                seed = seed - {c}

        return seed

    def sat(self, rules: frozenset[int]) -> bool:
        self.sat_counter += 1
        return self.sat_fun(set(rules))

    def run(self):
        # print('start marco', time.time())
        successful = self.g.solve()
        while successful:
            if self.loop_counter >= self.max_loops:
                raise Exception("Too many loops")
            self.loop_counter += 1

            seed = frozenset([v for v in self.g.get_model() if v > 0])
            if self.sat(seed):
                mss = self.grow(seed)
                self.mss_list.add(mss)
                self.g.add_clause([r for r in self.rules if r not in mss])

            else:
                mus = self.shrink(seed)
                self.mus_list.add(frozenset(mus))
                self.g.add_clause(([-r for r in mus]))

            successful = self.g.solve()
        # print('finished after ', self.sat_counter, ' runs', time.time())

    def analyse(self):
        # Populate mcs list
        # print('start analysis', time.time())
        for mss in self.mss_list:
            self.mcs_list.add(self.rules - mss)

        mus_index_list = list(enumerate(self.mus_list))
        self.graph.add_nodes_from([i for i, mus in mus_index_list])

        for combination in combinations(mus_index_list, 2):
            index1, mus1 = combination[0]
            index2, mus2 = combination[1]
            if mus1 & mus2 != set():
                self.graph.add_edge(index1, index2)
        # print('finish building graph', time.time())
        for i, component in enumerate(networkx.connected_components(self.graph)):

            mus_list = [mus_index_list[musId][1] for musId in component]
            mcs_list = []
            all_mus_rules: set[int] = set().union(*[mus for mus in mus_list])
            reduced_mcses = [RuleSet(setId=mcsId, rules=mcs & all_mus_rules) for mcsId, mcs in enumerate(self.mcs_list)]
            non_empty_mcses = [mcs for mcs in reduced_mcses if len(mcs.rules) != 0]
            seen = []
            for mcs in non_empty_mcses:
                if mcs.rules in seen:
                    continue
                else:
                    seen.append(mcs.rules)
                    mcs_list.append(mcs)

            mus_ruleset = [RuleSet(setId=musId, rules=set(mus)) for musId, mus in enumerate(mus_list)]
            self.tc_errors.append(Error(error_id=i, mus_list=mus_ruleset, mcs_list=mcs_list))
        # print('finish analysis', time.time())

    def show(self):
        print(f"Process finished after {self.loop_counter} iterations")
        print(f"{len(self.tc_errors)} islands found in the code")
        for island in self.tc_errors:
            print(f"island:")
            print(f"\nMUSs:")
            for mus in island.mus_list:
                print(mus)

            print(f"\nMCSs:")
            for mcs in island.mcs_list:
                print(mcs)
            print("\n\n")


if __name__ == "__main__":
    def s_fun(rules: set[int]) -> bool:
        props = [[1], [-1], [2], [-2], [1, 2]]
        solver = Solver()
        for rule in rules:
            solver.add_clause(props[rule - 1])
        return solver.solve()


    marco = Marco({1, 2, 3, 4, 5}, sat_fun=s_fun)
    marco.run()
    print(marco.mus_list)


