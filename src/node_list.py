from src.syntax import *
from src.traverse import Traverse


def update_function(data: dict[int, Loc], ast: Pretty) -> tuple[dict[int, Loc], list[int]]:
    data[ast.id] = ast.ann
    return data, []


def get_node_list(asts: list[Pretty]) -> dict[int, Loc]:
    traverser = Traverse(init_data={}, update_func=update_function)
    traverser.traverse_all(asts)
    return traverser.value
