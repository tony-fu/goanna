from pathlib import Path
from pyswip import Prolog
import src.logic as logic


def run_swip():
    prolog_file = Path(__file__).parent.parent / "prolog/test.pl"
    prolog_file.write_text('')
    prolog = Prolog()
    prolog.consult(prolog_file.as_posix())
    for i in range(100):
        prolog_file.write_text(f'main(X):- X = {i}.')
        result = next(prolog.query(f"make_reload_file('{prolog_file.as_posix()}'), main(X)"), False)
        print(result)

def run_mqi():
    prolog_file = Path(__file__).parent.parent / "prolog/test.pl"
    prolog_file.write_text('')

    with logic.start_prolog() as prolog:
        prolog.query(f"consult('{prolog_file.as_posix()}')")
        for i in range(100):
            prolog_file.write_text(f'main(X):- X = {i}.')
            result = prolog.query(f"make_reload_file('{prolog_file.as_posix()}'), main(X)")
            print(result)


if __name__ == "__main__":
    # run_mqi()
    prolog = Prolog()
    prolog.assertz('like(X) :- X = apple')
    prolog.assertz('like(X) :- X = banana')
