from collections import defaultdict
from typing import Any

import ujson


def is_list(prolog_json: dict) -> bool:
    match prolog_json:
        case {'functor': 'pair', 'args': ['list', _]}:
            return True
        case _:
            return False


def unroll_function(prolog_json: dict) -> list[Any]:
    match prolog_json:
        case {'functor': 'pair', 'args': [{'functor': 'function', 'args': [a]}, b]}:
            return [a, *unroll_function(b)]
        case _:
            return [prolog_json]


def is_function(prolog_json: dict) -> bool:
    match prolog_json:
        case {'functor': 'pair', 'args': [{'functor': 'function', 'args': [_]}, _]}:
            return True
        case _:
            return False


def unroll_tuple(prolog_json: dict) -> list[Any]:
    match prolog_json:
        case {'functor': 'pair', 'args': [{'functor': 'tuple', 'args': [a]}, b]}:
            return [*unroll_tuple(a), b]
        case _:
            return [prolog_json]


def is_tuple(prolog_json: dict) -> bool:
    match prolog_json:
        case {'functor': 'pair', 'args': [{'functor': 'tuple', 'args': [_]}, _]}:
            return True
        case _:
            return False


def unroll_adt(prolog_json: dict) -> list[Any]:
    match prolog_json:
        case {'functor': 'pair', 'args': [a, b]}:
            return [*unroll_adt(a), b]
        case _:
            return [prolog_json]


def is_adt(prolog_json: dict) -> bool:
    match prolog_json:
        case {'functor': 'pair', 'args': _}:
            return not any([is_list(prolog_json), is_tuple(prolog_json), is_function(prolog_json)])
        case _:
            return False


def unroll_prolog_list(prolog_json: dict) -> list[Any]:
    match prolog_json:
        case {'functor': '[|]', 'args': [a, b]}:
            return [a, *unroll_prolog_list(b)]
        case _:
            return [prolog_json]


class TypeVarMaker:
    def __init__(self):
        self.letters = 'abcdefghijklmnopqrstuvwxyz'
        self.mapping: dict[str: str] = {}

    def make_letter(self, value: str | None = None, force_new: bool = False):
        if force_new:
            letter = self.letters[-1]
            self.letters = self.letters[:-1]
            return letter

        if value is not None and value not in self.mapping:
            letter = self.letters[0]
            self.letters = self.letters[1:]
            self.mapping[value] = letter
            return letter

        if value is not None and value in self.mapping:
            return self.mapping[value]

class TypeBuilder:
    def __init__(self, json: dict | str, type_var_maker: TypeVarMaker):
        self.type_classes: dict[str, set[str]] = defaultdict(set)
        self.type_var_maker = type_var_maker
        self.type = self.from_json(json)

    def from_json(self, value: dict | str):
        match value:
            case '_':
                return self.type_var_maker.make_letter(force_new=True)
            case 'nil':
                return ''
            case 'unit':
                return '()'
            case {'functor': 'has', 'args': [type_classes, type_var]}:
                var_name = self.from_json(type_var)
                type_classes = unroll_prolog_list(type_classes)[:-1]
                if len(type_classes):
                    self.type_classes[var_name] = self.type_classes[var_name].union({cls.title() for cls in type_classes})
                return var_name

            case {'functor': 'pair', 'args': _}:
                if is_list(value):
                    x_type = self.from_json(value['args'][1]).strip()
                    return f'[{x_type}]'

                elif is_function(value):
                    args = unroll_function(value)
                    arg_types = []
                    for i, arg in enumerate(args):
                        if is_function(arg) and i != len(args) - 1:
                            arg_types.append('(' + self.from_json(arg) + ')')
                        else:
                            arg_types.append(self.from_json(arg))
                    return '->'.join(arg_types)

                elif is_tuple(value):
                    args = unroll_tuple(value)
                    return '(' + ', '.join([self.from_json(arg) for arg in args]) + ')'

                else:
                    adt_list = unroll_adt(value)
                    arg_types = []
                    for i, arg in enumerate(adt_list):
                        if is_adt(arg):
                            arg_types.append('(' + self.from_json(arg) + ')')
                        else:
                            arg_types.append(self.from_json(arg))
                    return ' '.join(arg_types)

            case _:
                if value[0].isupper():  # Prolog Variables
                    return self.type_var_maker.make_letter(value)

                elif value.startswith('skolem_'):  # Skolem Variables
                    return value[7:]

                elif value[0].islower():  # Prolog Atoms
                    return value.title()

                else:
                    raise NotImplementedError(value)

    def __str__(self):
        if len(self.type_classes):
            type_classes = []
            for var, classes in self.type_classes.items():
                for cls in classes:
                    type_classes.append(f'{cls} {var}')
            context = type_classes[0] if len(type_classes) == 1 else f"({', '.join(type_classes)})"
            return f'{context} => {self.type}'
        else:
            return self.type

    def __repr__(self):
        return self.__str__()
