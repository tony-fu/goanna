from src.type_utils import Env, GlobalNames


def name_raw(ast) -> str:
    """Get the name of a Name node AS IS, without row/column suffix """
    match ast:
        case {'tag': 'Ident' | 'Symbol', 'contents': [_, name]}:
            return name
        case _:
            raise ValueError(f"Unexpected ast: {ast}")


def name_suffxied(ast) -> str:
    "Get a name with row and column suffix"
    match ast:
        case {'tag': 'Ident' | 'Symbol', 'contents': [{'scope': {'type': 'LocalValue', 'loc': loc}}, name]}:
            return name + '.' + str(loc['line']) + '|' + str(loc['col'])

        case {'tag': 'Ident' | 'Symbol', 'contents': [{'loc': loc}, name]}:
            return name + '.' + str(loc['from']['line']) + '|' + str(loc['from']['col'])


def get_name(env: Env, ast: dict) -> str:
    "Get properly qualified name of a name node, unless it is bottom value, i.e. undefined"
    match ast:
        case {'tag': 'Ident', 'contents': [_, 'undefined']}:
            return 'undefined'

        case {'tag': 'Ident' | 'Symbol', 'contents': [{'scope': {'type': 'ValueBinder'}, 'loc': loc}, name]}:
            # Undecided
            if (name, name_suffxied(ast)) in env.get('global_names'):
                return env['module_name'] + '.' + name
            else:
                return env['module_name'] + '.' + name_suffxied(ast)

        case {'tag': 'Ident' | 'Symbol', 'contents': [{'scope': {'type': 'LocalValue', 'loc': loc}}, _]}:
            # Undecided
            return env['module_name'] + '.' + name_suffxied(ast)

        case {'tag': 'Ident' | 'Symbol', 'contents': [{'scope': {'type': 'GlobalSymbol', 'Symbol': symbol}}, name]}:
            # Global name
            return symbol['module'] + '.' + name

        case {'tag': 'Ident' | 'Symbol', 'contents': [{'scope': None}, name]}:
            # Probably type var
            return env['module_name'] + '.' + name

        case {'tag': 'Ident' | 'Symbol', 'contents': [{'scope': {'type': 'TypeBinder'}, 'loc': loc}, name]}:
            # Assume that types are not allowed to be shadowed
            return env['module_name'] + '.' + name

        case _:
            raise ValueError(f"Unexpected ast: {ast}")


def get_qop(env: Env, ast: dict) -> str:
    "Get properly qualified name of a QOp node"
    match ast:
        case {'tag': 'QVarOp' | 'QConOp', 'contents': [_, qname]}:
            return get_qname(env, qname)
        case _:
            raise ValueError(f"Unexpected ast: {ast}")


def get_qname(env: Env, ast: dict) -> str:
    "Get properly qualified name of a QName node, unless it is built-in symbol"
    match ast:
        case {'tag': 'UnQual', 'contents': [_, name]}:
            # A local name, undecided whether it is global or local
            return get_name(env, name)
        case {'tag': 'Qual', 'contents': [_, {'tag': 'ModuleName', 'contents': [_, mod_name]}, name]}:
            # A qualified name from another module, must be a global name
            return mod_name + '.' + name_raw(name)
        case {'tag': 'Special', 'contents': [_, {'tag': 'UnitCon'}]}:
            return '()'
        case {'tag': 'Special', 'contents': [_, {'tag': 'ListCon'}]}:
            return '[]'
        case {'tag': 'Special', 'contents': [_, {'tag': 'FunCon'}]}:
            return '(->)'
        case {'tag': 'Special', 'contents': [_, {'tag': 'TupleCon'}]}:
            return '(,)'
        case {'tag': 'Special', 'contents': [_, {'tag': 'Cons'}]}:
            return 'cons'
        case {'tag': 'Special', 'contents': [_, {'tag': 'ExprHole'}]}:
            return 'void'
        case _:
            raise ValueError(f"Unexpected ast: {ast}")


def get_global_names(ast) -> GlobalNames:
    """Get all global names (top level names) from module declarations"""

    def merge_results(results: [GlobalNames]) -> GlobalNames:
        if len(results) == 0:
            return set()
        return set().union(*results)

    match ast:
        case {'tag': 'Module', 'contents': [_, _, _, _, decls]}:
            return merge_results([get_global_names(decl) for decl in decls])

        case {"tag": "ClassDecl", 'contents': [_, _, _, _, class_decl_body]}:
            return set() if class_decl_body is None else merge_results([get_global_names(decl) for decl in class_decl_body])

        case {'tag': 'ClsDecl', 'contents': [_, decl]}:
            return get_global_names(decl)

        case {'tag': 'TypeDecl', 'contents': [_, d_head, _]}:
            return get_global_names(d_head)

        case {'tag': "DataDecl", 'contents': [_, _, _, d_head, con_decls, _]}:
            d_head_names = get_global_names(d_head)
            con_decl_names = [get_global_names(con_decl[3]) for con_decl in con_decls]
            return merge_results([d_head_names] + con_decl_names)

        case {'tag': "ConDecl", 'contents': [_, name, _]}:
            return {(name_raw(name), name_suffxied(name))}

        case {'tag': "InfixConDecl", 'contents': [_, _, name, _]}:
            return {(name_raw(name), name_suffxied(name))}

        case {'tag': "RecDecl", 'contents': [_, name, field_decls]}:
            r1 = {'variables': {(name_raw(name), name_suffxied(name))}, 'types': set(), }
            field_decls_names = [get_global_names(field_decl) for field_decl in field_decls]
            return merge_results([r1] + field_decls_names)

        case {'tag': "FieldDecl", 'contents': [_, name, _]}:
            return {(name_raw(name), name_suffxied(name))}

        case {'tag': "DHead", 'contents': [_, name]}:
            return set()

        case {'tag': "DHInfix", 'contents': [_, _, name]}:
            return {(name_raw(name), name_suffxied(name))}

        case {'tag': "DHParen", 'contents': [_, d_head]}:
            return get_global_names(d_head)

        case {'tag': "DHApp", 'contents': [_, _, d_head]}:
            return get_global_names(d_head)

        case {'tag': 'PList', 'contents': [_, pats]}:
            return merge_results([get_global_names(pat) for pat in pats])

        case {'tag': 'PTuple', 'contents': [_, _, pats]}:
            return merge_results([get_global_names(pat) for pat in pats])

        case {'tag': 'PatBind', 'contents': [_, pat, _, _]}:
            return get_global_names(pat)

        case {'tag': 'FunBind', 'contents': [_, matches]}:
            return merge_results([get_global_names(match) for match in matches])

        case {'tag': 'Match', 'contents': [_, name, _, _, _]}:
            return {(name_raw(name), name_suffxied(name))}

        case {'tag': 'InfixMatch', 'contents': [_, _, name, _, _, _]}:
            return {(name_raw(name), name_suffxied(name))}

        case {'tag': 'TypeSig', 'contents': [_, names, _]}:
            return {(name_raw(name), name_suffxied(name)) for name in names}

        case {'tag': 'PVar', 'contents': [_, name]}:
            return {(name_raw(name), name_suffxied(name))}

        case _:
            return set()
