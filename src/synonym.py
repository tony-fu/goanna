from textwrap import dedent
from typing import cast

from src.counter import UniqueId
from src.syntax import *

Synonym = dict[str, tuple[list[str], Ty]]


def gather_synonyms(_ast: Pretty) -> Synonym:
    match _ast:
        case Module(decls=decls):
            obj = {}
            for decl in decls:
                obj.update(gather_synonyms(decl))
            return obj
        case TypeDecl(d_head=d_head, ty=rhs):
            con_name: str = d_head.name
            ty_vars: list[TyVar] = d_head.ty_vars
            return {con_name: ([v.name for v in ty_vars], rhs)}

        case _:
            return {}


def merge_synonyms(_synonyms: list[Synonym]) -> Synonym:
    return {k: v for synonym in _synonyms for k, v in synonym.items()}


def replace(input_type: Ty, replacements: list[tuple[str, Ty]], get_id) -> Ty:
    match input_type:
        case TyVar(name=name):
            for k, v in replacements:
                if name == k:
                    return v.__class__(**{k: v for k, v in v.__dict__.items() if k != 'id'}, id=get_id())
            return TyVar(name=name, ann=input_type.ann, id=get_id())
        case TyApp(ty1=ty1, ty2=ty2):
            return TyApp(ty1=replace(ty1, replacements, get_id), ty2=replace(ty2, replacements, get_id),
                         ann=input_type.ann, id=get_id())
        case TyCon():
            return TyCon(name=input_type.name, ann=input_type.ann, id=get_id())
        case TyTuple(tys=tys):
            return TyTuple(tys=[replace(ty, replacements, get_id) for ty in tys], ann=input_type.ann, id=get_id())
        case TyFun(ty1=ty1, ty2=ty2):
            return TyFun(ty1=replace(ty1, replacements, get_id), ty2=replace(ty2, replacements, get_id),
                         ann=input_type.ann, id=get_id())
        case TyList(ty=ty):
            return TyList(ty=replace(ty, replacements, get_id), ann=input_type.ann, id=get_id())
        case TyForall(ty=ty):
            return TyForall(ty=replace(ty, replacements, get_id), ann=input_type.ann, context=input_type.context,
                            id=get_id())
        case _:
            raise Exception("Unknown type of Ty")


def replace_synonyms(_synonyms: Synonym, input_type: Pretty, get_id) -> tuple[bool, Pretty]:
    def unroll_type(_input_type: Ty) -> list[Ty]:
        match _input_type:
            case TyApp(ty1=ty1, ty2=ty2):
                return unroll_type(ty1) + unroll_type(ty2)
            case _:
                return [_input_type]

    def roll_type(_input_type: list[Ty]) -> Ty:
        if len(_input_type) == 1:
            return _input_type[0]
        else:
            return TyApp(ty1=roll_type(_input_type[:-1]), ty2=_input_type[-1], ann=_input_type[0].ann, id=get_id())

    synonym_names = _synonyms.keys()

    match input_type:
        case TyCon(name=name):
            if name in synonym_names:
                if _synonyms[name][0]:
                    raise Exception("The kind of synonym does not match its usage")
                ty = _synonyms[name][1]
                new_ty = ty.__class__(**{k: v for k, v in ty.__dict__.items() if k not in  ['id', 'ann']}, id=get_id(), ann=input_type.ann)
                return True, new_ty
            else:
                return False, input_type
        case TyApp():
            types = unroll_type(cast(TyApp, input_type))
            if isinstance(types[0], TyCon) and types[0].name in synonym_names:
                replace_from = _synonyms[types[0].name][0]
                replace_to = types[1:]
                replacing_type = _synonyms[types[0].name][1]
                if len(replace_from) != len(replace_to):
                    raise Exception("The kind of synonym does not match its usage")
                return True, replace(replacing_type, list(zip(replace_from, replace_to)), get_id)
            else:
                input_type: TyApp
                replaced1, new_type1 = replace_synonyms(_synonyms, input_type.ty1, get_id)
                replaced2, new_type2 = replace_synonyms(_synonyms, input_type.ty2, get_id)
                return replaced1 or replaced2, TyApp(ty1=cast(Ty, new_type1), ty2=cast(Ty, new_type2), ann=input_type.ann, id=input_type.id)

        case _:
            obj = {}
            replaced = False
            for k, v in input_type.__dict__.items():
                if isinstance(v, Pretty):
                    replaced, new_ast = replace_synonyms(_synonyms, v, get_id)
                    obj = {**obj, k: new_ast}
                elif isinstance(v, list):
                    if len(v) == 0:
                        obj = {**obj, k: v}
                    elif isinstance(v[0], Pretty):
                        each_replaced, new_ast = list(zip(*[replace_synonyms(_synonyms, vi, get_id) for vi in v]))
                        replaced = any(each_replaced)
                        obj = {**obj, k: list(new_ast)}
                    else:
                        obj = {**obj, k: v}

                else:
                    obj = {**obj, k: v}
            return replaced, type(input_type)(**obj)


def remove_synonyms(input_type: Module) -> Module:
    return Module(ann=input_type.ann, name=input_type.name,
                  decls=[decl for decl in input_type.decls if not isinstance(decl, TypeDecl)], id=input_type.id)


def replace_synonyms_recursive(_synonyms: Synonym, input_type: Module, get_id):
    loop_count = 0
    replaced = True
    while replaced:
        if loop_count > 50:
            raise Exception("Possible cyclic definition in type synonym")
        loop_count += 1
        replaced, new_ast = replace_synonyms(_synonyms, input_type, get_id)
        input_type = new_ast

    return remove_synonyms(input_type)


def translate_synonyms(modules: list[Module], counter_value: int) -> list[Module]:
    counter = UniqueId(counter_value)
    synonyms = merge_synonyms([gather_synonyms(module) for module in modules])
    return [replace_synonyms_recursive(synonyms, module, counter.get_id) for module in modules]
