from __future__ import annotations
from dataclasses import dataclass, field
from typing import Union
from src.type_utils import Loc


@dataclass
class Pretty:
    id: int = field(repr=False)
    ann: Loc = field(repr=False)

    def pretty(self):
        obj = {'type': self.__class__.__name__}
        for k, v in self.__dict__.items():
            if k == 'ann':
                continue
            # elif k == 'id':
            #     continue
            elif v is None:
                obj = {**obj, k: v}
            elif isinstance(v, str):
                obj = {**obj, k: v}
            elif isinstance(v, int):
                obj = {**obj, k: v}
            elif isinstance(v, list):
                for x in v:
                    if isinstance(x, Pretty):
                        obj = {**obj, k: [x.pretty() for x in v]}
                    else:
                        obj = {**obj, k: v}
            elif isinstance(v, Pretty):
                obj = {**obj, k: v.pretty()}
        return obj


Pat = Union[
    'PWildCard',
    'PApp',
    'PLit',
    'PList',
    'PTuple',
    'PVar']

Lit = Union[
    'LitChar',
    'LitString',
    'LitInt',
    'LitFrac']

Ty = Union[
    'TyCon',
    'TyApp',
    'TyFun',
    'TyTuple',
    'TyList',
    'TyVar',
    'TyForall']

Exp = Union[
    'ExpVar',
    'ExpCon',
    'ExpLit',
    'ExpApp',
    'ExpLambda',
    'ExpLet',
    'ExpIf',
    'ExpCase',
    'ExpDo',
    'ExpTuple',
    'ExpList',
    'ExpLeftSection',
    'ExpRightSection',
    'ExpEnumFrom',
    'ExpEnumTo',
    'ExpEnumFromTo']

Stmt = Union[
    'Generator',
    'Qualifier',
    'LetStmt']

Decl = Union[
    'TypeDecl',
    'DataDecl',
    'ClassDecl',
    'InstDecl',
    'TypeSig',
    'FunBind',
    'PatBind']


@dataclass
class Module(Pretty):
    decls: list[Decl]
    name: str


# Declarations

@dataclass
class TypeDecl(Pretty):
    d_head: DeclHead
    ty: Ty


@dataclass
class DataDecl(Pretty):
    d_head: DeclHead
    constructors: list[DataCon]
    deriving: list[TyCon]


@dataclass
class ClassDecl(Pretty):
    context: Context | None
    d_head: DeclHead
    decls: list[Decl]


@dataclass
class InstDecl(Pretty):
    context: Context | None
    ty_con: TyCon
    tys: list[Ty]
    body: list[Decl]


@dataclass
class PatBind(Pretty):
    pat: Pat
    rhs_list: list[Rhs]
    wheres: list[Decl]


@dataclass
class FunBind(Pretty):
    matches: list[PatBind]


@dataclass
class TypeSig(Pretty):
    names: list[str]
    ty: Ty


# Statements
@dataclass
class Generator(Pretty):
    pat: Pat
    exp: Exp


@dataclass
class Qualifier(Pretty):
    exp: Exp


@dataclass
class LetStmt(Pretty):
    binds: list[Decl]


# Patterns
@dataclass
class PWildCard(Pretty):
    pass

@dataclass
class PApp(Pretty):
    name: str
    pats: list[Pat]


@dataclass
class PLit(Pretty):
    lit: Lit


@dataclass
class PList(Pretty):
    pats: list[Pat]


@dataclass
class PTuple(Pretty):
    pats: list[Pat]


@dataclass
class PVar(Pretty):
    name: str


# Literals

@dataclass
class LitChar(Pretty):
    pass


@dataclass
class LitString(Pretty):
    pass


@dataclass
class LitInt(Pretty):
    pass


@dataclass
class LitFrac(Pretty):
    pass

# Types
@dataclass
class TyCon(Pretty):
    name: str


@dataclass
class TyApp(Pretty):
    ty1: Ty
    ty2: Ty


@dataclass
class TyFun(Pretty):
    ty1: Ty
    ty2: Ty


@dataclass
class TyTuple(Pretty):
    tys: list[Ty]


@dataclass
class TyList(Pretty):
    ty: Ty


@dataclass
class TyVar(Pretty):
    name: str


@dataclass
class TyForall(Pretty):
    context: Context | None
    ty: Ty


# Expressions


@dataclass
class ExpLit(Pretty):
    lit: Lit


@dataclass
class ExpVar(Pretty):
    name: str


@dataclass
class ExpCon(Pretty):
    name: str


@dataclass
class ExpApp(Pretty):
    exp1: Exp
    exp2: Exp


@dataclass
class ExpLambda(Pretty):
    pats: list[Pat]
    exp: Exp


@dataclass
class ExpLet(Pretty):
    binds: list[Decl]
    exp: Exp


@dataclass
class ExpIf(Pretty):
    cond: Exp
    if_true: Exp
    if_false: Exp


@dataclass
class ExpDo(Pretty):
    stmts: list[Stmt]


@dataclass
class ExpCase(Pretty):
    exp: Exp
    alts: list[Alt]


@dataclass
class ExpTuple(Pretty):
    exps: list[Exp]


@dataclass
class ExpList(Pretty):
    exps: list[Exp]


@dataclass
class ExpLeftSection(Pretty):
    left: Exp
    op: Exp


@dataclass
class ExpRightSection(Pretty):
    ann: Loc = field(repr=False)
    op: Exp
    right: Exp


@dataclass
class ExpEnumFromTo(Pretty):
    ann: Loc = field(repr=False)
    exp1: Exp
    exp2: Exp


@dataclass
class ExpEnumFrom(Pretty):
    ann: Loc = field(repr=False)
    exp: Exp


@dataclass
class ExpEnumTo(Pretty):
    ann: Loc = field(repr=False)
    exp: Exp


# Misc
@dataclass
class Alt(Pretty):
    ann: Loc = field(repr=False)
    pat: Pat
    exp: Exp
    binds: list[Decl]


@dataclass
class Rhs(Pretty):
    ann: Loc = field(repr=False)
    exp: Exp
    guards: list[Exp]


@dataclass
class DataCon(Pretty):
    ann: Loc = field(repr=False)
    name: str
    tys: list[Ty]


@dataclass
class DeclHead(Pretty):
    ann: Loc = field(repr=False)
    name: str
    ty_vars: list[TyVar]


@dataclass
class Context(Pretty):
    ann: Loc = field(repr=False)
    assertions: list[Ty]
