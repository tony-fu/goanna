from collections import UserDict

from src.counter import UniqueId
from src.syntax import *
from src.rename import get_name, get_qname, get_qop, get_global_names
from src.type_utils import Env, Loc, ParsedModule, GlobalNames
from typing import cast
import ujson


def transform_project(parsed_modules: list[ParsedModule]) -> tuple[list[Module], set[str], int]:
    counter = UniqueId(None)
    modules = []
    global_names = set()
    for parsed_module in parsed_modules:
        top_level_names = get_global_names(parsed_module.ast)
        ast = transform(
            {'module_name': parsed_module.moduleName, 'global_names': top_level_names},
            parsed_module.ast,
            counter.get_id
        )
        modules.append(cast(Module, ast))
        global_names = global_names.union({f'{parsed_module.moduleName}.{name}' for name, _ in top_level_names})
    return modules, global_names, counter.value


class HasLoc(UserDict):
    def __init__(self, data, module_name: str):
        super().__init__(data)
        self.module_name = module_name

    @property
    def loc(self) -> Loc:
        if self.get('loc', False):
            from_point: dict[str, int] = self.get('loc', {'from': False}).get('from', False)
            to_point: dict[str, int] = self.get('loc', {'to': False}).get('to', False)
            if from_point is False or to_point is False:
                raise ValueError("Tried to extract SrcSpan from an invalid location")
            else:
                return (from_point['line'], from_point['col']), (to_point['line'], to_point['col']), self.module_name,
        else:
            raise ValueError('Invalid annotation')


class HasSrcSpan(UserDict):
    def __init__(self, data, module_name: str):
        super().__init__(data)
        self.module_name = module_name

    @property
    def src_span(self) -> HasLoc:
        if isinstance(self.get('contents'), dict):
            return HasLoc(self['contents'], self.module_name)
        elif self.get('contents', False):
            return HasLoc(self['contents'][0], self.module_name)
        else:
            raise ValueError('Invalid annotation')


def join_locs(loc1: Loc, loc2: Loc) -> Loc:
    if loc1[0][0] < loc2[0][0]:
        from_point = loc1[0]
    elif loc1[0][0] == loc2[0][0] and loc1[0][1] <= loc2[0][1]:
        from_point = loc1[0]
    else:
        from_point = loc2[0]

    if loc1[1][0] > loc2[1][0]:
        to_point = loc1[1]
    elif loc1[1][0] == loc2[1][0] and loc1[1][1] >= loc2[1][1]:
        to_point = loc1[1]
    else:
        to_point = loc2[1]

    return from_point, to_point, loc1[2]


def unwrap_decl_head(env: Env, d_head: dict, get_id) -> DeclHead:
    def _type_var(ty_var_bind: HasSrcSpan) -> TyVar:
        return TyVar(ann=HasSrcSpan(ty_var_bind, env.get('module_name')).src_span.loc,
                     name=get_name(env, ty_var_bind['contents'][1]), id=get_id())

    def _unwrap(d_head_dict: dict, result: tuple[str | None, list[TyVar]]) -> tuple[str | None, list[TyVar]]:
        match d_head_dict:
            case {'tag': 'DHead', 'contents': [_, con_name]}:
                return get_name(env, con_name), result[1]
            case {'tag': 'DHInfix', 'contents': [_, type_var_bind, con_name]}:
                return get_name(env, con_name), result[1] + [_type_var(type_var_bind)]
            case {'tag': 'DHParen', 'contents': [_, _d_head]}:
                return _unwrap(_d_head, result)
            case {'tag': 'DHApp', 'contents': [_, _d_head, ty_var_bind]}:
                return _unwrap(_d_head, (result[0], [_type_var(ty_var_bind)] + result[1]))

    name, ty_vars = _unwrap(d_head, (None, []))
    return DeclHead(name=name, ty_vars=ty_vars, ann=HasSrcSpan(d_head, env.get('module_name')).src_span.loc,
                    id=get_id())


UnwrappedInstRule = tuple[TyCon | None, Context | None, list[Ty]]


def unwrap_inst_rule(env: Env, inst_rule: dict, get_id) -> UnwrappedInstRule:
    def _unwrap(rule_ast: dict, result: UnwrappedInstRule) -> UnwrappedInstRule:
        match rule_ast:
            case {'tag': 'IParen', 'contents': [_, _inst_rule]}:
                return _unwrap(_inst_rule, result)
            case {'tag': 'IRule', 'contents': [_, _, maybe_context, inst_head]}:
                _context = None if maybe_context is None else cast(Context, transform(env, maybe_context, get_id))
                _result = _unwrap(inst_head, result)
                return _result[0], _context, _result[2]
            case {'tag': 'IHCon', 'contents': [_, con_name]}:
                ty_con_name = get_qname(env, con_name)
                ty_con_ann = HasSrcSpan(con_name, env.get('module_name')).src_span.loc
                return TyCon(ann=ty_con_ann, name=ty_con_name, id=get_id()), result[1], result[2]
            case {'tag': 'IHInfix', 'contents': [_, ty, con_name]}:
                ty_con_name = get_qname(env, con_name)
                ty_con_ann = HasSrcSpan(con_name, env.get('module_name')).src_span.loc
                return TyCon(ann=ty_con_ann, name=ty_con_name, id=get_id()), result[1], result[2] + [
                    cast(Ty, transform(env, ty, get_id))]
            case {'tag': 'IHParen', 'contents': [_, _inst_head]}:
                return _unwrap(_inst_head, result)
            case {'tag': 'IHApp', 'contents': [_, _inst_head, ty]}:
                return _unwrap(_inst_head, (result[0], result[1], [cast(Ty, transform(env, ty, get_id))] + result[2]))

    return _unwrap(inst_rule, (None, None, []))


def transform(env: Env, ast: dict, get_id) -> Pretty | list[Pretty]:
    ast = HasSrcSpan(ast, env.get('module_name'))
    match ast:
        case {'tag': 'Module', 'contents': [_, _, _, _, decls]}:
            _decls: list[Decl] = [cast(Decl, transform(env, decl, get_id)) for decl in decls]
            return Module(ann=ast.src_span.loc, decls=_decls, name=env['module_name'], id=get_id())

        # Context
        case {'tag': 'CxSingle', 'contents': [_, assertion]}:
            _assertion = cast(Ty, transform(env, assertion, get_id))
            return Context(ann=ast.src_span.loc, assertions=[_assertion], id=get_id())

        case {'tag': 'CxTuple', 'contents': [_, assertions]}:
            _assertions = [cast(Ty, transform(env, assertion, get_id)) for assertion in assertions]
            return Context(ann=ast.src_span.loc, assertions=_assertions, id=get_id())

        case {'tag': 'CxEmpty', 'contents': [_, _]}:
            return Context(ann=ast.src_span.loc, assertions=[], id=get_id())

        case {'tag': 'TypeA', 'contents': [_, ty]}:
            return cast(Ty, transform(env, ty, get_id))

        case {'tag': 'ParenA', 'contents': [_, ass]}:
            return transform(env, ass, get_id)

        # Decls
        case {'tag': 'TypeDecl', 'contents': [_, decl_head, type_decl_rhs]}:
            _d_head = unwrap_decl_head(env, decl_head, get_id)
            _rhs = cast(Ty, transform(env, type_decl_rhs, get_id))
            return TypeDecl(ann=ast.src_span.loc, d_head=_d_head, ty=_rhs, id=get_id())

        case {'tag': 'InstDecl', 'contents': [_, _, instRule, instDecls]}:
            ty_con, context, tys = unwrap_inst_rule(env, instRule, get_id)
            _inst_decl = [] if instDecls is None else [cast(Decl, transform(env, instDecl['contents'][1], get_id)) for instDecl in instDecls]
            return InstDecl(ann=ast.src_span.loc, ty_con=ty_con, context=context, tys=tys, body=_inst_decl, id=get_id())

        case {"tag": "ClassDecl", 'contents': [_, context, decl_head, _, class_decl_body]}:
            context = None if context is None else cast(Context, transform(env, context, get_id))
            _d_head = unwrap_decl_head(env, decl_head, get_id)
            _body_decls = [] if class_decl_body is None else [cast(Decl, transform(env, decl['contents'][1], get_id)) for decl in class_decl_body]
            return ClassDecl(ann=ast.src_span.loc, context=context, d_head=_d_head, decls=_body_decls, id=get_id())

        case {'tag': 'ClsDataFam'}:
            raise NotImplementedError

        case {'tag': 'ClsTyFam'}:
            raise NotImplementedError

        case {'tag': 'ClsTyDef'}:
            raise NotImplementedError

        case {'tag': 'ClsDefSig'}:
            raise NotImplementedError

        case {'tag': "DataDecl", 'contents': [_, _, _, decl_head, con_decls, deriving]}:
            _d_head = unwrap_decl_head(env, decl_head, get_id)
            _con_decls = [cast(DataCon, transform(env, con_decl[3], get_id)) for con_decl in con_decls]
            derived_classes = deriving[0][2] if deriving else []
            _derived_classes = [unwrap_inst_rule(env, derived, get_id)[0] for derived in derived_classes]
            return DataDecl(ann=ast.src_span.loc, d_head=_d_head, constructors=_con_decls, deriving=_derived_classes,
                            id=get_id())

        case {'tag': 'ConDecl', 'contents': [_, name, types]}:
            _name = get_name(env, name)
            _types = [cast(Ty, transform(env, ty, get_id)) for ty in types]
            return DataCon(ann=ast.src_span.loc, name=_name, tys=_types, id=get_id())

        case {'tag': 'InfixConDecl', 'contents': [_, type1, name, type2]}:
            _name = get_qname(env, name)
            _type1 = cast(Ty, transform(env, type1, get_id))
            _type2 = cast(Ty, transform(env, type2, get_id))
            return DataCon(ann=ast.src_span.loc, name=_name, tys=[_type1, _type2], id=get_id())

        case {'tag': 'RecDecl', 'contents': [_, _, _]}:
            raise NotImplementedError("RecDecl is not implemented")

        case {'tag': 'PatBind', 'contents': [_, pat, rhs, wheres]}:
            _pat = cast(Pat, transform(env, pat, get_id))
            _rhs = cast(list[Rhs], transform(env, rhs, get_id))
            wheres = [] if wheres is None else wheres['contents'][1]
            _wheres = [cast(Decl, transform(env, decl, get_id)) for decl in wheres]
            return PatBind(ann=ast.src_span.loc, pat=_pat, rhs_list=_rhs, wheres=_wheres, id=get_id())

        case {'tag': 'FunBind', 'contents': [_, matches]}:
            _matches = [cast(PatBind, transform(env, match, get_id)) for match in matches]
            return FunBind(ann=ast.src_span.loc, matches=_matches, id=get_id())

        case {'tag': 'Match', 'contents': [_, name, pats, rhs, wheres]}:
            _name = get_name(env, name)
            _pats = [cast(Pat, transform(env, pat, get_id)) for pat in pats]
            _rhs = cast(list[Rhs], transform(env, rhs, get_id))
            wheres = [] if wheres is None else wheres['contents'][1]
            _wheres = [cast(Decl, transform(env, decl, get_id)) for decl in wheres]
            _pat_bind = PatBind(ann=ast.src_span.loc,
                                pat=PVar(name=_name, ann=HasSrcSpan(name, env.get('module_name')).src_span.loc,
                                         id=get_id()),
                                rhs_list=[Rhs(ann=__rhs.ann, guards=__rhs.guards,
                                              exp=ExpLambda(ann=__rhs.ann, pats=_pats, exp=__rhs.exp, id=get_id()),
                                              id=get_id()) for __rhs in _rhs],
                                wheres=_wheres, id=get_id())
            return _pat_bind

        case {'tag': 'InfixMatch', 'contents': [_, pat1, name, pat2, rhs, wheres]}:
            _pat1 = cast(Pat, transform(env, pat1, get_id))
            _name = get_qname(env, name)
            _pat2 = cast(Pat, transform(env, pat2, get_id))
            _rhs = cast(list[Rhs], transform(env, rhs, get_id))
            _wheres = [cast(Decl, transform(env, decl, get_id)) for decl in wheres]
            return PatBind(ann=ast.src_span.loc,
                           pat=PVar(name=_name, ann=HasSrcSpan(name, env.get('module_name')).src_span.loc, id=get_id()),
                           rhs_list=[Rhs(ann=__rhs.ann, guards=__rhs.guards,
                                         exp=ExpLambda(ann=__rhs.ann, pats=[_pat1, _pat2], exp=__rhs.exp, id=get_id()),
                                         id=get_id()) for __rhs in
                                     _rhs],
                           wheres=_wheres, id=get_id())

        case {'tag': 'TypeSig', 'contents': [_, names, sig]}:
            _names = [get_name(env, name) for name in names]
            _sig = cast(Ty, transform(env, sig, get_id))
            return TypeSig(ann=ast.src_span.loc, names=_names, ty=_sig, id=get_id())

        case {'tag': 'UnGuardedRhs', 'contents': [_, exp]}:
            _exp = cast(Exp, transform(env, exp, get_id))
            return [Rhs(ann=ast.src_span.loc, exp=_exp, guards=[], id=get_id())]

        case {'tag': 'GuardedRhss', 'contents': [_, rhs]}:
            _rhs = []
            for [rhs_ann, rhs_stmts, rhs_exp] in rhs:
                _exp = cast(Exp, transform(env, rhs_exp, get_id))
                _predicates = []
                for i, stmt in enumerate(rhs_stmts):
                    if stmt['tag'] != 'Qualifier':
                        raise Exception("Guarded RHS: Only Qualifier is supported.")
                    guard_exp = stmt['contents'][1]
                    _predicates.append(cast(Exp, transform(env, guard_exp, get_id)))
                _rhs.append(
                    Rhs(ann=HasLoc(rhs_ann, env.get('module_name')).loc, exp=_exp, guards=_predicates, id=get_id()))
            return _rhs

        # Exps
        case {'tag': 'Lit', 'contents': [_, lit]}:
            _lit = cast(Lit, transform(env, lit, get_id))
            return ExpLit(ann=ast.src_span.loc, lit=_lit, id=get_id())

        case {'tag': 'Con', 'contents': [_, qname]}:
            return ExpCon(ann=ast.src_span.loc, name=get_qname(env, qname), id=get_id())

        case {'tag': 'Var', 'contents': [_, qname]}:
            return ExpVar(ann=ast.src_span.loc, name=get_qname(env, qname), id=get_id())

        case {'tag': 'Tuple', 'contents': [_, _, exps]}:
            _exps = [cast(Exp, transform(env, exp, get_id)) for exp in exps]
            return ExpTuple(ann=ast.src_span.loc, exps=_exps, id=get_id())

        case {'tag': 'InfixApp', 'contents': [_, exp1, op, exp2]}:
            _exp1 = cast(Exp, transform(env, exp1, get_id))
            _op = get_qop(env, op)
            _op_ann = HasSrcSpan(op, env.get('module_name')).src_span.loc
            _combined_ann = join_locs(_exp1.ann, _op_ann)
            _exp2 = cast(Exp, transform(env, exp2, get_id))
            return ExpApp(ann=ast.src_span.loc,
                          exp1=ExpApp(ann=_combined_ann, exp1=ExpVar(name=_op, ann=_op_ann, id=get_id()), exp2=_exp1,
                                      id=get_id()),
                          exp2=_exp2, id=get_id())

        case {'tag': 'App', 'contents': [_, exp1, exp2]}:
            _exp1 = cast(Exp, transform(env, exp1, get_id))
            _exp2 = cast(Exp, transform(env, exp2, get_id))
            return ExpApp(ann=ast.src_span.loc, exp1=_exp1, exp2=_exp2, id=get_id())

        case {'tag': 'RightSection', 'contents': [_, op, right]}:
            _op = get_qop(env, op)
            _op_ann = HasSrcSpan(op, env.get('module_name')).src_span.loc
            _right = cast(Exp, transform(env, right, get_id))
            return ExpRightSection(ann=ast.src_span.loc, op=ExpVar(ann=_op_ann, name=_op, id=get_id()), right=_right,
                                   id=get_id())

        case {'tag': 'LeftSection', 'contents': [_, left, op]}:
            _left = cast(Exp, transform(env, left, get_id))
            _op = get_qop(env, op)
            _op_ann = HasSrcSpan(op, env.get('module_name')).src_span.loc
            return ExpLeftSection(ann=ast.src_span.loc, left=_left, op=ExpVar(ann=_op_ann, name=_op, id=get_id()),
                                  id=get_id())

        case {'tag': 'Lambda', 'contents': [_, pats, exp]}:
            _pats = [cast(Pat, transform(env, pat, get_id)) for pat in pats]
            _exp = cast(Exp, transform(env, exp, get_id))
            return ExpLambda(ann=ast.src_span.loc, pats=_pats, exp=_exp, id=get_id())

        case {'tag': 'Do', 'contents': [_, stmts]}:
            _stmts = [cast(Stmt, transform(env, stmt, get_id)) for stmt in stmts]
            return ExpDo(ann=ast.src_span.loc, stmts=_stmts, id=get_id())

        case {'tag': 'Paren', 'contents': [_, exp]}:
            return cast(Exp, transform(env, exp, get_id))

        case {'tag': "If", 'contents': [_, cond, left_branch, right_branch]}:
            _cond = cast(Exp, transform(env, cond, get_id))
            _left_branch = cast(Exp, transform(env, left_branch, get_id))
            _right_branch = cast(Exp, transform(env, right_branch, get_id))
            return ExpIf(ann=ast.src_span.loc, cond=_cond, if_true=_left_branch, if_false=_right_branch, id=get_id())

        case {'tag': 'Case', 'contents': [_, exp, alts]}:
            _exp = cast(Exp, transform(env, exp, get_id))
            _alts = [cast(Alt, transform(env, {'tag': 'Alt', 'contents': alt}, get_id)) for alt in alts]
            return ExpCase(ann=ast.src_span.loc, exp=_exp, alts=_alts, id=get_id())

        case {'tag': 'Alt', 'contents': [_, pat, rhs, maybe_binds]}:
            _pat = cast(Pat, transform(env, pat, get_id))
            _rhs = cast(list[Rhs], transform(env, rhs, get_id))
            _binds = [] if maybe_binds is None else [cast(Decl, transform(env, bind, get_id)) for bind in maybe_binds['contents'][1]]
            return Alt(ann=ast.src_span.loc, binds=_binds, exp=_rhs[0].exp, pat=_pat, id=get_id())

        case {'tag': 'Let', 'contents': [_, binds, exp]}:
            binds = binds['contents'][1]
            _binds = [cast(Decl, transform(env, bind, get_id)) for bind in binds]
            _exp = cast(Exp, transform(env, exp, get_id))
            return ExpLet(ann=ast.src_span.loc, binds=_binds, exp=_exp, id=get_id())

        case {'tag': 'List', 'contents': [_, exps]}:
            _exps = [cast(Exp, transform(env, exp, get_id)) for exp in exps]
            return ExpList(ann=ast.src_span.loc, exps=_exps, id=get_id())

        case {'tag': 'EnumFrom', 'contents': [_, exp]}:
            _exp = cast(Exp, transform(env, exp, get_id))
            return ExpEnumFrom(ann=ast.src_span.loc, exp=_exp, id=get_id())

        case {'tag': 'EnumTo', 'contents': [_, exp]}:
            _exp = cast(Exp, transform(env, exp, get_id))
            return ExpEnumTo(ann=ast.src_span.loc, exp=_exp, id=get_id())

        case {'tag': 'EnumFromTo', 'contents': [_, exp1, exp2]}:
            _exp1 = cast(Exp, transform(env, exp1, get_id))
            _exp2 = cast(Exp, transform(env, exp2, get_id))
            return ExpEnumFromTo(ann=ast.src_span.loc, exp1=_exp1, exp2=_exp2, id=get_id())

        # Statements
        case {'tag': 'Generator', 'contents': [_, pat, exp]}:
            _pat = cast(Pat, transform(env, pat, get_id))
            _exp = cast(Exp, transform(env, exp, get_id))
            return Generator(ann=ast.src_span.loc, pat=_pat, exp=_exp, id=get_id())

        case {'tag': 'Qualifier', 'contents': [_, exp]}:
            _exp = cast(Exp, transform(env, exp, get_id))
            return Qualifier(ann=ast.src_span.loc, exp=_exp, id=get_id())

        case {'tag': 'LetStmt', 'contents': [_, binds]}:
            _binds = [cast(Decl, transform(env, bind, get_id)) for bind in binds['contents'][1]]
            return LetStmt(ann=ast.src_span.loc, binds=_binds, id=get_id())

        # Literals
        case {'tag': 'Char', 'contents': [_, _, _]}:
            return LitChar(ann=ast.src_span.loc, id=get_id())

        case {'tag': 'String', 'contents': [_, _, _]}:
            return LitString(ann=ast.src_span.loc, id=get_id())

        case {'tag': 'Int', 'contents': [_, _, _]}:
            return LitInt(ann=ast.src_span.loc, id=get_id())

        case {'tag': 'Frac', 'contents': [_, _, _]}:
            return LitFrac(ann=ast.src_span.loc, id=get_id())

        # Types
        case {'tag': 'TyCon', 'contents': [_, qname]}:
            return TyCon(ann=ast.src_span.loc, name=get_qname(env, qname), id=get_id())

        case {'tag': 'TyVar', 'contents': [_, name]}:
            return TyVar(ann=ast.src_span.loc, name=get_name(env, name), id=get_id())

        case {'tag': 'TyApp', 'contents': [_, t1, t2]}:
            _t1 = cast(Ty, transform(env, t1, get_id))
            _t2 = cast(Ty, transform(env, t2, get_id))
            return TyApp(ann=ast.src_span.loc, ty1=_t1, ty2=_t2, id=get_id())

        case {'tag': 'TyFun', 'contents': [_, t1, t2]}:
            _t1 = cast(Ty, transform(env, t1, get_id))
            _t2 = cast(Ty, transform(env, t2, get_id))
            return TyFun(ann=ast.src_span.loc, ty1=_t1, ty2=_t2, id=get_id())

        case {'tag': 'TyTuple', 'contents': [_, _, ts]}:
            _ts = [cast(Ty, transform(env, t, get_id)) for t in ts]
            return TyTuple(ann=ast.src_span.loc, tys=_ts, id=get_id())

        case {'tag': 'TyList', 'contents': [_, tnode]}:
            _tnode = cast(Ty, transform(env, tnode, get_id))
            return TyList(ann=ast.src_span.loc, ty=_tnode, id=get_id())

        case {'tag': 'TyParen', 'contents': [_, ty]}:
            return cast(Ty, transform(env, ty, get_id))

        case {'tag': 'TyForall', 'contents': [_, _, _context, t]}:
            _context = None if _context is None else cast(Context, transform(env, _context, get_id))
            return TyForall(ann=ast.src_span.loc, context=_context, ty=cast(Ty, transform(env, t, get_id)), id=get_id())

        # Pats
        case {'tag': 'PVar', 'contents': [_, name]}:
            _name = get_name(env, name)
            return PVar(ann=ast.src_span.loc, name=_name, id=get_id())

        case {'tag': 'PTuple', 'contents': [_, _, pats]}:
            _pats = [cast(Pat, transform(env, pat, get_id)) for pat in pats]
            return PTuple(ann=ast.src_span.loc, pats=_pats, id=get_id())

        case {'tag': 'PList', 'contents': [_, pats]}:
            _pats = [cast(Pat, transform(env, pat, get_id)) for pat in pats]
            return PList(ann=ast.src_span.loc, pats=_pats, id=get_id())

        case {'tag': 'PLit', 'contents': [_, _, lit]}:
            return PLit(ann=ast.src_span.loc, lit=cast(Lit, transform(env, lit, get_id)), id=get_id())

        case {'tag': "PParen", 'contents': [_, p]}:
            return cast(Pat, transform(env, p, get_id))

        case {'tag': 'PApp', 'contents': [_, qname, p_args]}:
            _p_args = [cast(Pat, transform(env, p_arg, get_id)) for p_arg in p_args]
            return PApp(ann=HasSrcSpan(qname, env.get('module_name')).src_span.loc, name=get_qname(env, qname), pats=_p_args, id=get_id())

        case {'tag': 'PInfixApp', 'contents': [_, p1, op, p2]}:
            _p1 = cast(Pat, transform(env, p1, get_id))
            _p2 = cast(Pat, transform(env, p2, get_id))
            _op = get_qname(env, op)
            return PApp(ann=HasSrcSpan(op, env.get('module_name')).src_span.loc, name=_op, pats=[_p1, _p2], id=get_id())

        case {'tag': 'PWildCard'}:
            return PWildCard(ann=ast.src_span.loc, id=get_id())

        case _:
            print(ujson.dumps(ast))
            print("Unknown node type: ", ast.get('tag'))
            raise NotImplementedError
