from src.syntax import *
from typing import Callable, TypeVar

V = TypeVar('V')


class Traverse:
    def __init__(self,
                 init_data: V,
                 update_func: Callable[[V, Pretty], tuple[V, list[int]]],
                 ):
        self.data = init_data
        self.update = update_func
        self.ignore_list = []

    @property
    def value(self) -> V:
        return self.data

    def ignore(self, id: int):
        self.ignore_list.append(id)

    def traverse_all(self, asts: list[Pretty]):
        for ast in asts:
            self.traverse(ast)

    def traverse(self, ast: Pretty):
        is_ignored = ast.id in self.ignore_list
        if is_ignored:
            pass
        else:
            self.data, ignored = self.update(self.data, ast)
            for ignore in ignored:
                self.ignore(ignore)

        match ast:
            case Module(decls=decls):
                for decl in decls:
                    not is_ignored and self.traverse(decl)

            # Declarations
            case PatBind(pat=pat, rhs_list=rhs_list, wheres=wheres):
                not is_ignored and self.traverse(pat)
                for rhs in rhs_list:
                    not is_ignored and self.traverse(rhs)
                for where in wheres:
                    not is_ignored and self.traverse(where)
            case FunBind(matches=matches):
                for match in matches:
                    not is_ignored and self.traverse(match)
            case TypeSig(ty=ty):
                not is_ignored and self.traverse(ty)
            case ClassDecl(context=context, d_head=d_head, decls=decls):
                for decl in decls:
                    not is_ignored and self.traverse(decl)
                not is_ignored and context is not None and self.traverse(context)
                not is_ignored and self.traverse(d_head)
            case InstDecl(context=context, ty_con=ty_con, tys=tys, body=body):
                for decl in body:
                    not is_ignored and self.traverse(decl)
                not is_ignored and context is not None and self.traverse(context)
                not is_ignored and self.traverse(ty_con)
                for ty in tys:
                    not is_ignored and self.traverse(ty)
            case DataDecl(d_head=d_head, constructors=constructors, deriving=deriving):
                not is_ignored and self.traverse(d_head)
                for constructor in constructors:
                    not is_ignored and self.traverse(constructor)
                for ty in deriving:
                    not is_ignored and self.traverse(ty)

            # Patterns
            case PVar() | PLit() | PWildCard():
                pass
            case PApp(pats=pats):
                for pat in pats:
                    not is_ignored and self.traverse(pat)
            case PList(pats=pats):
                for pat in pats:
                    not is_ignored and self.traverse(pat)
            case PTuple(pats=pats):
                for pat in pats:
                    not is_ignored and self.traverse(pat)

            # Expressions
            case ExpVar() | ExpCon() | ExpLit():
                pass
            case ExpApp(exp1=exp1, exp2=exp2):
                not is_ignored and self.traverse(exp1)
                not is_ignored and self.traverse(exp2)
            case ExpLambda(exp=exp, pats=pats):
                not is_ignored and self.traverse(exp)
                for pat in pats:
                    not is_ignored and self.traverse(pat)
            case ExpLet(binds=decls, exp=exp):
                for decl in decls:
                    not is_ignored and self.traverse(decl)
                not is_ignored and self.traverse(exp)
            case ExpIf(cond=cond, if_true=then, if_false=else_):
                not is_ignored and self.traverse(cond)
                not is_ignored and self.traverse(then)
                not is_ignored and self.traverse(else_)
            case ExpDo(stmts=stmts):
                for stmt in stmts:
                    not is_ignored and self.traverse(stmt)
            case ExpCase(exp=exp, alts=alts):
                not is_ignored and self.traverse(exp)
                for alt in alts:
                    not is_ignored and self.traverse(alt)
            case ExpTuple(exps=exps):
                for exp in exps:
                    not is_ignored and self.traverse(exp)
            case ExpList(exps=exps):
                for exp in exps:
                    not is_ignored and self.traverse(exp)
            case ExpLeftSection(left=exp, op=op):
                not is_ignored and self.traverse(exp)
                not is_ignored and self.traverse(op)
            case ExpRightSection(op=op, right=exp):
                not is_ignored and self.traverse(exp)
                not is_ignored and self.traverse(op)
            case ExpEnumFrom(exp=exp):
                not is_ignored and self.traverse(exp)
            case ExpEnumTo(exp=exp):
                not is_ignored and self.traverse(exp)
            case ExpEnumFromTo(exp1=exp1, exp2=exp2):
                not is_ignored and self.traverse(exp1)
                not is_ignored and self.traverse(exp2)

            # Statements
            case Generator(exp=exp, pat=pat):
                not is_ignored and self.traverse(exp)
                not is_ignored and self.traverse(pat)
            case Qualifier(exp=exp):
                not is_ignored and self.traverse(exp)
            case LetStmt(binds=decls):
                for decl in decls:
                    not is_ignored and self.traverse(decl)

            # Types
            case TyVar() | TyCon():
                pass
            case TyApp(ty1=ty1, ty2=ty2):
                not is_ignored and self.traverse(ty1)
                not is_ignored and self.traverse(ty2)
            case TyFun(ty1=ty1, ty2=ty2):
                not is_ignored and self.traverse(ty1)
                not is_ignored and self.traverse(ty2)
            case TyTuple(tys=tys):
                for ty in tys:
                    not is_ignored and self.traverse(ty)
            case TyList(ty=ty):
                not is_ignored and self.traverse(ty)
            case TyForall(context=context, ty=ty):
                not is_ignored and context is not None and self.traverse(context)
                not is_ignored and self.traverse(ty)

            # Literals
            case LitInt(_) | LitFrac(_) | LitString(_) | LitChar(_):
                pass

            # Misc
            case DataCon(tys=tys):
                for ty in tys:
                    not is_ignored and self.traverse(ty)
            case Rhs(exp=exp, guards=guards):
                not is_ignored and self.traverse(exp)
                for guard in guards:
                    not is_ignored and self.traverse(guard)
            case Context(assertions=assertions):
                for assertion in assertions:
                    not is_ignored and self.traverse(assertion)
            case DeclHead(ty_vars=ty_vars):
                for ty_var in ty_vars:
                    not is_ignored and self.traverse(ty_var)

            case Alt(pat=pat, exp=exp, binds=binds):
                not is_ignored and self.traverse(pat)
                not is_ignored and self.traverse(exp)
                for bind in binds:
                    not is_ignored and self.traverse(bind)