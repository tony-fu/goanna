import itertools
from typing import Iterable
from src.inventory import Inventory
from src.marco import *
from src.reconstruct import TypeBuilder, TypeVarMaker
from src.type_utils import *


def original_name(state: TypeCheckState, name: str) -> str:
    for original, binding in state.decl_translation.items():
        if binding.translated == name:
            return original


def original_node_id(state: TypeCheckState, name: str) -> Iterable[int]:
    for original, binding in state.decl_translation.items():
        if binding.translated == name:
            yield binding.decl_node
        for instance_id, instance_name in binding.instances.items():
            if instance_name == name:
                yield instance_id

    for node_id, gamma_key in state.gamma_keys.items():
        if gamma_key == name:
            yield node_id


def run_marco(state: TypeCheckState) -> list[Error]:
    with Inventory(state) as inventory:
        inventory.run_preamble_type_check()
        marco_instance = Marco(inventory.effective_rules, sat_fun=inventory.sat)
        marco_instance.run()
        marco_instance.analyse()
        return eliminate_parent_child(state, marco_instance.tc_errors)


def eliminate_parent_child(state: TypeCheckState, errors: list[Error]) -> list[Error]:
    def is_within(loc1: Loc, loc2: Loc) -> bool:
        if loc1[2] != loc2[2]:
            return False
        if loc1[0][0] <= loc2[0][0] and loc1[0][1] <= loc2[0][1] and loc1[1][0] >= loc2[1][0] and loc1[1][1] >= loc2[1][
            1]:
            return True

    def relation(rs1: RuleSet, rs2: RuleSet, node_list: dict[int, Loc]) -> str:
        rs1_locs = [node_list[rule_id] for rule_id in rs1.rules]
        rs2_locs = [node_list[rule_id] for rule_id in rs2.rules]

        is_parent_child = True
        for rs1_loc in rs1_locs:
            has_parent = False
            for rs2_loc in rs2_locs:
                if is_within(rs1_loc, rs2_loc):
                    has_parent = True
                    break
            if not has_parent:
                is_parent_child = False
                break

        if is_parent_child:
            return 'parent-child'

        is_child_parent = True
        for rs2_loc in rs2_locs:
            has_parent = False
            for rs1_loc in rs1_locs:
                if is_within(rs2_loc, rs1_loc):
                    has_parent = True
                    break
            if not has_parent:
                is_child_parent = False
                break

        if is_child_parent:
            return 'child-parent'

        return 'no-relation'

    for error in errors:
        mcs_pairs = itertools.combinations(error.mcs_list, 2)
        to_delete = set()
        for rule_set1, rule_set2 in mcs_pairs:
            match relation(rule_set1, rule_set2, state.node_list):
                case 'parent-child':
                    to_delete.add(rule_set1.setId)
                case 'child-parent':
                    to_delete.add(rule_set2.setId)
                case _:
                    pass
        error.mcs_list = [mcs for mcs in error.mcs_list if mcs.setId not in to_delete]
    return errors


def error_analysis(state: TypeCheckState, errors: list[Error]) -> list[list[dict]]:
    results = []
    with Inventory(state) as inventory:
        inventory.run_preamble_type_inference()
        for error in errors:
            fixes = []
            for mcs in error.mcs_list:
                other_mcses = [other_error.mcs_list[0] for other_error in errors if
                               error.error_id != other_error.error_id]

                union_mcs = mcs.rules.union(*[other_mcs.rules for other_mcs in other_mcses])

                effective_rules = {r.id for r in state.rules if r.id not in union_mcs}
                result = inventory.check_type(effective_rules)[0]
                result = result['G']
                human_readable_types = {}
                for item in result:
                    item_name = item['args'][0]
                    item_type = item['args'][1]
                    node_type = str(TypeBuilder(item_type, TypeVarMaker()))
                    for node_id in original_node_id(state, item_name):
                        human_readable_types[node_id] = node_type
                fixes.append(human_readable_types)
            results.append(fixes)
    return results


def get_causes(diagnosis: list[Error], node_list: dict[int, Loc]) -> list[list[list[Loc]]]:
    for error in diagnosis:
        fixes = []
        for mcs in error.mcs_list:
            fix = [node_list[node_id] for node_id in mcs.rules]
            fixes.append(fix)
        yield fixes


# Errors [
#     Fixes [
#         Fix{
#             Fingerprint: {Name: Affected?, Name: Affected?}
#             FixLocations: [(Loc, HeadName, Id, IsFix?), (Loc, HeadName, Id, IsFix?), (Loc, HeadName, Id, IsFix?)]
#         },
#         Fix{
#             Fingerprint: {Name: Affected?, Name: Affected?}
#             FixLocations: [(Loc, HeadName, Id, IsFix?), (Loc, HeadName, Id, IsFix?), (Loc, HeadName, Id, IsFix?)]
#         }
#     ]
# ]
def affected_names_per_error(state: TypeCheckState, diagnosis: list[Error], node_list: dict[int, Loc]) -> list[
    list[str]]:
    for error in diagnosis:
        all_rule_ids = set()
        for mcs in error.mcs_list:
            all_rule_ids = all_rule_ids.union(set(mcs.rules))
        all_rules = sorted(
            [rule for rule in state.rules if rule.id in all_rule_ids],
            key=lambda rule: (node_list[rule.id][2], node_list[rule.id][0][0])
        )
        all_heads = [original_name(state, rule.head.name) for rule in all_rules if rule.head.type == 'type']
        yield list(dict.fromkeys(all_heads))


def affected_nodes_per_error(state: TypeCheckState, diagnosis: list[Error], node_list: dict[int, Loc]) -> list[
    list[int]]:
    for error in diagnosis:
        all_rule_ids = set()
        for mcs in error.mcs_list:
            all_rule_ids = all_rule_ids.union(set(mcs.rules))
        all_rule_ids = sorted(
            list(all_rule_ids),
            key=lambda rule: (node_list[rule][2], node_list[rule][0][0], node_list[rule][1][1])
        )

        yield all_rule_ids


def raiser(ex): raise ValueError(ex)


def get_src_text(loc: Loc, files: dict[str, str]) -> str:
    _from = loc[0]
    _to = loc[1]
    file_name = loc[2]
    file_content = files[file_name]
    lines = file_content.splitlines()
    text = lines[_from[0] - 1][_from[1] - 1:_to[1] - 1]
    return text


def get_fix_fingerprints(state: TypeCheckState, diagnosis: list[Error], node_list: dict[int, Loc], files: dict[str, str]
                         , types_groups: list[list[dict]]
                         ) -> list[list[dict]]:
    affected_names = affected_names_per_error(state, diagnosis, node_list)
    affected_nodes = affected_nodes_per_error(state, diagnosis, node_list)
    errors = []
    for error, decls, nodes, types_per_error in zip(diagnosis, affected_names, affected_nodes, types_groups):
        error: Error
        nodes: list[int]
        decls: list[str]
        fixes: dict[str: bool] = []
        for fix, types in zip(error.mcs_list, types_per_error):
            heads = {original_name(state, rule.head.name) for rule in state.rules if rule.id in fix.rules}
            fixes.append({
                'heads': {decl: decl in heads for decl in decls},
                'fixFile': node_list[list(fix.rules)[0]][2],
                'firstFixLine': node_list[list(fix.rules)[0]][0][0],
                'firstFixCol': node_list[list(fix.rules)[0]][1][1],
                'locations': {
                    node: {
                        'type': types[node] if node in types else raiser(f'node {node} not found in types'),
                        'is_fix': node in fix.rules,
                        'head': next(original_name(state, r.head.name) for r in state.rules if r.id == node),
                        'location': node_list[node],
                        'content': get_src_text(node_list[node], files)
                    } for node in nodes}
            })

        fixes = sorted(fixes, key=lambda x: (x['fixFile'], x['firstFixLine'], x['firstFixCol']))
        errors.append(fixes)
    return rank(errors)


def rank(errors):
    for error in errors:
        for fix in error:
            fix_locations = [location for location in fix.get('locations').values() if location.get('is_fix')]
            type_scores = []
            content_scores = []
            for location in fix_locations:
                fix_type: str = location.get('type')
                type_scores.append(len(fix_type))
                fix_content = location.get('content')
                content_scores.append(len(fix_content))
            location_score = 100 / len(fix_locations)
            type_score = 20 / (sum(type_scores) / len(type_scores))
            content_score = 20 / (sum(content_scores) / len(content_scores))
            ranked = location_score + type_score + content_score
            fix['score'] = ranked

    for error in errors:
        scores = sorted([fix['score'] for fix in error], reverse=True)
        for fix in error:
            fix['rank'] = scores.index(fix['score']) + 1

    for error in errors:
        for fix in error:
            for node_id in fix['locations']:
                ranked = [fix_ for fix_ in error if fix_['locations'][node_id]['is_fix']][0]['rank']
                fix['locations'][node_id]['rank'] = ranked

    return errors
