from typing import TypedDict, Literal

from pydantic import BaseModel
from src.logic import LAtom, LVar, LStruct, LTerm

GlobalNames = set[tuple[str, str]]
Env = TypedDict('Env', {'module_name': str, 'global_names': GlobalNames})
Loc = tuple[tuple[int, int], tuple[int, int], str]


class ParsedModule(BaseModel):
    ast: dict
    file: str
    moduleName: str


class ParserResult(BaseModel):
    tag: str
    contents: list[ParsedModule]


class Binding(BaseModel):
    is_defined: bool = False
    is_function: bool = False
    decl_node: int | None = None
    translated: str
    instances: dict[int, str] = {}


class RuleHead(BaseModel):
    type: Literal['type', 'instance']
    name: str
    module : str
    id: int | None

    def __str__(self):
        return f'{self.name}'


class Rule(BaseModel):
    head: RuleHead
    body: LTerm
    axiom: bool = False
    id: int | None
    is_predicate: bool = False

    def __str__(self):
        return f'{self.head} :- {self.body}'

class Zeta(BaseModel):
    current: str | None
    args: dict[str, set[str]]
    relations: list[tuple[str, str]]
    relation_map: dict[str, set[str]]


class Theta(BaseModel):
    current: list[str]
    type_vars: dict[str, set[str]]
    type_classes: dict[tuple[str, str], set[str]]


class ConstraintGenState(BaseModel):
    decl_translation: dict[str, Binding]
    class_translation: dict[str, str]
    type_translation: dict[str, str]
    rules: list[Rule]
    class_diagram: list[tuple[str, str]]
    gamma_keys: dict[int, str]
    fresh_counter: int = 0
    scopes: Zeta
    theta: Theta

    def add_class_edge(self, cls: str, sup: str):
        self.class_diagram.append((cls, sup))

    def add_rule(self, rule: LTerm, head: RuleHead, node: int):
        self.rules.append(Rule(head=head, body=rule, axiom=False, id=node))

    def add_axiom(self, rule: LTerm, head: RuleHead, is_predicate: bool = False):
        self.rules.append(Rule(head=head, body=rule, axiom=True, id=None, is_predicate=is_predicate))

    def fresh(self) -> LVar:
        self.fresh_counter += 1
        return LVar(value=f'_f{self.fresh_counter}')

class TypeCheckState(BaseModel):
    decl_translation: dict[str, Binding]
    type_translation: dict[str, str]
    gamma_keys: dict[int, str]
    file_name: str
    rules: list[Rule]
    class_diagram: list[tuple[str, str]]
    node_list: dict[int, Loc]
    scopes: Zeta
    theta: Theta

