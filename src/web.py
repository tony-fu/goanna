from tempfile import NamedTemporaryFile

import ujson
from fastapi import FastAPI, Body
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse
from starlette.staticfiles import StaticFiles

from src.bind import translate_types, gather_garma_keys, translate_classes, gather_scopes, translate_names, gather_theta
from src.constraints import get_all_constraints
from src.goanna import parse_single_file_project, split_files
from src.mqi import MQI
from src.node_list import get_node_list
from src.synonym import translate_synonyms
from src.transform_ast import transform_project
from src.type_check import *
from src.type_utils import TypeCheckState, ConstraintGenState

app = FastAPI()

origins = [
    "https://goanna.vercel.app",
    "http://127.0.0.1:3000",
    "http://localhost:3000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)



@app.on_event("startup")
async def startup_event():
    MQI()

@app.on_event("shutdown")
def shutdown_event():
    MQI().close()




@app.get("/demo")
async def demo():
    return RedirectResponse(url='https://goanna.vercel.app')

@app.post("/parse")
async def parse(body: str = Body()):
    # try:
    parse_result = parse_single_file_project(body)
    asts, global_names, counter_value = transform_project(parse_result.contents)
    asts = translate_synonyms(asts, counter_value)
    translated_names = translate_names(asts)
    translated_types = translate_types(asts)
    gamma_keys = gather_garma_keys(asts)
    scopes = gather_scopes(asts, translated_names, global_names)
    return {
        'asts': [ast.pretty() for ast in asts],
        'translated_names': translated_names,
        'translated_types': translated_types,
        'gamma_keys': gamma_keys,
        'scopes': scopes
    }


@app.post("/prolog")
async def prolog(body: str = Body()):
    # try:
    parse_result = parse_single_file_project(body)
    asts, global_names, counter_value = transform_project(parse_result.contents)
    asts = translate_synonyms(asts, counter_value)
    translated_names = translate_names(asts)
    translated_types = translate_types(asts)
    translated_class = translate_classes(asts)
    gamma_keys = gather_garma_keys(asts)
    scopes = gather_scopes(asts, translated_names, global_names)
    theta = gather_theta(asts, translated_names, translated_class)
    constraint_state = ConstraintGenState(
        decl_translation=translated_names,
        class_translation=translated_class,
        type_translation=translated_types,
        rules=[],
        gamma_keys=gamma_keys,
        class_diagram=[],
        scopes=scopes,
        theta=theta
    )

    rules, class_diagram = get_all_constraints(asts, constraint_state)
    node_list = get_node_list(asts)
    type_checking_state = TypeCheckState(
        decl_translation=translated_names,
        type_translation=translated_types,
        gamma_keys=gamma_keys,
        file_name='test.pl',
        rules=rules,
        class_diagram=class_diagram,
        node_list=node_list,
        scopes=scopes,
        theta=theta
    )
    inventory = Inventory(type_checking_state)
    return {
        'script': inventory.render_prolog(),
        'goal': '',
    }


# except Exception as e:
#     print(e)
#     return {'error': e.__dict__}


@app.post("/typecheck")
async def typecheck(body: str = Body()):
    try:
        files = split_files(body)
        parse_result = parse_single_file_project(body)
        asts, global_names, counter_value = transform_project(parse_result.contents)
        asts = translate_synonyms(asts, counter_value)
        translated_names = translate_names(asts)
        translated_types = translate_types(asts)
        translated_class = translate_classes(asts)
        gamma_keys = gather_garma_keys(asts)
        scopes = gather_scopes(asts, translated_names, global_names)
        theta = gather_theta(asts, translated_names, translated_class)

        constraint_state = ConstraintGenState(
            decl_translation=translated_names,
            class_translation=translated_class,
            type_translation=translated_types,
            rules=[],
            gamma_keys=gamma_keys,
            class_diagram=[],
            scopes=scopes,
            theta=theta
        )

        rules, class_diagram = get_all_constraints(asts, constraint_state)
        node_list = get_node_list(asts)
        with NamedTemporaryFile(mode='w', suffix='.pl', delete=False) as f:
            file_name = f.name
            type_checking_state = TypeCheckState(
                decl_translation=translated_names,
                type_translation=translated_types,
                gamma_keys=gamma_keys,
                file_name=file_name,
                rules=rules,
                class_diagram=class_diagram,
                node_list=node_list,
                scopes=scopes,
                theta=theta
            )

            error_list = run_marco(type_checking_state)
            type_groups = error_analysis(type_checking_state, error_list)
            fingerprints = get_fix_fingerprints(type_checking_state, error_list, node_list, files, type_groups)

            return {
                'translated_names': translated_names,
                'translated_types': translated_types,
                'fingerprints': fingerprints
            }


    except Exception as e:
        return {'error': str(e)}

app.mount("/", StaticFiles(directory="pages", html = True), name="site")